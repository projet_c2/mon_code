
using System;



public class sudoko{

    static void reml_int_aff(int[,] sudoku)
   {
    int i, j;
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 9; j++)
        {
            if ((i % 2 == 0) && (j % 3 == 0)){
                sudoku[i, j] = (i + 1 + j) % 9;}
            else{
                sudoku[i, j] = 0;}
        }
    }
    }

    static void afficher(int[,] sudoku , int k , int l){
        for (int i = 0; i < 9; i++)
         {
            if (i==k){
                Console.ForegroundColor = ConsoleColor.Green;
            }
            for (int j = 0; j < 9; j++){
                if ((j==l)){
                    Console.ForegroundColor = ConsoleColor.Green;
                    if (k==i){
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(sudoku[i, j] + "\t");
                        Console.ForegroundColor = ConsoleColor.Green;
                    }
                    else{ Console.Write(sudoku[i, j] + "\t");}
                }
                else { Console.Write(sudoku[i, j] + "\t");}
                if (i!=k) Console.ResetColor();
            }
            Console.ResetColor();
            Console.WriteLine();
         }
    }

   static int test_ligne_colone(int[,] sudoku, int k, int l, int v)
   {
    int a=0;
    int i =0;
    for(i=0;i<9;i++)
    {
       if ( (sudoku[k, i] == v)  || (sudoku[i, l] == v))    a= 1;
    }
    return a;
   }


   static int test_region(int[,] sudoku, int k, int l, int v)
   {
    int i=0, j = 0;
    int a = 0;
    for (i=k-k%3 ; (i<k-k%3+3) && (i!=k); i++)
    {
              for (j=l-l%3 ; j<l-l%3+3; j++)
              {
                  if (sudoku[i, j] == v)   a=1 ;
               }
         
    }
    
    return a;
   }


   static void process_sudk(int[,] sudoku)
   {
    Console.WriteLine("entrer des indices entre 0 et 8 dans la case ou  :\n"); 
    int k , l ;
    string input = Console.ReadLine();
    string[] numbers = input.Split(' ');
    int.TryParse(numbers[0], out k);
    int.TryParse(numbers[1], out l);
    if (k>8 || l>8 )           {Console.WriteLine(k + " ou " + l + " est invalide !!!!"); return;}
    if (sudoku[k , l] != 0)      Console.WriteLine("la case de ("+ k + "," + l +  ") est deja occupée par "+ sudoku[k ,l] + "!!\n");
    while (sudoku[k , l] == 0)
    {
        afficher(sudoku , k , l);
        Console.WriteLine("entrer une valeur entre 1 et 9 : \n");
        int v;
        string inputv = Console.ReadLine();
        string[] number = inputv.Split(' ');
        int.TryParse(number[0], out v);
        if (v>0 && v<10)
        {
           if ( (test_ligne_colone(sudoku , k, l , v)==0 ) && (test_region(sudoku , k, l , v) == 0))
           {
               sudoku[k, l] = v ;
               afficher(sudoku , k , l);
           }
           else                   Console.WriteLine(v + " est une valeur invalide --deja placée--\n");
        }
        else                      Console.WriteLine(v + " doit étre compris entre 1 et 9 !!!\n"); 
    }
    
   
    }



    static void Main(){
    
        int[,] sudoku = new int[9 , 9];
        reml_int_aff(sudoku);
    
        process_sudk(sudoku);


    }
}