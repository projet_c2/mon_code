
using System;

unsafe class Program
{
    static void Main()
    {
        int x = 10;
        int* p = &x; // Déclaration et initialisation d'un pointeur vers x

        Console.WriteLine("Valeur de x : " + x);
        Console.WriteLine("Valeur pointée par p : " + *p); // Accès à la valeur pointée par le pointeur

        *p = 20; // Modification de la valeur pointée par le pointeur

        Console.WriteLine("Nouvelle valeur de x : " + x);
    }
}