
using System;

public class tp1{

    static void somm_tab(){
        Console.WriteLine("entrer un entier n: ");
        int n , k , j , s=0;
        string inputs = Console.ReadLine();
        string[] nombre = inputs.Split(" ");
        int.TryParse(nombre[0],out n);
        double inv = 0.0;
        int l=1;
        
        while(l<=2){
            Console.WriteLine("la liste des nombre de puissance " + l + " est : ");
            for(int i=1; i<=n ; i++)
            {
             Console.Write((int)Math.Pow(i,l)+" ");
             s = s + i*i ;
             inv = inv + (1.0/(i*i));
            }
            Console.WriteLine();
            l++;
        }
        Console.WriteLine("\n la somme de carrés de " + n + " est : " + s/2);
        Console.WriteLine(" la somme des inverses  carrés de " + n +  " est : " + inv/2);
        k=0;
        while (k<=12)
        {
        j = 0;
        Console.WriteLine("\ntableau de multiplication de " + k + " est : \n" );
        for (j=0; j<=9 ; j++){
               Console.Write("                              " + k + " x " + j + " = " + (k*j) + "\n" );
               }
        Console.Write("\n --------- \n");
       
        k = k+1 ;
   
   
        }
    }

    static void resol_eq2()
    {
        Console.WriteLine("entrer a , b ,c : \n");
        float a;
        float b;
        float c;
        string inputs = Console.ReadLine();
        string[]  nombers = inputs.Split(' ');
        float.TryParse(nombers[0] , out a);
        float.TryParse(nombers[1], out b);
        float.TryParse(nombers[2] , out c);
        float del = b*b -4*a*c;
        if ( a==0 && b==0 && c==0)    Console.WriteLine("infinité de solutions \n ");
        if ( a==0 && b==0 & c!=0)     Console.WriteLine("impossible\n");
        if ( a==0 && b!=0 )           Console.WriteLine("premiere degree a une solution " + (-c/b));  
        if ( del == 0 && a!=0 )       Console.WriteLine("une  solution double " + (-b/(2*a)));
        if (del > 0 &&  a!=0)         Console.WriteLine("deux solutions differents " + ((-b + Math.Sqrt(del))/(2*a))+ " et " + ((-b - Math.Sqrt(del))/(2*a)));
        if ( del <0 && a!=0 )         Console.WriteLine("aucun solution reelle!!\n");
    }


    static void Main(){
        somm_tab();
        resol_eq2();
    
    }

}


