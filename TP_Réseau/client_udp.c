#include <stdio.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/socket.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    int s_com, emis;
    char mes[20];
    struct sockaddr_in adr;
    struct sockaddr_storage recep;
    struct hostent *entree;
    socklen_t lg_app;

    if (argc != 3) {
        printf("Usage: %s nom_serveur port\n", argv[0]);
        exit(1);
    }

    s_com = socket(AF_INET, SOCK_DGRAM, 0);
    printf("La socket est créée\n");

    adr.sin_family = AF_INET;
    adr.sin_port = htons(atoi(argv[2]));
    entree = (struct hostent *)gethostbyname(argv[1]);

    if (entree == NULL) {
        printf("Problème de résolution DNS\n");
        exit(1);
    }

    bcopy((char *)entree->h_addr, (char *)&adr.sin_addr, entree->h_length);

    printf("Entrez un mot\n");
    scanf("%s", mes);

    emis = sendto(s_com, mes, sizeof(mes), 0, (struct sockaddr *)&adr, sizeof(adr));

    if (emis <= 0) {
        printf("Gros problème\n");
    }

    recvfrom(s_com, mes, sizeof(mes), 0, (struct sockaddr *)&recep, &lg_app);
    printf("Message reçu : %s\n", mes);

    close(s_com);
    return 0;
}
