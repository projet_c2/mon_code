#include <stdio.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    int s_ecoute, scom, lg_app, i, j;
    struct sockaddr_in adr;
    struct sockaddr_storage recep;
    char buf[1500], renvoi[1500], host[1024], service[20];

    if (argc != 2) {
        printf("Usage: %s <port>\n", argv[0]);
        exit(1);
    }

    s_ecoute = socket(AF_INET, SOCK_STREAM, 0);
    printf("Creation de la socket\n");

    adr.sin_family = AF_INET;
    adr.sin_port = htons(atoi(argv[1]));
    adr.sin_addr.s_addr = INADDR_ANY;

    if (bind(s_ecoute, (struct sockaddr *)&adr, sizeof(struct sockaddr_in)) != 0) {
        printf("Probleme de bind sur v4\n");
        exit(1);
    }

    if (listen(s_ecoute, 5) != 0) {
        printf("Probleme d'ecoute\n");
        exit(1);
    }

    printf("En attente de connexion\n");

    while (1) {
        scom = accept(s_ecoute, (struct sockaddr *)&recep, (socklen_t *)&lg_app);
        getnameinfo((struct sockaddr *)&recep, sizeof(recep), host, sizeof(host), service, sizeof(service), 0);
        printf("Recu de %s venant du port %s\n", host, service);

        while (1) {
            recv(scom, buf, sizeof(buf), 0);
            printf("Buf recu %s\n", buf);

            bzero(renvoi, sizeof(renvoi));

            for (i = strlen(buf) - 1, j = 0; i >= 0; i--, j++)
                renvoi[j] = buf[i];
            renvoi[j] = '\0';

            send(scom, renvoi, strlen(renvoi), 0);

            bzero(buf, sizeof(buf));

            if (strcmp(renvoi, "NIF") == 0)
                break;
        }

        close(scom);
    }

    close(s_ecoute);
    return 0;
}
