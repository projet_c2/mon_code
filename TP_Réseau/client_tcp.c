#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/socket.h>
#include <string.h>
#include <unistd.h>


int main(int argc, char *argv[]) {
    char buffer[200], texte[200];
    int port, rc, sock, i, c;
    struct sockaddr_in addr;
    struct hostent *entree;

    if (argc != 3) {
        printf("Usage: %s <nom_machine> <numero_port>\n", argv[0]);
        exit(1);
    }

    port = atoi(argv[2]);
    addr.sin_port = htons(port);
    addr.sin_family = AF_INET;

    entree = (struct hostent *)gethostbyname(argv[1]);

    if (entree == NULL) {
        perror("Erreur lors de la résolution de l'hôte");
        exit(1);
    }

    bcopy((char *)entree->h_addr, (char *)&addr.sin_addr, entree->h_length);
    sock = socket(AF_INET, SOCK_STREAM, 0);

    if (connect(sock, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)) < 0) {
        perror("Probleme de connexion");
        exit(1);
    }

    printf("Connexion etablie\n");

    while (1) {
        bzero(texte, sizeof(texte));
        bzero(buffer, sizeof(buffer));
        i = 0;

        printf("Entrez une ligne de texte :\n");

        while ((c = getchar()) != '\n')
            texte[i++] = c;

        send(sock, texte, strlen(texte) + 1, 0);
        recv(sock, buffer, sizeof(buffer), 0);

        printf("Recu %s\n", buffer);

        if (strcmp("FIN", texte) == 0)
            break;
    }

    close(sock);
    return 0;
}


// attention pour l'execution ./