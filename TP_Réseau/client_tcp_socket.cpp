#include "pch.h"
#include <stdio.h>
#include <iostream>
#include <ws2tcpip.h>
#include <winsock2.h>

#pragma comment(lib, "ws2_32.lib")

using namespace std;

int main(int argc, CHAR* argv[]) {
    int s_com, i, c;
    char phrase[50], buf[50];
    WSADATA info;
    struct addrinfo* result = NULL, * ptr = NULL, hints;
    DWORD dwRetval;

    if (WSAStartup(MAKEWORD(2, 0), &info) == SOCKET_ERROR) {
        cout << "Erreur dans l'initialisation" << endl;
        WSACleanup();
        exit(1);
    }

    // Setup the hints address info structure which is passed to the getaddrinfo() function
    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = 0;

    dwRetval = getaddrinfo("nom_machine", "numero_port", &hints, &result);
    if (dwRetval != 0) {
        printf("getaddrinfo failed with error: %d\n", dwRetval);
        WSACleanup();
        return 1;
    }

    printf("getaddrinfo returned success\n");

    s_com = socket(AF_INET, SOCK_STREAM, 0);
    cout << "La socket est créée\n";

    if (connect(s_com, result->ai_addr, result->ai_addrlen) < 0) {
        cout << "Problème de connexion";
        getchar();
        exit(1);
    }

    freeaddrinfo(result);

    while (true) {
        printf("Entrez une ligne de texte\n");
        i = 0;
        while ((c = getchar()) != '\n')
            phrase[i++] = c;

        phrase[i] = '\0';
        printf("Ligne trouvée : %s\n", phrase);

        send(s_com, phrase, i + 1, 0);

        for (i = 0; i < sizeof(buf); i++)
            buf[i] = '\0';

        recv(s_com, buf, sizeof(buf), 0);
        printf("Message reçu : %s\n", buf);

        if (strncmp(phrase, "FIN", 3) == 0)
            break;
    }

    WSACleanup();
    return 0;
}
