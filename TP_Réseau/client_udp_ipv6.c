#include <stdio.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/socket.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    int s_com, emis, port, rc, lg_app;
    char mes[20];
    struct sockaddr_in6 adr;
    struct sockaddr_storage recep;
    struct in6_addr serveraddr;
    struct addrinfo hints, *res = NULL;

    if (argc != 3) {
        printf("Usage: %s nom_serveur port\n", argv[0]);
        exit(1);
    }

    memset(&hints, 0x00, sizeof(hints));
    hints.ai_flags = AI_NUMERICSERV;
    hints.ai_family = AF_INET6;
    hints.ai_socktype = SOCK_DGRAM;
    port = atoi(argv[2]);

    // on teste si l'adresse IP est numérique ou textuelle
    rc = inet_pton(AF_INET6, argv[1], &serveraddr);
    if (rc == 1) {
        hints.ai_flags |= AI_NUMERICHOST;
    }

    rc = getaddrinfo(argv[1], argv[2], &hints, &res);

    if (rc != 0) {
        printf("Problème de résolution DNS : %s\n", gai_strerror(rc));
        exit(1);
    }

    s_com = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    printf("La socket est créée\n");

    printf("Entrez un mot\n");
    scanf("%s", mes);

    emis = sendto(s_com, mes, sizeof(mes), 0, (struct sockaddr *)res->ai_addr, res->ai_addrlen);

    if (emis <= 0) {
        printf("Gros problème\n");
    }

    recvfrom(s_com, mes, sizeof(mes), 0, (struct sockaddr *)&recep, &lg_app);
    printf("Message reçu : %s\n", mes);

    close(s_com);
    return 0;
}
