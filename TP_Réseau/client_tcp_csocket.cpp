
#include "stdafx.h"
#include <string.h>
#include <iostream>
#include <afxsock.h>

using namespace std;

void DieError(char* errorMessage) {
    cout << errorMessage << endl;
    exit(0);
}

int _tmain(int argc, _TCHAR* argv[]) {
    if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0)) {
        DieError("Fatal Error: MFC initialization failed");
    }

    cout << "Début du programme\n";

    int port = 16515;
    CSocket servSocket;
    AfxSocketInit(NULL);

    if (!servSocket.Create(port)) {
        DieError("servSock.Create() failed");
    }

    if (!servSocket.Listen(5)) {
        DieError("servSock.Listen() failed");
    }

    for (;;) {
        sockaddr_in Addr; // Client address
        int clntLen = sizeof(Addr); // Taille du client
        CSocket sock_com;

        if (!servSocket.Accept(sock_com)) {
            DieError("servSock.Accept() failed");
        }

        if (!sock_com.GetPeerName((sockaddr*)&Addr, &clntLen)) {
            DieError("clntSock.GetPeerName() failed");
        }

        cout << "Handling client " << inet_ntoa(Addr.sin_addr) << endl;

        while (1) {
            int recvMsgSize, i, j;
            char Buffer[100], renvoi[100];

            recvMsgSize = sock_com.Receive(Buffer, 100, 0);

            if (recvMsgSize < 0) {
                DieError("clntSock.Receive() failed");
            }

            cout << "Message reçu : " << Buffer << "\n";

            for (i = strlen(Buffer) - 1, j = 0; i >= 0; i--, j++)
                renvoi[j] = Buffer[i];

            renvoi[j + 1] = '\0';
            sock_com.Send(renvoi, strlen(renvoi), 0);

            if (strcmp(renvoi, "NIF") == 0)
                break;
        }

        sock_com.Close();
    }

    servSocket.Close();
    getchar();
    return 0;
}
