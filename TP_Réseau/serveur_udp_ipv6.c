#include <stdio.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/socket.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    int s_ecoute, recus;
    char mes[20], host[200], service[10];
    struct sockaddr_in6 adr;
    struct sockaddr_storage recep;
    socklen_t lg_app;

    s_ecoute = socket(AF_INET6, SOCK_DGRAM, 0);
    printf("La socket est créée\n");

    adr.sin6_family = AF_INET6;
    adr.sin6_port = htons(atoi(argv[1]));
    memcpy((void *)&adr.sin6_addr, (void *)&in6addr_any, sizeof(struct in6_addr));

    if (bind(s_ecoute, (struct sockaddr *)&adr, sizeof(struct sockaddr_in6)) != 0) {
        printf("Problème de connexion\n");
        exit(1);
    }

    printf("Je suis en écoute\n");

    lg_app = sizeof(struct sockaddr_storage);
    recus = recvfrom(s_ecoute, mes, sizeof(mes), 0, (struct sockaddr *)&recep, &lg_app);

    if (recus <= 0) {
        printf("Bug\n");
    } else {
        getnameinfo((struct sockaddr *)&recep, sizeof(recep), host, sizeof(host), service, sizeof(service), 0);
        printf("Message : %s venant de %s et du port %s\n", mes, host, service);
        sendto(s_ecoute, "OK", 3, 0, (struct sockaddr *)&recep, lg_app);
    }

    close(s_ecoute);
    return 0;
}
