/**
 * @file arbres_parcours.c
 * @brief fichier d'implementation du module pour le parcours d'arborescence
 */
#include <stdio.h>
#include <stdlib.h>

#include "../pile.h"
#include "../eltsArbre.h"
#include "arbres_parcours.h"


/** TO DO
 * @brief calculer le nombre de fils ou freres d'un point a partir du pointeur du debut de la liste
 * @param [in] ptCell pointeur vers le 1er frere
 * @return le nombre de fils ou freres
 */
int  getNbFils_ou_Freres(cell_lvlh_t * ptCell)
 {
    int res = 0;
    cell_lvlh_t * cour = ptCell ;
    while ( cour != NULL)
    {
        cour = cour->lh;
        res++;
    }
    return res ;
}


/** TO DO
 * @brief parcours en profondeur postfixee
 * @param [in] file le flux de sortie
 * @param [in] racine la racine de l'arborescence
 */
void   printPostfixee( FILE * file , cell_lvlh_t * racine)
{
    pile_t * pile = initPile(NB_ELTPREF_MAX);
    eltType * element_pile = malloc(sizeof(eltType));
    int code = 0;
    cell_lvlh_t * cour = racine;
    while (cour != NULL)
    {
        while (cour->lv !=  NULL )
        {
            element_pile->adrCell = cour;
            element_pile->adrPrec= &cour;
            element_pile->nbFils_ou_Freres = getNbFils_ou_Freres(cour->lv);
            empiler(pile,element_pile,&code);
            cour = cour->lv;
        }
        fprintf( file, "(%c,%d) ", cour->val,getNbFils_ou_Freres(cour->lv));
        cour = cour->lh;
        while (( cour == NULL ) && (!estVidePile(pile)))
        {
            depiler(pile, element_pile, &code);
            cour = element_pile->adrCell;
            fprintf( file, "(%c,%d) ", cour->val,getNbFils_ou_Freres(cour->lv));
            cour = cour->lh;
        }
        
    }
    fprintf( file, "%d\n",getNbFils_ou_Freres(racine));
    free(element_pile);
    libererPile(&pile);
}
