/**
 * @file polynomial_main.c
 * @brief Programme pour les tests des operations sur les polynomes
 * ATTENTION : Il faut creer autant de tests que les cas d'execution pour chaque fonction a tester
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "linkedList.h"
#include "polynomial.h"
#include "teZZt.h"

BEGIN_TEST_GROUP(polynomial)

TEST(LL_init_list) {
	cell_t *list;

	printf("\nInitialization of the linked list : \n");
	LL_init_list(&list);

	REQUIRE ( list == NULL );
}


TEST(Poly_derive1) {  // exemple
	cell_t *poly = NULL;
	FILE   *file = NULL;
	char   buffer[1024];

	printf("\nDerive of polynomial 1 : \n");

	file = fmemopen(buffer, 1024, "w");
	REQUIRE ( NULL != file );
	LL_create_list_fromFileName(&poly, "polynome1.txt");
	LL_save_list_toFile(file, poly, monom_save2file);
	fclose(file);
	LL_save_list_toFile(stdout, poly, monom_save2file);
	printf("\n");
	CHECK( 0 == strcmp(buffer, "7.000 10\n9.000 7\n6.000 5\n8.000 2\n3.000 0\n") );

	file = fmemopen(buffer, 1024, "w");
	REQUIRE ( NULL != file );
	poly_derive(&poly);
	LL_save_list_toFile(file, poly, monom_save2file);
	fclose(file);
	LL_save_list_toFile(stdout, poly, monom_save2file);
	printf("\n");
	CHECK( 0 == strcmp(buffer, "70.000 9\n63.000 6\n30.000 4\n16.000 1\n") );
	LL_free_list(&poly);
}

TEST(Poly_derive) { // test sur la derivation d'un polynome
	cell_t *list=NULL;
	FILE *file=NULL;
	char buffer[1024];

	file = fmemopen(buffer, 1024, "w+");
	REQUIRE ( NULL != file );

	LL_create_list_fromFileName(&list, "polynome1.txt");
	LL_save_list_toFile(file, list,monom_save2file);
	fclose(file);
	LL_save_list_toFile(stdout, list,monom_save2file);
	printf("\n");
	CHECK( 0 == strcmp(buffer, "7.000 10\n9.000 7\n6.000 5\n8.000 2\n3.000 0\n") );

	
	
	REQUIRE ( NULL != file );
	
	poly_derive(&list);
	REQUIRE(list!=NULL);

    file = fmemopen(buffer, 1024, "w");

	LL_save_list_toFile(file, list,monom_save2file);
	
	fclose(file);

    LL_save_list_toFile(stdout, list,monom_save2file);
	printf("\n");

	CHECK( 0 == strcmp(buffer, "70.000 9\n63.000 6\n30.000 4\n16.000 1\n") );

	//liberation de la polynome
	LL_free_list(&list);
}

TEST(Poly_addition) { // test sur l'addition de deux polymones
	cell_t *list1=NULL;
	cell_t *list2=NULL;
	//cell_t **cour=NULL;
	FILE *file=NULL;
	char buffer[1024];

	//ces deux polynomes traitent  tous les cas possible 
	list1=LL_create_list_fromFileName(&list1, "polynome1.txt");
	printf("\npolynome1 :\n");
    LL_save_list_toFile(stdout, list1,monom_save2file);
	printf("\n");
	list2=LL_create_list_fromFileName(&list2, "polynome2.txt");
	printf("\npolynome2 :\n");
    LL_save_list_toFile(stdout, list2,monom_save2file);
	printf("\n");
	
	REQUIRE(list1 != NULL);
	REQUIRE(list2 != NULL);

	poly_add(&list1,&list2);

	REQUIRE(list1!=NULL);

	file = fmemopen(buffer, 1024, "w");
	REQUIRE ( NULL != file );
	LL_save_list_toFile(file, list1,monom_save2file);
	fclose(file);
	printf("\nsomme de polynome1 et polynome2 :\n");
    LL_save_list_toFile(stdout, list1,monom_save2file);
	printf("\n");

	REQUIRE(list2==NULL);
    
	CHECK( 0 == strcmp(buffer, "-3.000 10\n11.000 8\n9.000 7\n-2.100 5\n9.500 3\n10.500 2\n3.000 0\n") );
	
	//liberer la polynome list1
	LL_free_list(&list1);
}


TEST(Poly_produit) { // test sur le calcul du produit de deux polymones
	cell_t *list1=NULL;
	cell_t *list2=NULL;
	cell_t * list3=NULL;
	FILE *file=NULL;
	char buffer[1024];

	file = fmemopen(buffer, 1024, "w+");
	REQUIRE ( NULL != file );

	LL_create_list_fromFileName(&list1, "polynome1.txt");
	printf("\npolynome1 :\n");
	LL_save_list_toFile(stdout, list1,monom_save2file);
	printf("\n");
	LL_create_list_fromFileName(&list2, "polynome2.txt");
	printf("\npolynome2 :\n");
	LL_save_list_toFile(stdout, list2,monom_save2file);
	printf("\n");

	REQUIRE(list1 != NULL);
	REQUIRE(list2 != NULL);

	list3 = poly_prod(list1,list2);
	REQUIRE(list3 != NULL);
	LL_save_list_toFile(file, list3,monom_save2file);
	fclose(file);
	printf("\nproduit polynome1 et polynome2 :\n");
    LL_save_list_toFile(stdout, list3,monom_save2file);
	printf("\n");

	CHECK( 0 == strcmp(buffer, "-70.000 20\n77.000 18\n-90.000 17\n-17.700 15\n132.500 13\n-135.400 12\n94.900 10\n22.500 9\n90.000 8\n-49.800 7\n51.700 5\n20.000 4\n28.500 3\n7.500 2\n") );
	// liberer les polynomes
	LL_free_list(&list1);
	LL_free_list(&list3);
}

/*
TEST(LL_save_list_toFileName) { // test pour l'ecriture d'un polynome dans un fichier
	cell_t *list;

	//TO DO
}
*/

END_TEST_GROUP(polynomial)

int main(void) {
	RUN_TEST_GROUP(polynomial);
	return EXIT_SUCCESS;
}
