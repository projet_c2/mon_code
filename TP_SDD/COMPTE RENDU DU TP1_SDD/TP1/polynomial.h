#ifndef __POLYNOMIAL_H__
#define __POLYNOMIAL_H__


void poly_derive(cell_t **); // une fonction qui dérive un polynome donnée comme une liste 



void poly_add(cell_t **,cell_t **); // une fonction qui additionne deux polynomes données comme deux listes 


cell_t * poly_prod(cell_t *,cell_t *); // une fonction qui fait le produit de deux polynomes données comme deux listes



#endif