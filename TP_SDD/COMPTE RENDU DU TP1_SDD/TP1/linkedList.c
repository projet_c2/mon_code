
#include <stdio.h>
#include <stdlib.h>
#include "linkedList.h"


 void LL_init_list(cell_t **adrHeadPt)
{
    *adrHeadPt = NULL;
}



cell_t * LL_create_cell(monom_t *m)
{
    cell_t *new=(cell_t *)malloc((sizeof(cell_t ))); // allouer une cellule 
    if (new!=NULL){ // verifier si l'alocation est bien passée
    (*new).val.degree=(*m).degree;
    (*new).val.coef=(*m).coef;
    //mis à jour des données de la nouvelle cellule à partir de ce monome
    (*new).next =NULL;}
    return new;

 }


void LL_add_cell(cell_t **list ,cell_t *newcell)
{
    (*newcell).next=(struct cell_t *)(*list); // met à jour le pointeur nexte de nouvelle cellule par la valeur de la tete de liste
    (*list)=newcell;  // met à jour la tete de la liste par l'addresse de nouvelle cellule
}



 cell_t * LL_create_list_fromFileName(cell_t **list ,char * nom_fichier)
 {
    FILE * fich= fopen(nom_fichier,"r"); // ouvrir le fichier 
    cell_t * cellule=NULL;
    monom_t m;
    if(fich==NULL) { // tester si la fichier n'est pas ouvert
        (*list)=NULL; // pour cette cas la liste est vide
        }
    else{ // fich est bien ouvert
        while((fscanf(fich,"%lf ",&m.coef)!=EOF)&&(fscanf(fich,"%d",&m.degree)!=EOF)) // tester la lecture de degré et de coeficient et les enregistrer dans un monome m
        {
            cellule=LL_create_cell(&m); // créer une cellule pour ce monome
            if(cellule!=NULL) // tester si la création est bien passée
            {
                LL_add_cell(list,cellule); // ajouter cette cellule dans la liste
            }
        }
        fclose(fich); //fermer la fichier
    }
    return (*list);
}


void LL_save_list_toFile( FILE *file,cell_t *list , void (* monom_save2file)( FILE * ,monom_t *))
{
    cell_t * cour =list; //pointe sur la tete de liste pour  parcourir la liste  

    while(cour!=NULL){ // tester si on n'est pas à la fin de la liste
        monom_save2file(file,&((*cour).val)); // enregistrer le monome de chaque cellule  dans le fichier

        cour=(cell_t *)(cour->next);  // deplacer le pointeur cour à la cellule suivante
    }

}


void LL_save_list_toFileName(cell_t * list, char* nom_fichier,void (* LL_save_list_toFile)(FILE *,cell_t *,void (* monom_save2file)( FILE * ,monom_t *)))
{
    FILE * file=fopen(nom_fichier,"w"); // ouvrir le fichier
    LL_save_list_toFile(file,list,&(monom_save2file)); // sauvegarder  la liste dans le fichier ouvert
    fclose(file); // fermer le fichier
}

 
cell_t ** LL_search_prev(cell_t ** list,monom_t * m,int (* monom_degree_cmp)(monom_t *,monom_t *))
{
    
    cell_t ** ad_el_rech=list; // l'addresse de l'element recherchée
    cell_t *cour=NULL; // pour parcourir la liste 
    if(list!=NULL) // tester si la liste est vide ou non
    {
        cour= *list;  // placer le pointeur sur la tete de la liste
        while((cour!=NULL)&&(monom_degree_cmp(&(cour->val),m)!=0))  // tester si on est à la fin de liste ou si le monome de cellule  est de degré inferiure à le degré de m
        {
            ad_el_rech=(cell_t **) (&(cour->next)); // deplacer la ad_el_rech à la cellule suivante
            cour=(cell_t *)(cour->next);            // deplacer la cour à la cellule suivante
        }
    }
    
    return ad_el_rech;
}




 void  LL_del_cell(cell_t ** list)
 {
    if (list!=NULL)  // tester si la liste est vide ou non
    { 
        cell_t * cour=*list;  // cour pointe sur la tete de la liste
        if(cour!=NULL)  // tester si la liste est vide ou non
        {
            (*list)=(cell_t *)((*list)->next); // deplacer le pointeur de tete à l'element suivant
            free(cour);                  // liberer la case cour
        }
    }
 }




void LL_free_list(cell_t **list) //liberer la liste
{
    cell_t * cellule=NULL;
    while(*list!=NULL){  //parcourir la list
        cellule= *list; // conserver l'addresse de l'element actuel
        *list = (cell_t *)((*list)->next); // deplacer la pointeur à l'element suivant
        free(cellule);   // liberer l'addresse de cet element
    }
}
