
#include <stdio.h>
#include "valCell.h"



 int monom_degree_cmp(monom_t *m1,monom_t *m2) // fonction qui compare deux monomes selon leurs degrés
 {
    return (*m1).degree-((*m2).degree); // qui est 0 si ils ont le méme degré et negative si degré de m1 inferieur a celle de m2 et positive dans la derniere cas
}



void monom_save2file( FILE * file,monom_t *m)
{
    fprintf(file,"%.3f %d\n", (*m).coef,(*m).degree); // enregistrer dans le fichier le coeficient de m avec trois nombre apres virgule puis le degré de m dans le meme ligne espacé par un espace
} 
