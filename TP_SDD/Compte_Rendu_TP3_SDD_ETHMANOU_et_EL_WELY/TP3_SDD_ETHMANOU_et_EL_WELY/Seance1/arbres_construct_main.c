/**
 * program for general linked list testing
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../pile.h"
#include "../eltsArbre.h"
#include "arbres_construct.h"
#include "../teZZt.h"


BEGIN_TEST_GROUP(ARBRE_CONSTRUCT)

TEST(nouvCell) {
	cell_lvlh_t *new = NULL;

	new = allocPoint('A');
	REQUIRE( NULL != new ); 
	CHECK( 'A' == new->val );
	CHECK( NULL == new->lv );
	CHECK( NULL == new->lh );

	free(new);
}


TEST(lirePref_fromFileName_exTP) {
	int nbRacines = 0;
	int nbEltsPref = 0;
	eltPrefPostFixee_t tabEltPref[NB_ELTPREF_MAX];

	nbRacines = lirePref_fromFileName("../pref_exTP.txt", tabEltPref, &nbEltsPref);

	printf("\033[34m\nlirePref_fromFileName_exTP :");
	printf("\033[0m\n");

	CHECK( 2 == nbRacines ); 
	CHECK( 13 == nbEltsPref );

	CHECK( 'A' == tabEltPref[0].val );
	CHECK( 3 == tabEltPref[0].nbFils );

	CHECK( 'B' == tabEltPref[1].val );
	CHECK( 2 == tabEltPref[1].nbFils );

	CHECK( 'E' == tabEltPref[2].val );
	CHECK( 0 == tabEltPref[2].nbFils );

	CHECK( 'J' == tabEltPref[3].val );
	CHECK( 0 == tabEltPref[3].nbFils );

	CHECK( 'D' == tabEltPref[4].val );
	CHECK( 0 == tabEltPref[4].nbFils );

	CHECK( 'H' == tabEltPref[5].val );
	CHECK( 1 == tabEltPref[5].nbFils );

	CHECK( 'G' == tabEltPref[6].val );
	CHECK( 0 == tabEltPref[6].nbFils );

	CHECK( 'C' == tabEltPref[7].val );
	CHECK( 2 == tabEltPref[7].nbFils );

	CHECK( 'F' == tabEltPref[8].val );
	CHECK( 3 == tabEltPref[8].nbFils );

	CHECK( 'K' == tabEltPref[9].val );
	CHECK( 0 == tabEltPref[9].nbFils );

	CHECK( 'M' == tabEltPref[10].val );
	CHECK( 0 == tabEltPref[10].nbFils );

	CHECK( 'T' == tabEltPref[11].val );
	CHECK( 0 == tabEltPref[11].nbFils );
	
	CHECK( 'I' == tabEltPref[nbEltsPref-1].val );
	CHECK( 0 == tabEltPref[nbEltsPref-1].nbFils );
}

TEST(printTabEltPref_exTP) {
	int nbRacines = 0;
	int nbEltsPref = 0;
	eltPrefPostFixee_t tabEltPref[NB_ELTPREF_MAX];

	char buffer[1024];
	FILE * file = fmemopen(buffer, 1024, "w");
	REQUIRE ( NULL != file);

	printf("\033[34m\nprintPref_exTP :");
	printf("\033[0m\n");

	nbRacines = lirePref_fromFileName("../pref_exTP.txt", tabEltPref, &nbEltsPref);
	fprintf(file, "%d ", nbRacines);
	printTabEltPref(file, tabEltPref, nbEltsPref);
	fclose(file);
	CHECK( 0 == strcmp(buffer, "2 (A,3) (B,2) (E,0) (J,0) (D,0) (H,1) (G,0) (C,2) (F,3) (K,0) (M,0) (T,0) (I,0)\n") ); 
}

// 2  (A,3)  (B,2)  (E,0)  (J,0)  (D,0)  (H,1) (G,0)  (C,2)  (F,3)  (K,0)  (M,0)  (T,0)  (I,0)
TEST(pref2lvlh1_exTP) {
	int nbRacines = 0;
	int nbEltsPref = 0;
	eltPrefPostFixee_t tabEltPref[NB_ELTPREF_MAX];
	cell_lvlh_t *racine = NULL;
	
	char buffer[1024];
	FILE * file = fmemopen(buffer, 1024, "w");
	REQUIRE ( NULL != file);

	printf("\033[35m\npref2lvlh1_exTP :");
	printf("\033[0m\n");

	nbRacines = lirePref_fromFileName("../pref_exTP.txt", tabEltPref, &nbEltsPref);

	//cell_lvlh_t * adrtete = NULL;

	racine = pref2lvlh(tabEltPref, nbRacines );

	CHECK( 'A' == racine->val );
	
	CHECK( 'B'== (racine->lv)->val);
	CHECK( 'E' == ((racine->lv)->lv)->val);
	CHECK( NULL ==((racine->lv)->lv)->lv );
	CHECK( 'J' == (((racine->lv)->lv)->lh)->val);
	CHECK( NULL == (((racine->lv)->lv)->lh)->lv);
	CHECK( NULL == (((racine->lv)->lv)->lh)->lh);

	CHECK( 'D' == ((racine->lv)->lh)->val);
	CHECK( NULL == ((racine->lv)->lh)->lv);

	CHECK( 'H' == (((racine->lv)->lh)->lh)->val);
	CHECK( NULL == (((racine->lv)->lh)->lh)->lh);
	CHECK( 'G' == ((((racine->lv)->lh)->lh)->lv)->val);
	CHECK( NULL == ((((racine->lv)->lh)->lh)->lv)->lv);
	CHECK( NULL == ((((racine->lv)->lh)->lh)->lv)->lh);
    
	CHECK( 'C' == (racine->lh)->val );
	CHECK( NULL == (racine->lh)->lh );

	CHECK( 'F' == ((racine->lh)->lv)->val );
	CHECK( 'K' == (((racine->lh)->lv)->lv)->val );
	CHECK( NULL == (((racine->lh)->lv)->lv)->lv );
	CHECK( 'M' == ((((racine->lh)->lv)->lv)->lh)->val );
	CHECK( NULL == ((((racine->lh)->lv)->lv)->lh)->lv );
	CHECK( 'T' == (((((racine->lh)->lv)->lv)->lh)->lh)->val );
	CHECK( NULL == (((((racine->lh)->lv)->lv)->lh)->lh)->lv );
	CHECK( NULL == (((((racine->lh)->lv)->lv)->lh)->lh)->lh );

	CHECK( 'I' == (((racine->lh)->lv)->lh)->val );
	CHECK( NULL == (((racine->lh)->lv)->lh)->lv );
	CHECK( NULL == (((racine->lh)->lv)->lh)->lh );

    fclose(file);
	libererArbre(&racine);
}


END_TEST_GROUP(ARBRE_CONSTRUCT)

int main(void) {
	RUN_TEST_GROUP(ARBRE_CONSTRUCT);
	return EXIT_SUCCESS;
}
