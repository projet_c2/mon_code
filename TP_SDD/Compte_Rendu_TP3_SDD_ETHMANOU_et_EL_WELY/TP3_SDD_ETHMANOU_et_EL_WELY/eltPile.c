/**
 * @file eltPile.c
 * @brief fichier d'implementation pour la gestion du type des elements de pile
 */
#include <stdio.h>

#include "eltPile.h"

/** TO DO
 * @brief Copier la valeur d'un element dans un autre emplacement
 * @param [in] src l'adresse de l'element a copier
 * @param [in] dest l'adresse de la destination
 */
void copyElt ( eltType * src, eltType * dest)
{
    if ( src != NULL){ // tester si l'element est non nul
        dest->adrCell = src->adrCell; // copier 
        dest->adrPrec = src->adrPrec; // copier 
        dest->nbFils_ou_Freres = src->nbFils_ou_Freres; // copier
    }
}

