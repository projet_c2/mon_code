/**
 * program for general linked list testing
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../pile.h"
#include "../eltsArbre.h"
#include "../Seance1/arbres_construct.h"
#include "arbres_parcours.h"
#include "../teZZt.h"


BEGIN_TEST_GROUP(ARBRE_PARCOURS)

TEST(nouvCell) {
	cell_lvlh_t *new;
	cell_lvlh_t *new1;
	cell_lvlh_t *new2;
	cell_lvlh_t *new3;



	new = allocPoint('A');
	REQUIRE( NULL != new ); 
	CHECK( 'A' == new->val );
	CHECK( NULL == new->lv );
	CHECK( NULL == new->lh );

	new1 = allocPoint('B');
	REQUIRE( NULL != new1 ); 
	CHECK( 'B' == new1->val );
	CHECK( NULL == new1->lv );
	CHECK( NULL == new1->lh );

	new2 = allocPoint('C');
	REQUIRE( NULL != new1 ); 
	CHECK( 'C' == new2->val );
	CHECK( NULL == new2->lv );
	CHECK( NULL == new2->lh );

	new3 = allocPoint('X');
	REQUIRE( NULL != new3 ); 
	CHECK( 'X' == new3->val );
	CHECK( NULL == new3->lv );
	CHECK( NULL == new3->lh );

	free(new);
	free(new1);
	free(new2);
	free(new3);
}


TEST(getNbFils_ou_Freres) {
	int nbRacines = 0;
	int nbEltsPref = 0;
	eltPrefPostFixee_t tabEltPref[NB_ELTPREF_MAX];
	cell_lvlh_t *racine = NULL;
	
	char buffer[1024];
	FILE * file = fmemopen(buffer, 1024, "w");
	REQUIRE ( NULL != file);

	printf("\033[35m\ngetNbFils_ou_Freres :");
	printf("\033[0m\n");

	nbRacines = lirePref_fromFileName("../pref_exTP.txt", tabEltPref, &nbEltsPref);
	racine = pref2lvlh(tabEltPref, nbRacines);

	REQUIRE( NULL != racine );
	printf("A = %c\n", racine->val);
	CHECK('A'==racine->val);
	CHECK( 2 == getNbFils_ou_Freres(racine) );     // 2 freres y compris lui-meme
	CHECK( 3 == getNbFils_ou_Freres(racine->lv) ); // 3 fils

	REQUIRE( NULL != racine->lv );
	printf("B = %c\n", racine->lv->val);
	CHECK('B'==racine->lv->val);
	CHECK( 3 == getNbFils_ou_Freres(racine->lv) );     // 3 freres y compris lui-meme
	CHECK( 2 == getNbFils_ou_Freres(racine->lv->lv) ); // 2 fils

	REQUIRE( NULL != racine->lv->lv );
	printf("E = %c\n", racine->lv->lv->val);
	CHECK('E'==racine->lv->lv->val);
	CHECK( 0 == getNbFils_ou_Freres(racine->lv->lv->lv) ); // 0 fils

	REQUIRE( NULL != racine->lv->lv->lh );
	printf("J = %c\n", racine->lv->lv->lh->val);
	CHECK('J'==racine->lv->lv->lh->val);
	CHECK( 0 == getNbFils_ou_Freres(racine->lv->lv->lv) ); // 0 fils

	REQUIRE( NULL != racine->lv->lh );
	printf("D = %c\n", racine->lv->lh->val);
	CHECK('D'==racine->lv->lh->val);
	CHECK( 0 == getNbFils_ou_Freres(racine->lv->lh->lv) ); // 0 fils

	REQUIRE( NULL != racine->lv->lh->lh );
	printf("H = %c\n", racine->lv->lh->lh->val);
	CHECK('H'==racine->lv->lh->lh->val);
	CHECK( 1 == getNbFils_ou_Freres(racine->lv->lh->lh));
	CHECK( 1 == getNbFils_ou_Freres(racine->lv->lh->lh->lv) ); // 1 fils

    fclose(file);
	libererArbre(&racine);
}

TEST(printPostfixee) {
	int nbRacines = 0;
	int nbEltsPref = 0;
	eltPrefPostFixee_t tabEltPref[NB_ELTPREF_MAX];
	cell_lvlh_t *racine = NULL;
	
	char buffer[1024];
	FILE * file = fmemopen(buffer, 1024, "w");
	REQUIRE ( NULL != file);

	printf("\033[35m\nprintPostFixee :");
	printf("\033[0m\n");

	nbRacines = lirePref_fromFileName("../pref_exTP.txt", tabEltPref, &nbEltsPref);
	racine = pref2lvlh(tabEltPref, nbRacines);

	printPostfixee(stdout, racine);
	printPostfixee(file, racine);
	fclose(file);
	CHECK( 0 == strcmp(buffer,"(E,0) (J,0) (B,2) (D,0) (G,0) (H,1) (A,3) (K,0) (M,0) (T,0) (F,3) (I,0) (C,2) 2\n") );
	
	libererArbre(&racine);
}


END_TEST_GROUP(ARBRE_PARCOURS)

int main(void) {
	RUN_TEST_GROUP(ARBRE_PARCOURS);
	return EXIT_SUCCESS;
}
