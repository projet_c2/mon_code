/**
 * @file pile.c
 * @brief fichier d'implementation pour la gestion de pile
 */
 
#include <stdlib.h>
#include "pile.h"

/** TO DO
 * @brief Initialiser une pile du type pile_t
 *   - allocation de memoire pour une structure pile_t et un tableau de taille elements
 * @param [in] taille taille de la pile
 * @return l'adresse de la structure
 */
pile_t * initPile(int  taille ){
    pile_t * PtPile=NULL;
    if (taille==0){
        PtPile=NULL;
    }
    else{
        PtPile=malloc(sizeof(pile_t));
        PtPile->base = malloc(taille*sizeof(eltType));
        PtPile->sommet=-1;
        PtPile->taille=taille;
    }
    return PtPile;
}



/** TO DO
 * @brief Liberer les memoires occupees par la pile
 * @param [in, out] adrPtPile l'adresse du pointeur de la structure pile_t
 */
void  libererPile(pile_t ** adrPtPile)
{
    if ((*adrPtPile)!=NULL)
    {
        free((*adrPtPile)->base);
        free((*adrPtPile));
        (*adrPtPile)=NULL;
    }

}

/** TO DO
 * @brief Verifier si la pile est vide (aucun element dans la "base")
 * @param [in] ptPile l'adresse de la structure pile_t
 * @return 1 - vide, ou 0 - non vide
 */
int estVidePile(pile_t *  ptPile)
{
    int res=1;
    if(ptPile!=NULL){
        if(ptPile->sommet!=-1){
            res = 0;
        }
    }
    return res;
}


/** TO DO
 * @brief Entrer un element dans la pile
 * @param [in, out] ptPile l'adresse de la structure pile_t
 * @param [in] ptVal l'adresse de la valeur a empiler
 * @param [in, out] code : l'adresse du code de sortie
 *                     *code = 0 si reussi
 *                           = 1 si echec
 */
void empiler(pile_t * ptPile , eltType * ptVal , int * code )
{
    if ((ptPile!=NULL))
    {
        if((ptPile->sommet<ptPile->taille-1))
        {
            ptPile->sommet++;
            copyElt(ptVal,(ptPile->base + ptPile->sommet));
            (*code) = 0;
        }
        else{
            (*code)=1;
        }

    }
}


/** TO DO
 * @brief Sortir un element de la pile
 * @param [in, out] ptPile l'adresse de la structure pile_t
 * @param [out] ptRes l'adresse de l'element sorti
 * @param [in, out] code : l'adresse du code de sortie
 *                     *code = 0 si reussi
 *                           = 1 si echec
 */
void depiler(pile_t * ptPile , eltType * ptRes , int  * code )
{
   
    if(!estVidePile(ptPile))
    {
        (*code) = 0;
        copyElt((ptPile->base + ptPile->sommet),ptRes);
         ptPile->sommet=ptPile->sommet-1;
            
    }
    else{
        (*code)=1;
    }

    
}


