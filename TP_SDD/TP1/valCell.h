

#ifndef __VALCELL_H__
#define __VALCELL_H__

#include <stdio.h>




typedef struct{
    double coef;
    int degree;
} monom_t;


int monom_degree_cmp(monom_t *,monom_t *); // comparer les degrés de deux monomes



void monom_save2file( FILE * , monom_t *); // enregistrer la monome dans un fichier

#endif
