/**
 * program for general linked list testing
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../pile.h"
#include "../eltsArbre.h"
#include "../Seance1/arbres_construct.h"
#include "arbres_insert.h"
#include "../teZZt.h"


BEGIN_TEST_GROUP(ARBRE_INSERT)

TEST(nouvCell) {
	cell_lvlh_t *new;

	new = allocPoint('A');
	REQUIRE( NULL != new ); 
	CHECK( 'A' == new->val );
	CHECK( NULL == new->lv );
	CHECK( NULL == new->lh );

	free(new);
}


TEST(rechercher_v) {
	int nbRacines = 0;
	int nbEltsPref = 0;
	eltPrefPostFixee_t tabEltPref[NB_ELTPREF_MAX];
	cell_lvlh_t *racine = NULL;
	cell_lvlh_t *pere = NULL;

	printf("\033[35m\nrechercher_v :");
	printf("\033[0m\n");

	nbRacines = lirePref_fromFileName("../pref_exTP.txt", tabEltPref, &nbEltsPref);
	racine = pref2lvlh(tabEltPref, nbRacines);

	pere = rechercher_v(racine, 'A');   // valeur a la racine
	REQUIRE( NULL != pere );
	CHECK( 'A' == pere->val );

	pere = rechercher_v(racine, 'B');   // valeur existante
	REQUIRE( NULL != pere );
	CHECK( 'B' == pere->val );

	pere = rechercher_v(racine, 'C');   // valeur existante
	REQUIRE( NULL != pere );
	CHECK( 'C' == pere->val );

	pere = rechercher_v(racine, 'D');   // valeur existante
	REQUIRE( NULL != pere );
	CHECK( 'D' == pere->val );

	pere = rechercher_v(racine, 'E');   // valeur existante
	REQUIRE( NULL != pere );
	CHECK( 'E' == pere->val );

	pere = rechercher_v(racine, 'F');   // valeur existante
	REQUIRE( NULL != pere );
	CHECK( 'F' == pere->val );

	pere = rechercher_v(racine, 'G');   // valeur existante
	REQUIRE( NULL != pere );
	CHECK( 'G' == pere->val );

	pere = rechercher_v(racine, 'H');   // valeur existante
	REQUIRE( NULL != pere );
	CHECK( 'H' == pere->val );

	pere = rechercher_v(racine, 'I');   // valeur existante
	REQUIRE( NULL != pere );
	CHECK( 'I' == pere->val );

	pere = rechercher_v(racine, 'J');   // valeur existante
	REQUIRE( NULL != pere );
	CHECK( 'J' == pere->val );

	pere = rechercher_v(racine, 'K');   // valeur existante
	REQUIRE( NULL != pere );
	CHECK( 'K' == pere->val );

	pere = rechercher_v(racine, 'L');   // valeur inexistante
	CHECK( NULL == pere );

	pere = rechercher_v(racine, 'M');   // valeur existante
	REQUIRE( NULL != pere );
	CHECK( 'M' == pere->val );


	pere = rechercher_v(racine, 'N');   // valeur inexistante
	CHECK( NULL == pere );

	pere = rechercher_v(racine, 'O');   // valeur inexistante
	CHECK( NULL == pere );

	pere = rechercher_v(racine, 'P');   // valeur inexistante
	CHECK( NULL == pere );

	pere = rechercher_v(racine, 'Q');   // valeur inexistante
	CHECK( NULL == pere );

	pere = rechercher_v(racine, 'R');   // valeur inexistante
	CHECK( NULL == pere );

	pere = rechercher_v(racine, 'S');   // valeur inexistante
	CHECK( NULL == pere );

	pere = rechercher_v(racine, 'T');   // valeur existante
	REQUIRE( NULL != pere );
	CHECK( 'T' == pere->val );

	pere = rechercher_v(racine, 'U');   // valeur inexistante
	CHECK( NULL == pere );

	pere = rechercher_v(racine, 'V');   // valeur inexistante
	CHECK( NULL == pere );

	pere = rechercher_v(racine, 'W');   // valeur inexistante
	CHECK( NULL == pere );

	pere = rechercher_v(racine, 'X');   // valeur inexistante
	CHECK( NULL == pere );

	pere = rechercher_v(racine, 'Y');   // valeur inexistante
	CHECK( NULL == pere );

	pere = rechercher_v(racine, 'Z');   // valeur inexistante
	CHECK( NULL == pere );


	libererArbre(&racine);
}


TEST(rechercherPrecFilsTries) {
	int nbRacines = 0;
	int nbEltsPref = 0;
	eltPrefPostFixee_t tabEltPref[NB_ELTPREF_MAX];
	cell_lvlh_t *racine = NULL;
	cell_lvlh_t *pere = NULL;
	cell_lvlh_t **pprec = NULL;

	printf("\033[34m\nrechercherPrecFilsTries :");
	printf("\033[0m\n");
	nbRacines = lirePref_fromFileName("../pref_exTP.txt", tabEltPref, &nbEltsPref);
	racine = pref2lvlh(tabEltPref, nbRacines);
/************* test de point A **********/
	pere = rechercher_v(racine, 'A');
	REQUIRE( NULL != pere );
	CHECK( 'A' == pere->val );

	pprec = rechercherPrecFilsTries(pere, 'B');
	REQUIRE( NULL != *pprec );
	CHECK('B' == (*pprec)->val);

	pprec = rechercherPrecFilsTries(pere, 'C');
	REQUIRE( NULL != *pprec );
	CHECK('D' == (*pprec)->val);

	pprec = rechercherPrecFilsTries(pere, 'F');
	REQUIRE( NULL != *pprec );
	CHECK('H' == (*pprec)->val);

	pprec = rechercherPrecFilsTries(pere, 'E');
	REQUIRE( NULL != *pprec );
	CHECK('H' == (*pprec)->val);



/************* test de point B **********/

    pere = rechercher_v(racine, 'B');
	REQUIRE( NULL != pere );
	CHECK( 'B' == pere->val );

	pprec = rechercherPrecFilsTries(pere, 'A');
	REQUIRE( NULL != *pprec );
	CHECK('E' == (*pprec)->val);

	pprec = rechercherPrecFilsTries(pere, 'C');
	REQUIRE( NULL != *pprec );
	CHECK('E' == (*pprec)->val);

	pprec = rechercherPrecFilsTries(pere, 'D');
	REQUIRE( NULL != *pprec );
	CHECK('E' == (*pprec)->val);

	pprec = rechercherPrecFilsTries(pere, 'E');
	REQUIRE( NULL != *pprec );
	CHECK('E' == (*pprec)->val);

	pprec = rechercherPrecFilsTries(pere, 'F');
	REQUIRE( NULL != *pprec );
	CHECK('J' == (*pprec)->val);

	pprec = rechercherPrecFilsTries(pere, 'G');
	REQUIRE( NULL != *pprec );
	CHECK('J' == (*pprec)->val);

	pprec = rechercherPrecFilsTries(pere, 'H');
	REQUIRE( NULL != *pprec );
	CHECK('J' == (*pprec)->val);

	pprec = rechercherPrecFilsTries(pere, 'I');
	REQUIRE( NULL != *pprec );
	CHECK('J' == (*pprec)->val);
	

	pprec = rechercherPrecFilsTries(pere, 'X');
	REQUIRE( NULL == *pprec );

/************* test de point G **********/
     pere = rechercher_v(racine, 'H');
	REQUIRE( NULL != pere );
	CHECK( 'H' == pere->val );

	pprec = rechercherPrecFilsTries(pere, 'B');
	REQUIRE( NULL != *pprec );
	CHECK('G' == (*pprec)->val);

	pprec = rechercherPrecFilsTries(pere, 'C');
	REQUIRE( NULL != *pprec );
	CHECK('G' == (*pprec)->val);

	pprec = rechercherPrecFilsTries(pere, 'D');
	REQUIRE( NULL != *pprec );
	CHECK('G' == (*pprec)->val);

	pprec = rechercherPrecFilsTries(pere, 'E');
	REQUIRE( NULL != *pprec );
	CHECK('G' == (*pprec)->val);


/************* test de point F **********/
	pere = rechercher_v(racine, 'F');
	REQUIRE( NULL != pere );
	CHECK( 'F' == pere->val );

	pprec = rechercherPrecFilsTries(pere, 'A');
	REQUIRE( NULL != *pprec );
	CHECK( 'K' == (*pprec)->val );

	pprec = rechercherPrecFilsTries(pere, 'L');
	REQUIRE( NULL != *pprec );
	CHECK( 'M' == (*pprec)->val );

	
	
	libererArbre(&racine);
}

TEST(insererTrie) 
{
    int nbRacines = 0;
	int nbEltsPref = 0;
	int code = 0;
	eltPrefPostFixee_t tabEltPref[NB_ELTPREF_MAX];
	cell_lvlh_t *racine = NULL;
	cell_lvlh_t *pere = NULL;
	cell_lvlh_t **pprec = NULL;

	printf("\033[34m\nrechercherPrecFilsTries :");
	printf("\033[0m\n");
	nbRacines = lirePref_fromFileName("../pref_exTP.txt", tabEltPref, &nbEltsPref);
	racine = pref2lvlh(tabEltPref, nbRacines);
	
/******** insertion dans les fils de A *******/	
    code = insererTrie(racine,'A','C');
	CHECK(code == 1);

	pere = rechercher_v(racine, 'A');
	REQUIRE( NULL != pere );
	CHECK( 'A' == pere->val );

	pprec = rechercherPrecFilsTries(pere, 'C');
	REQUIRE( NULL != *pprec );
	
	CHECK('C' == (*pprec)->val);
	CHECK('D'==((*pprec)->lh)->val);
/******** insertion dans les fils de F *******/
	code = insererTrie(racine,'F','A');
	CHECK(code == 1);

	pere = rechercher_v(racine, 'F');
	REQUIRE( NULL != pere );
	CHECK( 'F' == pere->val );

	pprec = rechercherPrecFilsTries(pere, 'A');
	REQUIRE( NULL != *pprec );

	CHECK('A' == (*pprec)->val);
	CHECK('K'==((*pprec)->lh)->val);


	libererArbre(&racine);


}

END_TEST_GROUP(ARBRE_INSERT)

int main(void) {
	RUN_TEST_GROUP(ARBRE_INSERT);
	return EXIT_SUCCESS;
}
