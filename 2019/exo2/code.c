#include "code.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


void stats1(char *texte, int *nb_chars, int * nb_lignes, int * nb_mots){
  int i=0, taille = strlen(texte);
  *nb_chars = taille;   *nb_mots = 0;  *nb_lignes = 1;//initialisation de *nb_mots et *nb_lignes //
  while(i<taille){
    if (texte[i]=='\n')    (*nb_lignes)++;
    if(((texte[i]>64) && (texte[i]<91)) && ((texte[i+1]<65) || (texte[i+1]>90)))    (*nb_mots)++; // si je trouve un lettre suivi par un caractere non lettre c'est un mot//
    i++;
  } 
}


long taille_fichier(char * nom) {
  FILE * f  = fopen(nom, "rb");
  long r = 0;
  if (f) {
    fseek(f, 0, SEEK_END);
    r = ftell(f);
    fclose(f);
  }
  return r;
}


char * lecture(char * nom) {
  int i=0, n=taille_fichier(nom);
  char * tx =NULL ;
  FILE * fichier = fopen(nom, "r");
  if (fichier != NULL){
   tx = (char *)malloc(n*sizeof(char *));
    while(i<n){
      tx[i] = fgetc(fichier); // lire le fichier caracter par caracter//
      i++;
    }
    tx[i] = '\0';
    fclose(fichier);
  }
  
  return tx;
}


void initialiser( info * infos) {
     char * tab = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; // pour faciliter les recuperations des lettres//
     int i=0, j=0;
     for(i=0 ; i<26; i++){
      infos[i].lettre = tab[i];
      infos[i].nb = 0;
      infos[i].nbd =0;
      for(j=0; j<MAX ; j++){
        strcpy(infos[i].mots[j] , "");
      }
     }
}

void inserer(info * infos, char * mot ){
  int i=0 , j=0 , l=0, k=0;
  // i : pour trouver la lettre de mot ; 
  while(infos[i].lettre != mot[0]){   i++;}
  while(((strcmp(infos[i].mots[j], mot)<0) && (strcmp(infos[i].mots[j], "") != 0))){ j++;}
  if ((strcmp(infos[i].mots[j] , mot)==0)){
    infos[i].nb ++;
  }
  else {
    l=j;
    while(strcmp(infos[i].mots[l], "") != 0){ l++;}
    k = l;
    while(k>j){     strcpy(infos[i].mots[k],infos[i].mots[k-1]); k=k-1;}
    strcpy(infos[i].mots[j], mot);
    infos[i].nb++;
    infos[i].nbd++;
  }
} 

void stats2(char *texte, info * infos, int *nb_chars, int * nb_lignes, int * nb_mots){
  int i=0,counter = 0, taille = 0; // counter : pour counter le nombre de lettre d'un mot //
  taille = strlen(texte);
  *nb_chars = taille; *nb_mots = 0; *nb_lignes = 1;
  char tab[MAX][50];  // pour memoriser les mots dans un tableau pour l'inserer dans infos//
  initialiser(infos);
  while(i<taille){
    if (texte[i]=='\n'){  (*nb_lignes)++; 
    }
    counter = 0;
    while((i<taille) && ((texte[i]>64) && (texte[i]<91))){
        tab[*nb_mots][counter] = texte[i];
        counter++;
        i++;
    }
    if (counter>0){
      tab[*nb_mots][counter] = '\0';
      inserer(infos , tab[*nb_mots]);
      (*nb_mots)++;
      i=i-1;
    }
    i=i+1;
  }
} 

int total_mots(info * infos ) {
  int i=0 , nb_mots_total = 0;
  for(i=0 ; i<26 ; i++){
    nb_mots_total = nb_mots_total +  infos[i].nb;
  }
  return nb_mots_total;
}

int total_mots_distincts(info * infos) {
  int i=0 , nb_mots_dinstincts = 0;
  for(i=0 ; i<26 ; i++){
    nb_mots_dinstincts = nb_mots_dinstincts +  infos[i].nbd;
  }
  return nb_mots_dinstincts;
}

