#include "code.h"
#include <string.h>
#include <math.h>

void init(char * tab, int taille, char value) 
{
    int i=0;
    for(i=0 ; i<taille;i++){
        tab[i] = value;
    }
    tab[taille]='\0';
	
}  

void inverser(char * tab) 
{
    int taille = strlen(tab);
    int i=0 , milieu=taille/2;
    char c='0';
    while(i<milieu){
        c=tab[i];
        tab[i] = tab[taille-i-1];
        tab[taille-i-1] = c;
        i++;
    }

}

void conversiond2b(int nb, char * tab) 
{
    int i=0;
    if (nb==0){
        tab[0]='0';
        tab[1]=0;
    }
    while(nb>0){
        if (nb%2==0)   tab[i] = '0';
        else           tab[i] = '1';
        i=i+1;
        nb = nb/2;

    }
    if (i!=0)        tab[i]=0;
    inverser(tab);
}

int conversionb2d(const char * tab)
{
	int taille=strlen(tab), i=0;
    int nb=0;
    while(i<taille){
        if(tab[i]=='1')     nb = nb + pow(2, taille-i-1);
        i++;
    }
	return nb;

}

char * addition(char *r, char *a, char * b) {
    int nb1 = conversionb2d(a), nb2 = conversionb2d(b);
    int nb3 = nb1 + nb2;
    conversiond2b(nb3 , r);
    return r;
}	

