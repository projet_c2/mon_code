#include "code.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


char devinette[255] = "JEDETESTELEC";


void majuscules(char * s) {
   int i=0;
   while(*(s+i)!= '\0'){
      if ((*(s+i)>='a') && (*(s+i)<='z'))    *(s+i) = *(s+i) -32;
      i=i+1;
   }
}

void multitap1(char * d, char * s) {
   int i=0, j=0, a=0;
   char tab[32] = "ABC*DEF*GHI*JKL*MNO*PQRSTUV*WXYZ";
   char res[1024];
   strcpy(d , "");
   majuscules(s);
   int n = strlen(s);
   while((i<n)){
      j=0;
      while ((tab[j]!= '\0') && (tab[j]!= s[i])){
         j = j+1;
      }

      a = (j/4) +2;
      if(tab[j-(j%4)+4]=='*'){
         if(j%4==0){
            snprintf(res,1024,"%d ",a);
            strcat(d,res);
         }
         if(j%4==1) {
            snprintf(res,1024,"%d%d ",a,a);
            strcat(d,res);
         }
         if(j%4==2){
            snprintf(res,1024,"%d%d%d ",a, a , a);
            strcat(d,res);
         }
      }
      else {
         if(j%4==0){
            snprintf(res,1024,"%d ",a);
            strcat(d,res);
         }
         if(j%4==1) {
            snprintf(res,1024,"%d%d ",a,a);
            strcat(d,res);
         }
         if(j%4==2){
            snprintf(res,1024,"%d%d%d ",a, a , a);
            strcat(d,res);
         }
         if(j%4==3){
            snprintf(res,1024,"%d%d%d%d ",a, a , a, a);

            strcat(d,res);
         }
   }
   i=i+1;


   }
   d[strlen(d)-1]='\0';
}



void multitap2(char * s) {
   char d[1024];
   multitap1(d,s);
   char res_0[49];
   char res[49]="";
   int n= strlen(d), i=0, j=0, bol=0;
   char c='1';
   while(i<n){
      bol=0;
      j= i+1 ;
      while((j<n) && (d[j]!=' ')){
         j=j+1;
      }
      bol = j-i;
      c = d[i];
      if (bol == 1){
         snprintf(res_0 , 45, "%c",c);
         strcat(res,res_0);
      }
      else{
         snprintf(res_0,45, "%d*%c",bol,c);
         strcat(res, res_0);
      }
      i = j+1;
   }
   strcpy(s, res);
}

void antitap1(char *d, char * s) {
   int i=0,j=0 , n= strlen(s);
   char res[50] = "";
   char tab[32] = "ABC*DEF*GHI*JKL*MNO*PQRSTUV*WXYZ";
   int a=0 , k=0;
   strcpy(d,"");
   while(i<n){
      j= i+1 ;
      while((j<n) && (s[j]!=' ')){
         j=j+1;
      }
      a=s[i]-48;
      k = (a-2)*4;
      snprintf(res , 5,"%c" , tab[k+j-i-1]);
      strcat(d , res);


      i = j+1;
   }
}

void antitap2(char * s) {
   char  d[50] ="";
   char res[5] = "";
   char tab[32] = "ABC*DEF*GHI*JKL*MNO*PQRSTUV*WXYZ";
   int a=0 , k=0;
   int i=0, j=0, bol =0 , n=strlen(s);
   while(i<n){
      j=i+1;
      if (s[j]=='*'){  bol = s[i] -48;
                       a = s[j+1] -48;
                       i=j+1;}
      else{          a = s[i]-48;
                     bol = 1;}

      k = (a-2)*4;

      snprintf(res , 5,"%c" , tab[k+bol-1]);
      strcat(d , res);

      i=i+1;
   }
   strcpy(s ,d);
}



char * sms(char * nom) {
   char * s = NULL;
   FILE * f = fopen(nom , "r");
   if (f!=NULL){
      char  d[1024];
      char tab[100][50];
      int i=0, j=0;
      s = (char *)malloc(100*sizeof(char));
      strcpy(s , "");
      while((fscanf(f , "%s", tab[i])==1)){
            i++;
      }
   fclose(f);
   for(j=0; j<i-1; j++){
      multitap1(d, tab[j]);
      strcat(s, d);
      strcat(s, "\n");
   }
   multitap1(d, tab[j]);
   strcat(s, d);

   
   }

   return s;
}
















/*
void multitap1(char * d, char * s) {
   int i=0, j=0, a=0, bol=0;
   char tab[26] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
   char res[1024];
   strcpy(d , "");
   majuscules(s);
   int n = strlen(s);
   while((i<n)){
      j=0;(j==18) 

      a = (j/3) +2;
      if (j==21)  a=8;
      if (j==25)  a=9;
      if ( (a!=7) && (a!=9) ){
         if (((j%3==0))|| (j==19)){
            snprintf(res,1024,"%d ",a);
            //lg = lg + 2;
            strcat(d,res);
         }
         if (((j%3==1)) || (j==20)){
            snprintf(res ,1024, "%d%d ",a,a);
            //lg = lg +3;
            strcat(d, res);
         }
         if (((j%3==2)) || (j==21)){
            snprintf(res ,1024, "%d%d%d ",a,a,a);
            //lg = lg +4;
            strcat(d, res);
         }
      }
      else {
         if ((j==15) || (j==22)){
            snprintf(res,1024,"%d ",a);
            //lg = lg + 2;
            strcat(d,res);
         }
         if ((j==16) || (j==23)){
            snprintf(res,1024,"%d%d ",a,a);
            //lg = lg + 3;
            strcat(d,res);
         }
         if ((j==17) || (j==24)){
            snprintf(res,1024,"%d%d%d ",a,a,a);
            //lg = lg + 4;
            strcat(d,res);
         }
         if ((j==18) || (j==25)){
            snprintf(res,1024,"%d%d%d%d ",a, a , a, a);
            //lg = lg + 5;
            strcat(d,res);
         }
      }


      i=i+1;
   }
   d[strlen(d)-1]='\0';
}
*/


/*
void antitap1(char *d, char * s) {
   int i=0,j=0 , n= strlen(s), bol=0;
   char c='1';
   strcpy(d,"");
   while(i<n){
      bol=0;
      j= i+1 ;
      while((j<n) && (s[j]!=' ')){
         j=j+1;
      }
      bol = j-i;
      c=s[i];
      switch (c){
           case '2' :
               if (bol==1)   strcat(d,"A");
               if (bol==2)   strcat(d , "B");
               if (bol==3)   strcat(d , "C");
               break;
           case '3' :
               if (bol==1)   strcat(d,"D");
               if (bol==2)   strcat(d , "E");
               if (bol==3)   strcat(d , "F");
               break;
           case '4' :
               if (bol==1)   strcat(d,"G");
               if (bol==2)   strcat(d , "H");
               if (bol==3)   strcat(d , "I");
               break;
           case '5' :
               if (bol==1)   strcat(d,"J");
               if (bol==2)   strcat(d , "K");
               if (bol==3)   strcat(d , "L");
               break;
           case '6' :
               if (bol==1)   strcat(d,"M");
               if (bol==2)   strcat(d , "N");
               if (bol==3)   strcat(d , "O");
               break;
           case '7' :
               if (bol==1)   strcat(d,"P");
               if (bol==2)   strcat(d , "Q");
               if (bol==3)   strcat(d , "R");
               if (bol==4)   strcat(d, "S");
               break;
           case '8' :
               if (bol==1)   strcat(d,"T");
               if (bol==2)   strcat(d , "U");
               if (bol==3)   strcat(d , "V");
               break;
           case '9' :
               if (bol==1)   strcat(d,"W");
               if (bol==2)   strcat(d , "X");
               if (bol==3)   strcat(d , "Y");
               if (bol==4)   strcat(d, "Z");
               break;
      }
      i=j+1;
   }
}*/


/*
void antitap2(char * s) {
   char  d[50] ="";
   int i=0, j=0, bol =0 , n=strlen(s);
   while(i<n){
      bol = 1;
      j=i+1;
      if (s[j]=='*'){  bol = s[i] -48;
                       i=j+1;}
      switch (s[i]){
           case '2' :
               if (bol==1)   strcat(d,"A");
               if (bol==2)   strcat(d , "B");
               if (bol==3)   strcat(d , "C");
               break;
           case '3' :
               if (bol==1)   strcat(d,"D");
               if (bol==2)   strcat(d , "E");
               if (bol==3)   strcat(d , "F");
               break;
           case '4' :
               if (bol==1)   strcat(d,"G");
               if (bol==2)   strcat(d , "H");
               if (bol==3)   strcat(d , "I");
               break;
           case '5' :
               if (bol==1)   strcat(d,"J");
               if (bol==2)   strcat(d , "K");
               if (bol==3)   strcat(d , "L");
               break;
           case '6' :
               if (bol==1)   strcat(d,"M");
               if (bol==2)   strcat(d , "N");
               if (bol==3)   strcat(d , "O");
               break;
           case '7' :
               if (bol==1)   strcat(d,"P");
               if (bol==2)   strcat(d , "Q");
               if (bol==3)   strcat(d , "R");
               if (bol==4)   strcat(d, "S");
               break;
           case '8' :
               if (bol==1)   strcat(d,"T");
               if (bol==2)   strcat(d , "U");
               if (bol==3)   strcat(d , "V");
               break;
           case '9' :
               if (bol==1)   strcat(d,"W");
               if (bol==2)   strcat(d , "X");
               if (bol==3)   strcat(d , "Y");
               if (bol==4)   strcat(d, "Z");
               break;
      }
      i=i+1;


   }
   strcpy(s,d);
}
*/