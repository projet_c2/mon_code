#include "code.h"
#include <stdlib.h>

char * init(char * tab, int taille) {
   int i=0;
   if (tab==NULL)    tab = (char *)malloc(taille*sizeof(int));
   if(tab != NULL){
   while(i<taille){
      if ((i==0) || (i==1))   tab[i]=0;
      else                    tab[i]=1;
      i=i+1;
   
   }}
  return tab;
}  

void affichage(FILE * file, char * tab, int taille ) {
   int i=3 ;
   fprintf(file , "%d",2);
   while(i<taille){
      if (tab[i]==1){
         fprintf(file," %d",i);
      }
      i=i+1;
   }
}

void suppMultiple(char * tab, int taille, int nb) {
   int i=nb+1;
   while(i<taille){
      if (i%nb==0)   tab[i]=0;
      i++;
   }

}

int prochainNombre(char * tab, int taille, int nb) {
   int i=nb+1;
   while((i<taille) && (tab[i]==0)){
      i++;
   }
   if (i>=taille)  i=-1;
   return i;
}

int listeNombrePremiers(FILE * file, int taille) {
   int i=2, nb=0;
   char * tab = (char *)malloc(taille*sizeof(int));
   if (tab!= NULL){
      init(tab , taille);
      while(((i<taille) && (prochainNombre(tab , taille , i)>0))){
         suppMultiple(tab , taille , i);
         i= prochainNombre(tab , taille , i);
         if (i<0)      i=taille;
      }
      i=2;
      while((i<taille)){
         if (tab[i]==1)   nb = nb +1;
         i++;
      }
      affichage(file , tab , taille);

   }
   free(tab);
   return nb;
}
