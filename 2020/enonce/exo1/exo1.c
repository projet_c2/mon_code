/*
 * REMPLACER LES NOMS DES FONCTIONS PAR LES VOTRES
 * DANS LES FICHIERS VOTRECODE.H et VOTRECODE.C
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "code.h"
#include "../teZZt.h"


// DEBUT DES FONCTIONS DE TESTS
BEGIN_TEST_GROUP(exo1)


TEST(INIT1, "initialisation tab statique") {
	char tab[5];
    
    init(tab, 5);
    CHECK(0 == tab[0]);
    CHECK(0 == tab[1]);
    CHECK(1 == tab[2]);
    CHECK(1 == tab[2]);
    CHECK(1 == tab[3]);
    CHECK(1 == tab[4]);
}

TEST(INIT2, "initialisation tab dynamique") {
	char * tab = NULL;
    
    tab = init(tab, 5);

    REQUIRE( NULL != tab);

    CHECK(0 == tab[0]);
    CHECK(0 == tab[1]);
    CHECK(1 == tab[2]);
    CHECK(1 == tab[3]);
    CHECK(1 == tab[4]);

    free(tab);
}

TEST(AFF1, "") {
	char buffer[1024];
    FILE * file = fmemopen(buffer, 1024, "w");

	char tab[10];
    init(tab, 10);
    //affichage(stdout, tab, 10);
    affichage(file, tab, 10);
    fclose(file);

    CHECK( !strcmp("2 3 4 5 6 7 8 9", buffer ) );

}	

TEST(SUPP2, "") {
	char buffer[1024];
    FILE * file = fmemopen(buffer, 1024, "w");

	char tab [20];
    init(tab, 20);
    suppMultiple(tab, 20, 2);
    // affichage(stdout, tab, 20);
    affichage(file, tab,   20);
    fclose(file);

    CHECK( !strcmp("2 3 5 7 9 11 13 15 17 19", buffer ) );

    
}	

TEST(SUPP3, "") {
	char buffer[1024];
    FILE * file = fmemopen(buffer, 1024, "w");

	char tab [20];
    init(tab, 20);
    suppMultiple(tab, 20, 3);
    // affichage(stdout, tab, 20);
    affichage(file, tab,   20);

    fclose(file);

    CHECK( !strcmp("2 3 4 5 7 8 10 11 13 14 16 17 19", buffer ) );

    
}	

TEST(SUPP2_3, "") {
	char buffer[1024];
    FILE * file = fmemopen(buffer, 1024, "w");

	char tab [20];
    init(tab, 20);
    suppMultiple(tab, 20, 2);
    suppMultiple(tab, 20, 3);
    //affichage(stdout, tab, 20);
    affichage(file, tab,   20);

    fclose(file);
    CHECK( !strcmp("2 3 5 7 11 13 17 19", buffer ) );
}


TEST(PROCHAIN, "") {
	
	char tab [20];
    init(tab, 20);
    suppMultiple(tab, 20, 2);
    suppMultiple(tab, 20, 3);
    
    CHECK(  3 == prochainNombre(tab, 20,  2));
    CHECK(  5 == prochainNombre(tab, 20,  3));
    CHECK(  5 == prochainNombre(tab, 20,  4));
    CHECK(  7 == prochainNombre(tab, 20,  5));
    CHECK( 11 == prochainNombre(tab, 20,  7));
    CHECK( 13 == prochainNombre(tab, 20, 11));
    CHECK( 17 == prochainNombre(tab, 20, 13));
    CHECK( 17 == prochainNombre(tab, 20, 15));
    CHECK( 19 == prochainNombre(tab, 20, 17));
    CHECK( -1 == prochainNombre(tab, 20, 19));	
    CHECK( -1 == prochainNombre(tab, 20, 30));	
    
}

TEST(FINAL1, "") {
	char buffer[1024];
    FILE * file = fmemopen(buffer, 1024, "w");
	int nombre;
	
	nombre =  listeNombrePremiers(file , 10);
    fclose(file);

	CHECK( 4 == nombre );
	CHECK( !strcmp("2 3 5 7", buffer ) );

   
}

TEST(FINAL2, "") {
	char buffer[1024];
    FILE * file = fmemopen(buffer, 1024, "w");
	int nombre;
	
    nombre =  listeNombrePremiers(file , 20);
    fclose(file);

    CHECK( 8 == nombre );
    CHECK( !strcmp("2 3 5 7 11 13 17 19", buffer ) );

    
}

TEST(FINAL3, "") {
	char buffer[1024];
    FILE * file = fmemopen(buffer, 1024, "w");
	int nombre;
	
    nombre =  listeNombrePremiers(file , 100);
    fclose(file);
    // je vais tester le tableau 
    CHECK( 25 == nombre );
    CHECK( !strcmp("2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97", buffer ) );
}

TEST(FINAL4, "") {
	char buffer[1024];
	//  c'est encore assez grand
    FILE * file = fmemopen(buffer, 1024, "w");
	int nombre;
	
    nombre =  listeNombrePremiers(file , 1000);
    fclose(file);
    
    // je vais tester la valeur et le tableau :-)
    //printf("%d\nbuffer est %s\n", nombre, buffer);
    CHECK(nombre==168);
    CHECK(!strcmp("2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 101 103 107 109 113 127 131 137 139 149 151 157 163 167 173 179 181 191 193 197 199 211 223 227 229 233 239 241 251 257 263 269 271 277 281 283 293 307 311 313 317 331 337 347 349 353 359 367 373 379 383 389 397 401 409 419 421 431 433 439 443 449 457 461 463 467 479 487 491 499 503 509 521 523 541 547 557 563 569 571 577 587 593 599 601 607 613 617 619 631 641 643 647 653 659 661 673 677 683 691 701 709 719 727 733 739 743 751 757 761 769 773 787 797 809 811 821 823 827 829 839 853 857 859 863 877 881 883 887 907 911 919 929 937 941 947 953 967 971 977 983 991 997", buffer));
    
    
}



END_TEST_GROUP(exo1)


int main() {
    RUN_TEST_GROUP(exo1); 
 
   return 0;
}

