#ifndef hall_of_fame_h
#define hall_of_fame_h
#define TAILLE_MAX 50


struct donnee{
      int score;
      char nom[100];
      char alias[40];
      struct donnee * suivant;
   };
typedef struct donnee donnee_t ;

// et de leur typedef si besoin


/* DECLARATIONS DES METHODES */
void afficherDonnee(FILE *, donnee_t);
void saisirDonnee (FILE * , donnee_t *);
int tableauFromFilename(char s[] ,donnee_t tableau[]);
// mettre ici les autres declarations

#endif
