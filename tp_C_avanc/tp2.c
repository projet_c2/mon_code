#include <stdio.h>


void reml_int_aff(int sudoku[9][9])
{
     int i,j;
     for(i=0;i<9;i++)
     {
         for(j=0;j<9;j++)
         {
             if ((i%2==0) && (j%3==0))    sudoku[i][j] = (i+1+j)%9 ;
             else                         sudoku[i][j] = 0;
             printf("%d ", sudoku[i][j]);
         }
         printf("\n");
     }
}


int test_ligne_colone(int sudoku[9][9], int k, int l, int v)
{
    int a=0;
    int i =0;
    for(i=0;i<9;i++)
    {
       if ( (sudoku[k][i] == v)  || (sudoku[i][l] == v))    a= 1;
    }
    return a;
}


int test_region(int sudoku[9][9], int k, int l, int v)
{
    int rk = k%3 , rl = l%3 ;
    int i=0, j = 0;
    int a = 0;
    for (i=k-rk ; (i<k-rk+3) && (i!=k); i++)
    {
              for (j=l-rl ; j<l-rl+3; j++)
              {
                  if (sudoku[i][j] == v)   a=1 ;
               }
         
    }
    
    return a;
}




void process_sudk(int sudoku[9][9])
{
    printf("entrer des indices entre 0 et 8 dans la case ou  :\n"); 
    int k , l ;
    scanf("%d %d", &k, &l);
    if (k>8 || l>8 )           printf("%d ou  %d est invalide !!!!", k , l); 
    if (sudoku[k][l] != 0)      printf("la case de (%d,%d) est deja occupée par %d !!!\n" , k, l, sudoku[k][l]);
    while (sudoku[k][l] == 0)
    {
        printf("entrer une valeur entre 1 et 9 : \n");
        int v;
        scanf("%d", &v);
        if (v>0 && v<10)
        {
           if ( (test_ligne_colone(sudoku , k, l , v)==0 ) && (test_region(sudoku , k, l , v) == 0))
           {
               sudoku[k][l] = v ;
           }
           else                   printf("%d est une valeur invalide --deja placée--\n", v);
        }
        else                      printf("%d doit étre compris entre 1 et 9 !!!\n", v); 
    }
    
   
}











int main()
{
    int sudoku[9][9];
    reml_int_aff(sudoku);
    
    process_sudk(sudoku);

}







