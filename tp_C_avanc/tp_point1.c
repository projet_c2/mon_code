#include <stdio.h>



int main()
{
   int i  , *ptri  = &i;
   char c1 = '1', *ptrc1 = &c1;
   double d , *ptrd = &d;

   printf("ptri = %u ptrc1 = %u \n",ptri, ptrc1);
   printf("ptrd = %u  \n",ptrd);
   printf("ptri = %x ptrc1 = %x \n",ptri, ptrc1);
   // seule la version suivante ne genere pas d'avertissement
   printf("ptri = %p ptrc1 = %p \n",ptri, ptrc1);
   printf("*ptri = %d et *ptrc1=%c \n ", *ptri, *ptrc1);
   printf("ptrd = %p  \n",ptrd);
   
   ptri++;
   ptrc1++;
   
   printf("ptri = %u ptrc1 = %d \n",ptri, ptrc1);
   // cela permet de voir la taille d'un int et d'un char en memoire
   // sizeof(int)  sizeof(char)


   return 0 ;
}

