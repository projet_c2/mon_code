
#base64
import base64

text = "Hello, World!"
encoded = base64.b64encode(text.encode('utf-8')).decode('utf-8')
print(encoded)

####

def rot13(text):
    result = []
    for char in text:
        if 'a' <= char <= 'z':
            offset = ord('a')
            result.append(chr(((ord(char) - offset + 13) % 26) + offset))
        elif 'A' <= char <= 'Z':
            offset = ord('A')
            result.append(chr(((ord(char) - offset + 13) % 26) + offset))
        else:
            result.append(char)
    return ''.join(result)

text = "Hello"
encoded = rot13(text)
print(encoded)


##################################


from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes

# Génération d'une clé AES
key = get_random_bytes(16)

# Texte à chiffrer
text = "Ceci est un exemple de texte à chiffrer."

# Création de l'objet de chiffrement AES
cipher = AES.new(key, AES.MODE_EAX)

# Chiffrement du texte
ciphertext, tag = cipher.encrypt_and_digest(text.encode('utf-8'))

# Déchiffrement du texte
decipher = AES.new(key, AES.MODE_EAX, cipher.nonce)
decrypted_text = decipher.decrypt(ciphertext).decode('utf-8')

print("Texte chiffré:", ciphertext)
print("Texte déchiffré:", decrypted_text)



####################################

import math

def calculate_entropy(data):
    entropy = 0
    for byte in data:
        probability = data.count(byte) / len(data)
        entropy -= probability * math.log2(probability)
    return entropy

data = b"Hello, World!"
entropy = calculate_entropy(data)
print("Entropie:", entropy)


#################################

# Simuler une attaque par rejeu
def simulate_replay_attack(data):
    intercepted_data = data  # Simuler l'interception du message chiffré
    # L'attaquant renvoie le message intercepté
    return intercepted_data

original_data = "Ouvrir la porte 123"
encrypted_data = encrypt(original_data)
attacker_data = simulate_replay_attack(encrypted_data)
print("Données originales:", original_data)
print("Données interceptées par l'attaquant:", attacker_data)



#####################################


import matplotlib.pyplot as plt

# Exemple de série chronologique
timestamps = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
values = [10, 12, 15, 20, 18, 25, 28, 30, 35, 40]

plt.plot(timestamps, values)
plt.xlabel('Temps')
plt.ylabel('Valeurs')
plt.title('Exemple de série chronologique')
plt.show()



#######################################ZGNoaWZmcmVtZW50##########################""

import base64

encoded = "SGVsbG8sIFdvcmxkIQ=="
decoded = base64.b64decode(encoded).decode('utf-8')
print(decoded)

#####################


def rot13(text):
    result = []
    for char in text:
        if 'a' <= char <= 'z':
            offset = ord('a')
            result.append(chr(((ord(char) - offset - 13) % 26) + offset))
        elif 'A' <= char <= 'Z':
            offset = ord('A')
            result.append(chr(((ord(char) - offset - 13) % 26) + offset))
        else:
            result.append(char)
    return ''.join(result)

encoded = "Uryyb"
decoded = rot13(encoded)
print(decoded)



##########################

from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes

# Clé AES préalablement générée (doit être la même que celle utilisée pour le chiffrement)
key = get_random_bytes(16)

# Texte chiffré
ciphertext = b'\xc2\x9dC\xf6K\x9eRrX\xaf\xb0=\x1eV\x0b\x07\xb3\xc8\x05'

# Création de l'objet de déchiffrement AES
decipher = AES.new(key, AES.MODE_EAX, nonce=nonce)

# Déchiffrement du texte
decrypted_text = decipher.decrypt(ciphertext).decode('utf-8')

print("Texte déchiffré:", decrypted_text)



############################"




"