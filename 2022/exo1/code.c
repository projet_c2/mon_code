#include "code.h"
#include <math.h>
#include <string.h>
#include <stdlib.h>


int convert_to_d(char * s) {

   int n=0;
   while(*(s+n)!= '\0'){
      n=n+1;
   }

   int i=0,j=0, a=0;
   while(s[i]!='\0'){
      j=s[i];
      if(j>57){
         a = a + (j-55)*pow(16,n-i-1);
      }
      else{
         a = a + (j-48)*pow(16,n-i-1);
      }
      i=i+1;
   }

   return a;
}

// ********************************************************

int is_prime(int n)
{
   int a=0;
   if (n>1){
   a=1;
   int i=2 , m=(int)sqrt(n);
   while((i<m)&&(a==1)){
      if (n%i==0)   a=0;
      i=i+1;
   }}
   return a;
}

void facteurs_simple(char * res, int n) {
   int a=1 ,j=0;
   char  res_0[1024] ;
   strcpy(res, "");
   while((n>1) && (a<n+1)){
      if ((is_prime(a)==1) && (n%(a)==0)){
         snprintf(res_0,1024,"%d ",a);
         j= j+ strlen(res_0);
         strcat(res,res_0);
         n = n/a ;
      }
      else a=a+1;
   }
   res[j-1]='\0';
}


void facteurs(char * res, int n) {
   int a=2 , p_oc=0;
   char res_0[1024];
   strcpy(res, "");
   while((n>1) && (a<n+1)){
      if ((is_prime(a)==1) && (n%a==0)){
        p_oc = 0;
         while((n>1) && (n%a==0)){
            p_oc = p_oc +1;
            n = n/a;
         }
         if (p_oc==1){
            snprintf(res_0,1024,"%d*",a);
            strcat(res,res_0);
         }
         if (p_oc>1) {
            snprintf(res_0,1024,"%d^%d*",a,p_oc);
            strcat(res,res_0);
         }
      }
      else a=a+1;
   }
   res[strlen(res)-1]='\0';
}


// ********************************************************

double ** creer_id(int n)  {
   double ** m = (double **)malloc(n*sizeof(double));
   int i=0, j=0;
   if(m==NULL)   {exit(0);}
   for(i=0;i<n;i++){
      
      *(m+i) = (double *)malloc(n*sizeof(double));
      if(*(m +i)==NULL)   {exit(0);}
   }
   for(i=0;i<n;i++){
      for(j=0;j<n;j++){
         if (i==j)    *(*(m+i)+j) = 1;
         else         *(*(m+i)+j) =0;
      }
   }
   
   return m;
}

void liberer(double **m, int n) {
   int i=0;
   for(i=0;i<n;i++){
      free(*(m+i));
   }
   free(m);
}

double ** lire_mat(char * nom, int * ordre) {
   double ** m= NULL;
   FILE * fichier = fopen(nom,"r");
   if (fichier != NULL){
   fscanf(fichier, "%d\n",ordre);
   int  i=0 , j=0;
   m = creer_id(*ordre);
   for(i=0;i<(*ordre);i++){
      for(j=0;j<(*ordre);j++){
         fscanf(fichier,"%le",*(m+i)+j);
      }
   }
   fclose(fichier);
   }
   return m;
}

double ** multiplication(double ** a, double **b, int ordre) {
   int i=0, j=0 , k=0;
   double s=0;
   double ** m= creer_id(ordre);
   for(i=0;i<ordre;i++){
      for(j=0;j<ordre;j++){
         s=0;
         for(k=0;k<ordre;k++){
            s= s+ (*(*(a+i)+k))*(*(*(b+k)+j));
         }
         *(*(m+i)+j) = s;

      }
   }

   return m;
}
