#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>


#define POPULATION 5 // Taille de la population
#define ITERATIONS 10 // Nombre d'itérations

typedef struct {
    int  * rules; // Ordre de visite des rules
    int score; // Évaluation de la qualité de l'Cerveau
} Cerveau;


void initialise_rule(Rule *rule)
{
    rule->priority = rand()%6;
    rule->action = rand()%2+1;
    if(rand()%100 <50)
    {
        rule->playerDirection = rand()%7+1;
        rule->diamondDistance = rand()%3;
        rule->diamondDirection = -1;
        rule->playerDistance = -1;
    }else{
        rule->playerDirection = -1;
        rule->diamondDistance = -1;
        rule->diamondDirection =rand()%7+1;
        rule->playerDistance = rand()%3;
    }
}

initialise_rulebas(Rule  *rule)
{
    rule->playerDirection =-1;
    rule->diamondDistance = -1;
    rule->diamondDirection = -1;
    rule->playerDistance = -1;
    rule->priority = -1;
    rule->action = -1;
}
void copie_rules(Rule* rule1,Rule* rule2)
{
   rule1->playerDirection  = rule2->playerDirection; 
    rule1->diamondDistance =  rule2->diamondDistance; 
    rule1->diamondDirection  = rule2->diamondDirection ;
    rule1->playerDistance  = rule2->playerDistance ;
    rule1->priority =rule2->priority ;
    rule1->action = rule2->action;
}


Rule **initalisation_cereveau()
{
    Rule ** rules_cerveau= malloc(OPULATION*sizeof(Rule *)); // Coordonnées des rules
    if(rules_cerveau)
    {
        for (int i =0;i<POPULATION;i++)
        {
            rules_cerveau[i]=malloc(MAX_Rule*sizeof(Rule));
            if(rules_cerveau[i])
            {
               initialise_rule(rules_cerveau[i]);
            }
       }
    }

    return rules_cerveau;

   
}
Rule  ** rules_cerveau =initalisation_cereveau();
Rule  ** rules_cerveau_fills =initalisation_cereveau();
Rule  ** rules_cerveau_parents =initalisation_cereveau();

void creerPopulationInitiale(Cerveau population[], int taillePopulation)
{
    for (int i = 0; i < taillePopulation; i++) {
        // Génération aléatoire d'un ordre de utilisation  des rules
        for (int j = 0; j < MAX_Rule; j++) {
            population[i].rules[j] = j;
        }
        for (int j = MAX_Rule - 1; j > 0; j--) {
            int index = rand() % (j + 1);
            int temp = population[i].rules[j];
            population[i].rules[j] = population[i].rules[index];
            population[i].rules[index] = temp;
        }
    }
}

int evaluerrules(int index_rules[], int i,Monster *monsters, Traveler *traveler) {
    int distanceTotale = 0;
    for (int i = 0; i < MAX_Rule ; i++) {
        int ruleActuelle = index_rules[i];
        distanceTotale += (calcule_score(rules_cerveau[i][ruleActuelle], monsters,traveler)/ MAX_Rule);
    }
    return distanceTotale;
}

void evaluerPopulation(Cerveau population[], int taillePopulation,Monster *monsters, Traveler *traveler) {
    for (int i = 0; i < taillePopulation; i++) {
        int distancerules = evaluerrules(population[i].rules, i,monsters,traveler);
        population[i].score = distancerules;
    }
}

void selectionnerParents(Cerveau population[], Cerveau parents[], int taillePopulation) {
    for (int i = 0; i < taillePopulation; i++) {
        parents[i] = population[i];
        for ( int j=0;j<MAX_Rule;j++){
            copie_rules(&rules_cerveau_parents[i][j],&rules_cerveau[i][j]);
        }
        
    }
}

void croiserParents(Cerveau parents[], Cerveau enfants[], int tailleParents) {
    for (int i = 0; i < tailleParents; i++) {
        int indexParent1 = rand() % tailleParents;
        int indexParent2 = rand() % tailleParents;

        int pointDeCoupure1 = rand() % MAX_Rule;
        int pointDeCoupure2 = rand() % MAX_Rule;

        if (pointDeCoupure1 > pointDeCoupure2) {
            int temp = pointDeCoupure1;
            pointDeCoupure1 = pointDeCoupure2;
            pointDeCoupure2 = temp;
        }

        int* parent1 = parents[indexParent1].rules;
        int* parent2 = parents[indexParent2].rules;

        int* enfant = enfants[i].rules;

        for (int j = 0; j < MAX_Rule; j++) {
            if (j >= pointDeCoupure1 && j <= pointDeCoupure2) {
                enfant[j] = parent1[j];
                copie_rules(&rules_cerveau-rules_cerveau_fills[i][enfant[j]],&rules_cerveau_parents[indexParent1][parent1[j]]);
            } else {
                enfant[j] = -1;
                initialise_rulebas(&rules_cerveau_fills[i][enfant[j]]);
            }
        }

        int indexParent2Copy = 0;
        for (int j = 0; j < MAX_Rule; j++) {
            if (enfant[j] == -1) {
                while (enfant[j] == -1) {
                    int Rule = parent2[indexParent2Copy++];
                    int estDejaPresent = 0;
                    for (int k = 0; k < MAX_Rule; k++) {
                        if (enfant[k] == Rule) {
                            estDejaPresent = 1;
                            break;
                        }
                    }
                    if (!estDejaPresent) {
                        enfant[j] = Rule;
                        copie_rules(&rules_cerveau_fills[j][Rule],&rules_cerveau[indexParent2][Rule]);

                    }
                }
            }
        }
    }
}

void muterEnfants(Cerveau enfants[], int tailleEnfants) {
    for (int i = 0; i < tailleEnfants; i++) {
        if (rand() % 100 < 5) { // Probabilité de mutation de 5%
            int index1 = rand() % MAX_Rule;
            int index2 = rand() % MAX_Rule;
            int temp = enfants[i].rules[index1];
            enfants[i].rules[index1] = enfants[i].rules[index2];
            enfants[i].rules[index2] = temp;
        }
    }
}

void remplacerPopulation(Cerveau population[], Cerveau enfants[], int taillePopulation) {
    for (int i = 0; i < taillePopulation; i++) {
        population[i] = enfants[i];
        for ( int j=0;j<MAX_Rule;j++){
            copie_rules(&rules_cerveau[i][j],&rules_cerveau_fills[i][j]);
        }

    }
}

void algorithmeGenetique(int taillePopulation, int nombreIterations,Monster *monsters, Traveler *traveler) {
    Cerveau population[taillePopulation];
    Cerveau parents[taillePopulation];
    Cerveau enfants[taillePopulation];

    creerPopulationInitiale(population, taillePopulation);

    for (int iteration = 0; iteration < nombreIterations; iteration++) {
        evaluerPopulation(population, taillePopulation,monsters,traveler);

        int meilleurscore = population[0].score;
         int indexMeilleur = 0;
        for (int i = 1; i < taillePopulation; i++) {
            if (population[i].score > meilleurscore) {
               meilleurscore = population[i].score;
               indexMeilleur = i;
            }
        }
        printf("Meilleur Cerveau (rules) : %d \n",indexMeilleur);
        printf("Meilleur Cerveau (score) : %d\n", population[indexMeilleur].score);
        selectionnerParents(population, parents, taillePopulation);
        croiserParents(parents, enfants, taillePopulation);
        muterEnfants(enfants, taillePopulation);
        remplacerPopulation(population, enfants, taillePopulation);
    }
}

int main() {
    srand(time(NULL));

    algorithmeGenetique(POPULATION, ITERATIONS);

    return 0;
}
