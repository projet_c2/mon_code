#include <SDL2/SDL.h>
#include <stdlib.h>
#include <time.h>
#include "main_jeu.h"
#include "animation.h"



typedef struct {
    int x;
    int y;
    int isAlive;
} Monster;



Uint32 bombTimer = 0;

int gameOver = 0;

int score =  0;

SDL_Texture* my_texture1;
SDL_Texture* my_texture2;
SDL_Texture* my_texture3;
SDL_Texture* my_texture4;
SDL_Texture* my_texture5;





int main() {


    Monster * monsters = malloc(MAX_MONSTERS*sizeof(Monster));
    if(monsters==NULL){
        return 1;
    }
    int aa=1000;
    int a_rand =3;

    SDL_DisplayMode screen;

    // Initialisation de SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Erreur lors de l'initialisation de SDL : %s\n", SDL_GetError());
        return -1;
    }

    SDL_GetCurrentDisplayMode(0, &screen);
       printf("Résolution écran\n\tw : %d\n\th : %d\n",
          screen.w, screen.h);

       /* Création de la fenêtre */
    SDL_Window*  window = SDL_CreateWindow("JG17",
                 SDL_WINDOWPOS_CENTERED,
                 SDL_WINDOWPOS_CENTERED, screen.w * 0.66,
                 screen.h * 0.66,
                 SDL_WINDOW_OPENGL);
    if (window == NULL) {
        printf("Erreur lors de l'initialisation de window : %s\n", SDL_GetError());
        return -1;
    }

    int SCREEN_WIDTH = screen.w * 0.66;
    int  SCREEN_HEIGHT = screen.h * 0.66;

     // Initialisation de SDL_ttf
    if (TTF_Init() < 0) {
        fprintf(stderr, "Erreur lors de l'initialisation de SDL_ttf: %s\n", TTF_GetError());
        return 1;
    }
     TTF_Font* font1 = TTF_OpenFont("/usr/share/fonts/truetype/tlwg/Laksaman-BoldItalic.ttf", 64);
    if (font1 == NULL) {
        fprintf(stderr, "Erreur lors du chargement de la police de caractères: %s\n", TTF_GetError());
        return 1;
    } 

    TTF_Font* font = TTF_OpenFont("/usr/share/fonts/truetype/tlwg/Laksaman-BoldItalic.ttf", 24);
    if (font == NULL) {
        fprintf(stderr, "Erreur lors du chargement de la police de caractères: %s\n", TTF_GetError());
        return 1;
    } 


    // Création du renderer
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) {
        printf("Erreur lors de la création du renderer : %s\n", SDL_GetError());
        SDL_DestroyWindow(window);
        SDL_Quit();
        return -1;
    }

    //include les images :
    my_texture1 = load_texture_from_image("bg_jeu.png", window, renderer);
    my_texture2 = load_texture_from_image("per_1.png", window, renderer);
    my_texture3 = load_texture_from_image("dm_hg.png", window, renderer);
    my_texture4 = load_texture_from_image("bom.png", window, renderer);
    my_texture5 = load_texture_from_image("action_bom.png", window, renderer);

    // Génération aléatoire du labyrinthe
    srand(time(NULL));
    int numRectanglesX = SCREEN_WIDTH / RECTANGLE_SIZE;
    int numRectanglesY = SCREEN_HEIGHT / RECTANGLE_SIZE;

    SDL_Rect** rectangles = (SDL_Rect**)malloc(numRectanglesX * sizeof(SDL_Rect*));
    if (rectangles == NULL) {
        printf("Erreur lors de l'allocation de mémoire pour les rectangles.\n");
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        return -1;
    }

    for (int i = 0; i < numRectanglesX; i++) {
        rectangles[i] = (SDL_Rect*)malloc(numRectanglesY *sizeof(SDL_Rect));
        if (rectangles[i] == NULL) {
            printf("Erreur lors de l'allocation de mémoire pour les rectangles.\n");
            for (int j = 0; j < i; j++) {
                free(rectangles[j]);
            }
            free(rectangles);
            SDL_DestroyRenderer(renderer);
            SDL_DestroyWindow(window);
            SDL_Quit();
            return -1;
        }
        for (int j = 0; j < numRectanglesY; j++) {
            SDL_Rect rectangle = {i * RECTANGLE_SIZE, j * RECTANGLE_SIZE, RECTANGLE_SIZE, RECTANGLE_SIZE};
            rectangles[i][j] = rectangle;
        }
    }

    //Gestion des bombes;
    Bomb * bombs = (Bomb *)malloc(NUM_BOMBS * sizeof(Bomb));
    for(int i=0 ; i<NUM_BOMBS;i++){
        bombs[i].x = -1;
        bombs[i].y = -1;
        bombs[i].timer = 30000;
    }
    int a=0 , b=0;

    int diamondX =-1;  
    int diamondY = -1;

    // Génération aléatoire des rectangles du labyrinthe
    for (int i = 0; i < numRectanglesX; i++) {
        for (int j = 0; j < numRectanglesY; j++) {
            if (rand() % 10 == 0) {
                rectangles[i][j].w = 0;
                rectangles[i][j].h = 0;
                if(b<NUM_BOMBS){
                   bombs[b].x = rand() % numRectanglesX;
                   bombs[b].y = rand() % numRectanglesY;
                   bombs[b].timer = 30000;
                   b++;
                }else{
                    if(0==a){
                        printf("hello ..\n");
                        // Gestion des diamants
                        diamondX =rand() % numRectanglesX;
                        diamondY =  rand() % numRectanglesY;
                        a=1;
                    }
                }
            }
        }
    }

    if(b<NUM_BOMBS-1){
        for(int j=b ; j<NUM_BOMBS; j++){
            bombs[j].x = rand() % numRectanglesX;
            bombs[j].y = rand() % numRectanglesY;
            bombs[j].timer = 30000;
        }
    }

    // Position initiale du traveler
    int travelerX = numRectanglesX / 2 * RECTANGLE_SIZE + (RECTANGLE_SIZE - TRAVELER_SIZE) / 2;
    int travelerY = numRectanglesY / 2 * RECTANGLE_SIZE + (RECTANGLE_SIZE - TRAVELER_SIZE) / 2;

    // Initialisation de la position des monstresrand() % numRectanglesY
    for (int i = 0; i < MAX_MONSTERS; i++) {
        monsters[i].x = numRectanglesX / 2 ;
        monsters[i].y = numRectanglesY / 2 ;
        monsters[i].isAlive = 1;
    }


    // Boucle principale du jeu
    int gameRunning = 1;
    SDL_Event event;
    int stepCount = 0;
    while (gameRunning) {
        // Gestion des événements
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                gameRunning = 0;
            }
        }

        // Mise à jour de la position des monstres
        if (aa==1000){for (int i = 0; i < MAX_MONSTERS; i++) {
            if (monsters[i].isAlive) {
                int monsterX = monsters[i].x;
                int monsterY = monsters[i].y;

                // Déplacement aléatoire des monstres
                int randomDirection = rand() % 4;
                //a_rand=3;
                switch (randomDirection) {
                    case 0:  // Haut
                        if ((monsterY> 0)&& (rectangles[monsterX][monsterY-1].h > 0) ){
                            monsterY--;
                        }
                        break;
                    case 1:  // Bas
                        if ((monsterY < numRectanglesY) && (rectangles[monsterX][monsterY+1].h > 0)) {
                            monsterY++;
                        }
                        break;
                    case 2:  // Gauche
                        if ((monsterX > 0) && (rectangles[monsterX-1][monsterY].w > 0)) {
                            monsterX--;
                        }
                        break;
                    case 3:  // Droite
                        if ((monsterX < numRectanglesX)  && (rectangles[monsterX+1][monsterY].w > 0)) {
                            monsterX++;
                        }
                        break;
                }

                // Vérification si le monstre entre en collision avec le joueur
                /*if (monsterX == playerX && monsterY == playerY) {
                    gameOver = 1;
                    break;
                }*/

                monsters[i].x = monsterX;
                monsters[i].y = monsterY;
            }
        }}

        // Déplacement du traveler
        const Uint8 *keyboardState = SDL_GetKeyboardState(NULL);
        if (keyboardState[SDL_SCANCODE_LEFT] && stepCount >= STEP_THRESHOLD && travelerX - RECTANGLE_SIZE >= 0) {
            int targetX = travelerX - RECTANGLE_SIZE;
            int targetIndexX = targetX / RECTANGLE_SIZE;
            int targetIndexY = travelerY / RECTANGLE_SIZE;
            if (targetIndexX >= 0 && rectangles[targetIndexX][targetIndexY].w > 0) {
                travelerX = targetX;
                SDL_Rect travelerRect = { travelerX, travelerY, TRAVELER_SIZE, TRAVELER_SIZE };
                play_with_t_4g(my_texture2, window,renderer,travelerRect,8,4,1,1,0);
                stepCount = 0;
            }
        }
        if (keyboardState[SDL_SCANCODE_RIGHT] && stepCount >= STEP_THRESHOLD && travelerX + RECTANGLE_SIZE + TRAVELER_SIZE <= SCREEN_WIDTH) {
            int targetX = travelerX + RECTANGLE_SIZE;
            int targetIndexX = targetX / RECTANGLE_SIZE;
            int targetIndexY = travelerY / RECTANGLE_SIZE;
            if (targetIndexX < numRectanglesX && rectangles[targetIndexX][targetIndexY].w > 0) {
                travelerX = targetX;
                SDL_Rect travelerRect = { travelerX, travelerY, TRAVELER_SIZE, TRAVELER_SIZE };
                play_with_t_4g(my_texture2, window,renderer,travelerRect,8,4,1,2,0);
                stepCount = 0;
            }
        }
        if (keyboardState[SDL_SCANCODE_UP] && stepCount >= STEP_THRESHOLD && travelerY - RECTANGLE_SIZE >= 0) {
            int targetY = travelerY - RECTANGLE_SIZE;
            int targetIndexX = travelerX / RECTANGLE_SIZE;
            int targetIndexY = targetY / RECTANGLE_SIZE;
            if (targetIndexY >= 0 && rectangles[targetIndexX][targetIndexY].h > 0) {
                travelerY = targetY;
                SDL_Rect travelerRect = { travelerX, travelerY, TRAVELER_SIZE, TRAVELER_SIZE };
                play_with_t_4g(my_texture2, window,renderer,travelerRect,8,4,1,3,0);
                stepCount = 0;
            }
        }
        if (keyboardState[SDL_SCANCODE_DOWN] && stepCount >= STEP_THRESHOLD && travelerY + RECTANGLE_SIZE + TRAVELER_SIZE <= SCREEN_HEIGHT) {
            int targetY = travelerY + RECTANGLE_SIZE;
            int targetIndexX = travelerX / RECTANGLE_SIZE;
            int targetIndexY = targetY / RECTANGLE_SIZE;
            if (targetIndexY < numRectanglesY && rectangles[targetIndexX][targetIndexY].h > 0) {
                travelerY = targetY;
                SDL_Rect travelerRect = { travelerX, travelerY, TRAVELER_SIZE, TRAVELER_SIZE };
                play_with_t_4g(my_texture2, window,renderer,travelerRect,8,4,1,0,0);
                stepCount = 0;
            }
        }

        // Gestion des bombes
       /*for (int i = 0; i < NUM_BOMBS; i++) {
         bombs[i].timer -= 16;  // Soustraire le temps écoulé depuis la dernière itération (16 ms)
           if (bombs[i].timer <= 0) {
                bombs[i].x = rand() % numRectanglesX;
                bombs[i].y = rand() % numRectanglesY;
                bombs[i].timer = 30000;  // Réinitialiser le timer à 3 secondes
           }
            while(rectangles[ bombs[i].x][bombs[i].y].h == 0){
               
                bombs[i].x = rand() % numRectanglesX;
                bombs[i].y  = rand() % numRectanglesY;
            }
        }*/

        // Manger le diamant
        int travelerIndexX = travelerX / RECTANGLE_SIZE;
        int travelerIndexY = travelerY / RECTANGLE_SIZE;
        if (travelerIndexX == diamondX && travelerIndexY == diamondY) {
            score ++;
            diamondX = rand() % numRectanglesX;
            diamondY = rand() % numRectanglesY;
            do {
            // Étape a : Génération de coordonnées aléatoires
            diamondX = rand() % numRectanglesX;
            diamondY = rand() % numRectanglesY;

            // Étape b : Vérification si la position est un mur ou non
            if (rectangles[ diamondX][diamondY].h > 0) {
                // La position est un mur, générez une nouvelle position
                break;
            }

            // Si la position est invalide, sortir de la boucle
    
            } while (1);
        }
      
        // Manger un bomb
       /* for (int i = 0; i < NUM_BOMBS; i++) {
          if (travelerIndexX == bombs[i].x && travelerIndexY == bombs[i].y) {
             gameOver = 1;
             SDL_Rect travelerRect = { travelerX, travelerY, TRAVELER_SIZE, TRAVELER_SIZE };
            play_with_texture_4(my_texture5,window,renderer,travelerRect,6,6,31);
             break;  // Sortir de la boucle dès qu'une bombe est mangé
           }
        }*/

        if (gameOver) {
             
            
            // Réinitialiser les positions du traveler et des bombes
            travelerX = numRectanglesX / 2 * RECTANGLE_SIZE + (RECTANGLE_SIZE - TRAVELER_SIZE) / 2;
            travelerY = numRectanglesY / 2 * RECTANGLE_SIZE + (RECTANGLE_SIZE - TRAVELER_SIZE) / 2;
           /*for (int i = 0; i < NUM_BOMBS; i++) {
                bombs[i].x = rand() % numRectanglesX;
                bombs[i].y = rand() % numRectanglesY;
                bombs[i].timer = 30000;  // Réinitialiser le timer à 3 secondes
            }*/

             do {
            // Étape a : Génération de coordonnées aléatoires
            diamondX = rand() % numRectanglesX;
            diamondY = rand() % numRectanglesY;

            // Étape b : Vérification si la position est un mur ou non
            if (rectangles[ diamondX][diamondY].h > 0) {
                // La position est un mur, générez une nouvelle position
                break;
            }

            // Si la position est invalide, sortir de la boucle
    
            } while (1);

            // Réinitialiser l'état du jeu
            gameOver = 0;


             // Afficher le message Game Over avec le score
              char message[100];
           snprintf(message, sizeof(message), "Game Over. Votre score est %d\n", score);
           SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, "Game Over", message,NULL);
           score =0;
        }



        // Incrémenter le compteur de pas
        stepCount++;

        // Effacer l'écran
        SDL_SetRenderDrawColor(renderer, 64, 49, 45, 255);
        SDL_RenderClear(renderer);

        // Dessiner le labyrinthe
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
        for (int i = 0; i < numRectanglesX; i++) {
            for (int j = 0; j < numRectanglesY; j++) {
                
                play_with_texture_1(my_texture1,renderer, rectangles[i][j],0);
            }
        }

         // Dessiner le diamant
        if (diamondX != -1 && diamondY != -1) {
            SDL_Rect diamondRect = {diamondX * RECTANGLE_SIZE + (RECTANGLE_SIZE - DIAMOND_SIZE) / 2, diamondY * RECTANGLE_SIZE + (RECTANGLE_SIZE - DIAMOND_SIZE) / 2, DIAMOND_SIZE, DIAMOND_SIZE};
            
            play_with_texture_1(my_texture3, renderer, diamondRect,0);

        }

         // Dessiner le bomb
/*
        for (int i = 0; i < NUM_BOMBS; i++) {
            SDL_Rect bombRect = { bombs[i].x * RECTANGLE_SIZE + (RECTANGLE_SIZE - DIAMOND_SIZE) / 2, bombs[i].y * RECTANGLE_SIZE + (RECTANGLE_SIZE - DIAMOND_SIZE) / 2, DIAMOND_SIZE, DIAMOND_SIZE };
            play_with_texture_1(my_texture4, renderer, bombRect,0);

        }
*/

        // Dessiner le traveler
        SDL_Rect travelerRect = { travelerX, travelerY, TRAVELER_SIZE, TRAVELER_SIZE };
        play_with_texture_1(my_texture2, renderer,travelerRect,1);

        // Affichage des monstres
        for (int i = 0; i < MAX_MONSTERS; i++) {
            if (monsters[i].isAlive) {
                SDL_Rect monsterRect = {monsters[i].x *RECTANGLE_SIZE, monsters[i].y*RECTANGLE_SIZE , RECTANGLE_SIZE, RECTANGLE_SIZE};
                SDL_RenderCopy(renderer, my_texture4, NULL, &monsterRect);
            }
        }

        // Affichage du score
        char scoreText[10];
        snprintf(scoreText, sizeof(scoreText), "Score: %d", score);
        SDL_Color textColor ;
        textColor.a=0;
        textColor.b=255;
        textColor.g=255;
        SDL_Surface* textSurface = TTF_RenderText_Solid(font, scoreText, textColor);
        if (textSurface == NULL) {
            fprintf(stderr, "Erreur lors du rendu du texte: %s\n", TTF_GetError());
            return 1;
        }
        SDL_Texture* textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
        if (textTexture == NULL) {
            fprintf(stderr, "Erreur lors de la création de la texture du texte: %s\n", SDL_GetError());
            return 1;
        }
        SDL_Rect textRect = { 10, 10, textSurface->w, textSurface->h };
        SDL_RenderCopy(renderer, textTexture, NULL, &textRect);
        SDL_FreeSurface(textSurface);
        SDL_DestroyTexture(textTexture);

        SDL_RenderPresent(renderer);

        aa--;
        if(aa==0){aa=1000;}
    }

    // Nettoyage
    for (int j = 0; j < numRectanglesX; j++) {
        free(rectangles[j]);
    }
    free(rectangles);
    free(bombs);
    free(monsters);
    SDL_DestroyTexture(my_texture1);
    SDL_DestroyTexture(my_texture2);
    SDL_DestroyTexture(my_texture3);
    SDL_DestroyTexture(my_texture4);
    SDL_DestroyTexture(my_texture5);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
