#ifndef ANIMATION_H
#define ANIMATION

#include "main_jeu.h"


void end_sdl(char ok, char const* msg, SDL_Window* window, SDL_Renderer* renderer);
SDL_Texture* load_texture_from_image(char* file_image_name, SDL_Window* window, SDL_Renderer* renderer);
void play_with_texture_1(SDL_Texture* my_texture,SDL_Renderer* renderer, SDL_Rect dest, int division);
void play_with_texture_4(SDL_Texture* my_texture, SDL_Window* window, SDL_Renderer* renderer,SDL_Rect dest,int nb_images,int pas_y,int nb_anim);
void play_with_t_4g(SDL_Texture* my_texture, SDL_Window* window, SDL_Renderer* renderer,SDL_Rect dest,int nb_images,int pas_y,int nb_anim,int nb_y,int deri);

#endif