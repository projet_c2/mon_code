
#include "main_jeu.h"
#include "animation.h"

typedef struct {
    int x;
    int y;
    int health;
} Monster;

typedef struct {
    int x;
    int y;
} Traveler;


typedef struct {
    int playerDirection;
    int diamondDirection;
    int playerDistance;
    int diamondDistance;
    int priority;
    int action;
} Rule;



int calcule_score(Rule * rule ,Monster * monsters , Traveler * traveler);


typedef struct {
    Rule *rules;
    Monster *monster;
    Traveler *traveler;
    int diamondX;
    int diamondY;
    Rule *result;
} ThreadData;

typedef struct {
    Rule *rule;
    Monster *monsters;
    Traveler *traveler;
    int score;
} th_calcul;

/*-----------------------------------------genetique-----------------------------------------------------*/

#define POPULATION 5 // Taille de la population
#define ITERATIONS 5 // Nombre d'itérations

typedef struct {
    int  * rules; // Ordre de visite des rules
    int score; // Évaluation de la qualité de l'Cerveau
} Cerveau;


void initialise_rule(Rule *rule)
{
    rule->priority = rand()%6;
    rule->action = rand()%2+1;
    if(rand()%100 <50)
    {
        rule->playerDirection = rand()%7+1;
        rule->diamondDistance = rand()%3;
        rule->diamondDirection = -1;
        rule->playerDistance = -1;
    }else{
        rule->playerDirection = -1;
        rule->diamondDistance = -1;
        rule->diamondDirection =rand()%7+1;
        rule->playerDistance = rand()%3;
    }
}

void initialise_rulebas(Rule  *rule)
{
    rule->playerDirection =-1;
    rule->diamondDistance = -1;
    rule->diamondDirection = -1;
    rule->playerDistance = -1;
    rule->priority = -1;
    rule->action = -1;
}
void copie_rules(Rule* rule1,Rule* rule2)
{
    rule1->playerDirection  = rule2->playerDirection; 
    rule1->diamondDistance =  rule2->diamondDistance; 
    rule1->diamondDirection  = rule2->diamondDirection ;
    rule1->playerDistance  = rule2->playerDistance ;
    rule1->priority = rule2->priority;
    rule1->action = rule2->action;
}


Rule ** initalisation_cereveau()
{
    Rule ** rules_cerveau= malloc(POPULATION*sizeof(Rule *)); // Coordonnées des rules
    if(rules_cerveau)
    {
        for (int i =0;i<POPULATION;i++)
        {
            rules_cerveau[i]=malloc(MAX_Rule*sizeof(Rule));
            if(rules_cerveau[i])
            {
               initialise_rule(rules_cerveau[i]);
            }
            else{   exit(0);}
       }
    }
    return rules_cerveau;  
}
/*
Rule  ** rules_cerveau ;
Rule  ** rules_cerveau_fills ;
Rule  ** rules_cerveau_parents ;*/

void liberer(Rule ** mat_rules )
{
    for (int i =0;i<POPULATION;i++)
        {
            free(mat_rules[i]);    
       }
    free(mat_rules);
}

void creerPopulationInitiale(Cerveau *population, int taillePopulation)
{
    for (int i = 0; i < taillePopulation; i++) {
        // Génération aléatoire d'un ordre de utilisation  des rules
        for (int j = 0; j < MAX_Rule; j++) {
            population[i].rules[j] = j;
        }
        for (int j = MAX_Rule - 1; j > 0; j--) {
            int index = rand() % (j + 1);
            int temp = population[i].rules[j];
            population[i].rules[j] = population[i].rules[index];
            population[i].rules[index] = temp;
        }
    }
}

int evaluerrules(int  * index_rules, int j,Monster *monsters, Traveler *traveler,Rule  ** rules_cerveau ) {
    int distanceTotale = 0;
    for (int i = 0; i < MAX_Rule ; i++) {
        int ruleActuelle = index_rules[i];
        distanceTotale += (calcule_score(&rules_cerveau[j][ruleActuelle], monsters,traveler)/ MAX_Rule);
    }
    return distanceTotale;
}

void evaluerPopulation(Cerveau *population, int taillePopulation,Monster *monsters, Traveler *traveler,Rule  ** rules_cerveau ) {
    for (int i = 0; i < taillePopulation; i++) {
        int distancerules = evaluerrules(population[i].rules, i,monsters,traveler,rules_cerveau);
        population[i].score = distancerules;
    }
}

void selectionnerParents(Cerveau *population, Cerveau parents[], int taillePopulation,Rule  ** rules_cerveau ,Rule  ** rules_cerveau_parents) {
    for (int i = 0; i < taillePopulation; i++) {
        parents[i] = population[i];
        for ( int j=0;j<MAX_Rule;j++){
            copie_rules(&rules_cerveau_parents[i][j],&rules_cerveau[i][j]);
        }
        
    }
}

void croiserParents(Cerveau parents[], Cerveau enfants[], int tailleParents,Rule  ** rules_cerveau_parents ,Rule  ** rules_cerveau_fills ) {
    for (int i = 0; i < tailleParents; i++) {
        int indexParent1 = rand() % (tailleParents);
        int indexParent2 = rand() % (tailleParents);

        int pointDeCoupure1 = rand() % MAX_Rule;
        int pointDeCoupure2 = rand() % MAX_Rule;

        if (pointDeCoupure1 > pointDeCoupure2) {
            int temp = pointDeCoupure1;
            pointDeCoupure1 = pointDeCoupure2;
            pointDeCoupure2 = temp;
        }

        int* parent1 = parents[indexParent1].rules;
        int* parent2 = parents[indexParent2].rules;

        int* enfant = enfants[i].rules;

        for (int j = 0; j < MAX_Rule; j++) {
            if (j >= pointDeCoupure1 && j <= pointDeCoupure2) {
                enfant[j] = parent1[j];
                
                if((enfant[j]!=-1) && (parent1[j]!=-1))
                {
                    Rule * rule1 = &rules_cerveau_fills[i][enfant[j]] , * rule2 = &rules_cerveau_parents[indexParent1][parent1[j]];
                    rule1->playerDirection  = rule2->playerDirection; 
                   rule1->diamondDistance =  rule2->diamondDistance; 
                   rule1->diamondDirection  = rule2->diamondDirection ;
                   rule1->playerDistance  = rule2->playerDistance ;
                  rule1->priority = rule2->priority;
                  rule1->action = rule2->action;
                    }
            } else {
                enfant[j] = -1;
            }
        }

        int indexParent2Copy = 0;
        int rule = -2;
        for (int j = 0; j < MAX_Rule; j++) {
            if (enfant[j] == -1) {
                
                while (enfant[j] == -1) {
                    if(indexParent2Copy+1<MAX_Rule )   rule= parent2[indexParent2Copy++];
                    else                             break;
                    //printf("rule %d\n", indexParent2Copy);
                    int estDejaPresent = 0;
                    for (int k = 0; k < MAX_Rule; k++) {
                        if (enfant[k] == rule) {
                            estDejaPresent = 1;
                            break;
                        }
                    }
                    if (!estDejaPresent) {
                        enfant[j] = rule;
                        if((rule!=-1) && (enfant[j]!=-1))
                        {
                            Rule * rule2 = &rules_cerveau_fills[i][enfant[j]];
                            Rule * rule3 = &rules_cerveau_parents[indexParent2][rule];
                            rule2->playerDirection  = rule3->playerDirection; 
                            rule2->diamondDistance =  rule3->diamondDistance; 
                            rule2->diamondDirection  = rule3->diamondDirection ;
                            rule2->playerDistance  = rule3->playerDistance ;
                            rule2->priority = rule3->priority;
                            rule2->action = rule3->action;
                        }

                    }
                }
            }
        }
    }
}

void muterEnfants(Cerveau enfants[], int tailleEnfants) {
    for (int i = 0; i < tailleEnfants; i++) {
        if (rand() % 100 < 5) { // Probabilité de mutation de 5%
            int index1 = rand() % MAX_Rule;
            int index2 = rand() % MAX_Rule;
            int temp = enfants[i].rules[index1];
            enfants[i].rules[index1] = enfants[i].rules[index2];
            enfants[i].rules[index2] = temp;
        }
    }
}

void remplacerPopulation(Cerveau *population, Cerveau enfants[], int taillePopulation,Rule  ** rules_cerveau,Rule  ** rules_cerveau_fills) {
    for (int i = 0; i < taillePopulation; i++) {
        population[i] = enfants[i];
        for ( int j=0;j<MAX_Rule;j++){
            copie_rules(&rules_cerveau[i][j],&rules_cerveau_fills[i][j]);
        }

    }
}

void algorithmeGenetique(int taillePopulation, int nombreIterations,Monster *monsters, Traveler *traveler,Rule  ** rules_cerveau,Rule  ** rules_cerveau_parents ,Rule  ** rules_cerveau_fills) {
    Cerveau *population = calloc(taillePopulation,sizeof(Cerveau));
    Cerveau *parents = calloc(taillePopulation,sizeof(Cerveau));
    Cerveau *enfants= calloc(taillePopulation,sizeof(Cerveau));
    for(int i=0; i<taillePopulation;i++)
    {
        population[i].rules= malloc(MAX_Rule* sizeof(int));
        population[i].score= 0;
        parents[i].rules= malloc(MAX_Rule *sizeof(int));
        parents[i].score= 0;
        enfants[i].rules= malloc(MAX_Rule * sizeof(int));
        enfants[i].score=0;
        for(int j = 0; j<MAX_Rule;j++)
        {
            population[i].rules[j]=j;
            parents[i].rules[j]=j;
            enfants[i].rules[j]=j;

        }
    }
    

    creerPopulationInitiale(population, taillePopulation);

    for (int iteration = 0; iteration < nombreIterations; iteration++) {
        evaluerPopulation(population, taillePopulation,monsters,traveler,rules_cerveau);

        int meilleurscore = population[0].score;
         int indexMeilleur = 0;
        for (int i = 1; i < taillePopulation; i++) {
            if (population[i].score > meilleurscore) {
               meilleurscore = population[i].score;
               indexMeilleur = i;
            }
        }
        printf("Meilleur Cerveau (rules) : %d \n",indexMeilleur);
        printf("Meilleur Cerveau (score) : %d\n", population[indexMeilleur].score);
        selectionnerParents(population, parents, taillePopulation,rules_cerveau,rules_cerveau_parents);
        croiserParents(parents, enfants, taillePopulation,rules_cerveau_parents,rules_cerveau_fills);
        muterEnfants(enfants, taillePopulation);
        remplacerPopulation(population, enfants, taillePopulation, rules_cerveau,rules_cerveau_fills);
    }

     for(int i=0; i<taillePopulation;i++)
    {
        //free(enfants[i].rules);
        free(population[i].rules);
    }

    free(population);
    free(parents);
    free(enfants);
}
/*----------------------------------------fin genetique---------------------------------------------------*/


int direction_signal(Monster * monster, int a,int b){
    int dx = a - monster->x;
    int dy = b - monster->y;
    int direction =-1;

    if(0==dx || 0==dy){
        if(0==dx){
            if(dy>0)   direction= 1;
            else       direction= 5;
        }
        else{
            if(dx>0)    direction= 3;
            else        direction = 7;
        }
    }
    
    if(dx>0 && dy>0){
        direction= 2;
    }
    if(dx>0 && dy<0){
        direction= 4;
    }
    if(dx<0 && dy>0){
        direction= 8;
    }
    if(dx<0 && dy<0){
        direction= 6;
    }


    return direction;

}


int distance_signal(Monster * monster, int a , int b) {
    int dx = abs(a - monster->x);
    int dy = abs(b - monster->y);

    if (dx + dy < 10) {
        return 0;  // Distance inférieure à 80
    } else if (dx + dy >= 10 && dx + dy <= 30) {
        return 1;  // Distance entre 80 et 150
    } else {
        return -1;  // Distance supérieure à 150
    }
}


// Fonction pour déplacer le monstre vers le voyageur
void moveToTraveler(int playerDirection,Monster* monster) {
    int a=playerDirection;

    switch(a){
        case 1 :
            monster->y++;
            break;
        case 2 :
            monster->y++;
            monster->x++;
            break;
        case 3 :
             monster->x++;
             break;
        case 4 :
            monster->x++;
            monster->y--;
            break;
        case 5 :
            monster->y--;
            break;
        case 6 :
            monster->x--;
            monster->y--;
            break;
        case 7 :
            monster->x--;
            break;
        case 8 :
            monster->x--;
            monster->y++;
            break;    
    }

}

void moveToDiamond_joueur(Traveler * traveler ,  int diamondX , int diamondY , int numRectanglesX , int numRectanglesY) {
    int distanceX = traveler->x - diamondX;
    int distanceY = traveler->y - diamondY;
    int a=0;

    if ((distanceX<=0) && traveler->x+1<numRectanglesX  ) {
        traveler->x++;
        a=1;
        if ((distanceY<0) && traveler->y+1<numRectanglesY  )  {
        traveler->y++;
       } else if ((distanceY>0) && traveler->y-1>-1 ){
        traveler->y--;
    }
    } else{
        if ((distanceX>0) && traveler->x-1>-1 ) {
        traveler->x--;
        a=1;
        if ((distanceY<0) && traveler->y+1<numRectanglesY ) {
        traveler->y++;
        } else if ((distanceY>0) && traveler->y-1>-1 ) {
        traveler->y--;
    }
    }   


    }

    if(0==a) {if ((distanceY<=0) && traveler->y+1<numRectanglesY) {
        traveler->y++;
    } else if ((distanceY>0) && traveler->y-1>-1) {
        traveler->y--;
    }}   
}


void moveToDiamond(int diamondDirection ,Monster* monster) {
    int a=diamondDirection;

    switch(a){
        case 1 :
            monster->y++;
            break;
        case 2 :
            monster->y++;
            monster->x++;
            break;
        case 3 :
             monster->x++;
             break;
        case 4 :
            monster->x++;
            monster->y--;
            break;
        case 5 :
            monster->y--;
            break;
        case 6 :
            monster->x--;
            monster->y--;
            break;
        case 7 :
            monster->x--;
            break;
        case 8 :
            monster->x--;
            monster->y++;
            break;   
    }   
}
/*
void *makeDecisionThread(void *arg) {
    ThreadData *data = (ThreadData *)arg;

    int bestScore = -1;
    int bestRuleIndex = -1;

    // Obtenez les données nécessaires de ThreadData
    Rule *rules = data->rules;
    Monster *monster = data->monster;
    Traveler *traveler = data->traveler;
    int diamondX = data->diamondX;
    int diamondY = data->diamondY;

    int playerDirection = direction_signal(monster, traveler->x, traveler->y);
    int diamondDirection = direction_signal(monster, diamondX, diamondY);
    int playerDistance = distance_signal(monster, traveler->x, traveler->y);
    int diamondDistance = distance_signal(monster, diamondX, diamondY);

    for (int i = 0; i < MAX_Rule; i++) {
        Rule currentRule = rules[i];
        if (currentRule.playerDirection == playerDirection && currentRule.diamondDirection == diamondDirection &&
            currentRule.playerDistance == playerDistance && currentRule.diamondDistance == diamondDistance) {
            data->result = &rules[i];
            pthread_exit(NULL);
        }

        int score = currentRule.priority;//calcule_score(&currentRule, monster, traveler);

        if (score > bestScore) {
            bestScore = score;
            bestRuleIndex = i;
        }
    }

    data->result = &rules[bestRuleIndex];
    pthread_exit(NULL);
}

Rule *makeAIDecision_app(Rule *rules, Monster *monster, Traveler *traveler, int diamondX, int diamondY) {
    pthread_t thread;
    ThreadData data;
    data.rules = rules;
    data.monster = monster;
    data.traveler = traveler;
    data.diamondX = diamondX;
    data.diamondY = diamondY;

    pthread_create(&thread, NULL, makeDecisionThread, (void *)&data);
    pthread_join(thread, NULL);

    pthread_detach(thread);

    return data.result;
}*/
void updateAI_rule(Rule * rule ,Monster * monster) {
    int a = rule->action;
    a=2;
    switch (a) {
        case 1:
            
            moveToTraveler(rule->playerDirection,monster);  
            //action = makeAIDecision_app(rule);
            break;
        case 2:
            
                moveToDiamond(rule->diamondDirection,monster);
                //action = makeAIDecision_app(rule);

            break;

        default  : 
           // reste immobile
              break;
         }

    
}


/*
void updateAI_app(Rule * rules ,Monster * monster, Traveler * traveler , int diamondX , int diamondY ) {


    Rule * rule_choisi = makeAIDecision_app(rules,monster,traveler,diamondX,diamondY);


    switch (rule_choisi->action) {
        case 1:
            
            moveToTraveler(rule_choisi->playerDirection,monster);  
            //action = makeAIDecision_app(rule);
            break;
        case 2:
            
                moveToDiamond(rule_choisi->diamondDirection,monster);
                //action = makeAIDecision_app(rule);

            break;

        default  :  // reste immobile
              break;
         }

    
}
*/


void *calcScoreThread(void *arg) {
    th_calcul *data = (th_calcul *)arg;

    // Récupérer les données nécessaires de th_calcul
    Rule *rule = &data->rule[0];
    Monster *monsters = data->monsters;
    Traveler *traveler = data->traveler;

    int monsterX = 0;
    int monsterY = 0;
    int aa = 100;
    int gameOver = 1;

    // Initialisation de SDL
    /*if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Erreur lors de l'initialisation de SDL : %s\n", SDL_GetError());
        data->score = 0;
        return NULL;
    }*/

    //SDL_DisplayMode screen;

    //SDL_GetCurrentDisplayMode(0, &screen);
      // printf("Résolution écran\n\tw : %d\n\th : %d\n",screen.w, screen.h);

    int SCREEN_WIDTH = (int)(2500 * 0.66);
    int SCREEN_HEIGHT = (int)(1600* 0.66);

    int numRectanglesX = SCREEN_WIDTH / RECTANGLE_SIZE;
    int numRectanglesY = SCREEN_HEIGHT / RECTANGLE_SIZE;

     int diamondX = numRectanglesX/2;
     int diamondY = numRectanglesY/2;


    


    
     

    


    
     
    


    // Génération aléatoire du labyrinthe
    srand(time(NULL));

    
    SDL_Rect** rectangles = (SDL_Rect**)malloc(numRectanglesX * sizeof(SDL_Rect*));
    if (rectangles == NULL) {
        printf("Erreur lors de l'allocation de mémoire pour les rectangles.\n");
        SDL_Quit();
        return NULL;
    }

    for (int i = 0; i < numRectanglesX; i++) {
        rectangles[i] = (SDL_Rect*)malloc(numRectanglesY *sizeof(SDL_Rect));
        
        if (rectangles[i] == NULL) {
            printf("Erreur lors de l'allocation de mémoire pour les rectangles.\n");
            for (int j = 0; j < i; j++) {
                free(rectangles[j]);
            }
            free(rectangles);
            SDL_Quit();
            return NULL;
        }
        for (int j = 0; j < numRectanglesY; j++) {
            SDL_Rect rectangle = {i * RECTANGLE_SIZE, j * RECTANGLE_SIZE, RECTANGLE_SIZE, RECTANGLE_SIZE};
            rectangles[i][j] = rectangle;
        }
    }

    // Position initiale du traveler
    traveler->x = numRectanglesX/2;
    Rule rula = {1 , direction_signal(&monsters[0] , diamondX , diamondY) , 1 , 1 , 5 , 2};
    traveler->y = numRectanglesY/2;

    // Initialisation de la position des rand() % numRectanglesY
    for (int i = 0; i < MAX_MONSTERS; i++) {
        monsters[i].x = numRectanglesX/3 ;
        monsters[i].y = numRectanglesY/3;
        monsters[i].health = 15;
    }
    
    int gameRunning = 1;


    int score_joueur=0;
    int score = 0;
    
    while (gameRunning) {


        if(aa==100){
            moveToDiamond_joueur(traveler,  diamondX , diamondY , numRectanglesX , numRectanglesY);
        

        if (monsterX == traveler->x && monsterY == traveler->y) {
                    gameOver = 1;
            }


        // Manger le diamant
        if (traveler->x == diamondX && traveler->y == diamondY) {
            score_joueur ++;
            monsters->health -= 1;
            diamondX = rand() % numRectanglesX;
            diamondY = rand() % numRectanglesY;
        }


        if (monsters->x == traveler->x && monsters->y == traveler->y) {
                    gameOver = 1;
                    score++;
                    monsters->health +=2;

            }
        updateAI_rule(rule,&monsters[0]);
        
            // Vérification si le monstre entre en collision avec le joueur
        if (monsters->x == traveler->x && monsters->y == traveler->y) {
                    gameOver = 1;
                    score++;
                    monsters->health +=2;

            }

        }
        if (gameOver) {
 
            // Réinitialiser les positions du traveler et des bombes
            traveler->x=numRectanglesX/2;
            traveler->y = numRectanglesY/2;

            diamondX = rand() % numRectanglesX;
            diamondY = rand() % numRectanglesY;
            rula.diamondDirection= direction_signal(&monsters[0] , diamondX , diamondY);

            // Réinitialiser l'état du jeu
            gameOver = 0;

           score_joueur =0;

        }


        
        
        aa--;
        if(aa==0){aa=100;}


        if((monsters->health<0) || (monsters->health>30))  break;
    
    }

    // Nettoyage
    for (int j = 0; j < numRectanglesX; j++) {
        free(rectangles[j]);
    }
    free(rectangles);
    SDL_Quit();

    data->score = score;

    return NULL;
}

int calcule_score(Rule *rule, Monster *monsters, Traveler *traveler) {
    pthread_t thread ;
    th_calcul  * data = calloc(1, sizeof(th_calcul));
    data->rule = rule;
    data->monsters = monsters;
    data->traveler = traveler;
    data->score = 0;

    pthread_create(&thread, NULL, calcScoreThread, (void *)data);
    pthread_join(thread, NULL);

    pthread_detach(thread);
    int score =data->score;
    free(data);
    return score;
}



int optimisat(int j ,int k, Rule  * rules, Monster * monsters , Traveler * traveler){
    int scoremax=0;
    switch(j){
        case 0  :
                for(int i=0;i<3;i++){
                rules[k].playerDirection=i;
                int score =calcule_score(rules,monsters, traveler);
                if(score>scoremax){   scoremax=score ;}
                 }
              break;
        case 1 :
            for(int i=0;i<3;i++){
            rules[k].diamondDirection=i;
            int score =calcule_score(rules,monsters, traveler);
            if(score>scoremax){   scoremax=score ;}
            }
            break;
        case 2 :
            for(int i=0;i<3;i++){
            rules[k].playerDistance=i;
            int score =calcule_score(rules,monsters, traveler);
            if(score>scoremax){   scoremax=score ;}
            }
            break;
        case 3 :
            for(int i=0;i<3;i++){
            rules[k].diamondDistance=i;
            int score =calcule_score(rules,monsters, traveler);
            if(score>scoremax){   scoremax=score ; }
            }
            break;

        case 4 :
            for(int i=0;i<5;i++){
            rules[k].priority=i;
            int score =calcule_score(rules,monsters, traveler);
            if(score>scoremax){   scoremax=score ;}
            }
            break;
        
        case 5 :
            for(int i=1;i<4;i++){
            rules[k].action=i;
            int score =calcule_score(rules,monsters, traveler);
            if(score>scoremax){   scoremax=score ;}
            }
            break;
    }

    return scoremax;
    
}


void  meilleure_score(Rule * rules, Monster * monsters, Traveler * traveler )
{
    for(int i=0; i<MAX_Rule;i++)
    {
        for(int j=0; j<6 ; j++)
        {
           int score = optimisat(j,i, rules,monsters, traveler);
           printf("\nla score de regle %d N_facteur %d: \t %d\n" ,i,j, score );
        }


        printf("\n\n");
    }

}

int main() {

    
    /*Rule rules[] = {{1,2,0,1 , 4, 2},
            {1,1,2,1, 3, 1},
            {1,1,2,0, 4, 3},
            {2,1,2,1, 2, 1},
            {4,5,2,2, 1, 2},
            {3,2,0,0, 3, 1},
            {3,6,1,1, 3, 1},
            {2,8,1,1, 3, 2},
            {7,4,1,2, 4, 1},
            {5,2,2,0, 3, 2},
            {6,5,2,2, 4, 2},
            {8,1,2,1, 2, 1} 
    };*/

    Rule ** rules_cerveau =initalisation_cereveau();
    Rule ** rules_cerveau_fills =initalisation_cereveau();
    Rule ** rules_cerveau_parents =initalisation_cereveau();

    Monster * monsters = calloc(MAX_MONSTERS,sizeof(Monster));
    if(monsters==NULL){
        return 1;
    }
    monsters[0].x=20;
    monsters[0].y=8;
    monsters[0].health=190;
    
    
    Traveler * traveler = malloc(1*sizeof(Traveler));
    if(traveler==NULL)   return 0;

    traveler->x=0;
    traveler->y=0;


    //meilleure_score(rules, &monsters[0], traveler);

    /*Rule * rule = makeAIDecision_app(rules , &monsters[0] , traveler , 20 , 30);
    printf("\n %d %d %d %d %d %d" ,rule->playerDirection , rule->diamondDirection , rule->playerDistance , rule->diamondDistance , rule->priority , rule->action);*/
    srand(time(NULL));

    algorithmeGenetique(POPULATION, ITERATIONS,&monsters[0], traveler, rules_cerveau,rules_cerveau_parents,rules_cerveau_fills);

   liberer(rules_cerveau);
   liberer(rules_cerveau_parents);
   liberer(rules_cerveau_fills);
    free(monsters);
    free(traveler);

    return 0;
}
