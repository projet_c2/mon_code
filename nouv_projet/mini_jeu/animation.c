#include "animation.h"


void end_sdl(char ok, char const* msg, SDL_Window* window, SDL_Renderer* renderer) {
    char msg_formated[255];
    int l;

    if (!ok) {
        strncpy(msg_formated, msg, 250);
        l = strlen(msg_formated);
        strcpy(msg_formated + l, " : %s\n");

        SDL_Log(msg_formated, SDL_GetError());
    }

    if (renderer != NULL) {
        SDL_DestroyRenderer(renderer);
        renderer = NULL;
    }

    if (window != NULL) {
        SDL_DestroyWindow(window);
        window = NULL;
    }

    SDL_Quit();

    if (!ok) {
        exit(EXIT_FAILURE);
    }
}

SDL_Texture* load_texture_from_image(char* file_image_name, SDL_Window* window, SDL_Renderer* renderer) {
    SDL_Surface* my_image = NULL;
    SDL_Texture* my_texture = NULL;

    my_image = IMG_Load(file_image_name);
    if (my_image == NULL)
        end_sdl(0, "Chargement de l'image impossible", window, renderer);

    my_texture = SDL_CreateTextureFromSurface(renderer, my_image);
    SDL_FreeSurface(my_image);
    if (my_texture == NULL)
        end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);

    return my_texture;
}

void play_with_texture_1(SDL_Texture* my_texture, SDL_Renderer* renderer, SDL_Rect dest, int division) {
    SDL_Rect source = {0},
              destination = {0};
    destination = dest;
    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);
    if(division == 0){

        SDL_RenderCopy(renderer, my_texture, &source, &destination);
    }
    else {
        SDL_Rect state = {0};
        int offset_x = source.w / 6.5;
        int offset_y = source.h / 4;

        state.x = 0;
        state.y = 0* offset_y;
        state.w = offset_x;
        state.h = offset_y;

        SDL_RenderCopy(renderer, my_texture, &state, &destination);

    }

    

    
}

void play_with_texture_4(SDL_Texture* my_texture, SDL_Window* window, SDL_Renderer* renderer,SDL_Rect dest,int nb_images,int pas_y,int nb_anim) {
    SDL_Rect source = {0};
    SDL_Rect window_dimensions = {0};
    SDL_Rect destination = {0};
    SDL_Rect state = {0};

    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);

    int offset_x = source.w / nb_images;
    int offset_y = source.h / pas_y;

    state.x = 0;
    state.y = 0 * offset_y;
    state.w = offset_x;
    state.h = offset_y;

   destination = dest;

    //int speed = 0;
    int x = 0;

    while (x < nb_anim) {
        x++;
        state.x += offset_x;
        state.x %= source.w;
        if (state.x == 0) {
            state.y += offset_y;
            state.y %= source.h;
        }

        SDL_RenderCopy(renderer, my_texture, &state, &destination);
        SDL_RenderPresent(renderer);
        SDL_Delay(50);
    }

    SDL_RenderClear(renderer);
}

void play_with_t_4g(SDL_Texture* my_texture, SDL_Window* window, SDL_Renderer* renderer,SDL_Rect dest,int nb_images,int pas_y,int nb_anim,int nb_y,int division) {
    SDL_Rect source = {0};
    SDL_Rect window_dimensions = {0};
    SDL_Rect destination = {0};
    SDL_Rect state = {0};

    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);

    
    int offset_x = source.w / nb_images;
    int offset_y = source.h / pas_y;

    state.x = 0;
    state.y = nb_y * offset_y;
    state.w = offset_x;
    state.h = offset_y;

   destination = dest;

    //int speed = 0;
    int x = 0;

    while (x < nb_anim) {
        x++;
        destination.x -= x*division/(nb_anim);
        state.x += offset_x;
        state.x %= source.w;
        SDL_RenderCopy(renderer, my_texture, &state, &destination);
        SDL_RenderPresent(renderer);
        SDL_Delay(50);
    }

}