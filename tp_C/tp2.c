#include <stdio.h>
#include <math.h>


void prog1()
{
    printf("entrer un nombre n : \n");
    int n;
    scanf("%d", &n);
    printf("les divisions entieres de %d par 2 sont : " , n);
    while (n!=0)
    {
         printf("%d ; ", n/2);
         n = n/2 ;
    }
    printf ("\n\n");
    printf(" fin de -----------------question 1-----------------------\n");


}

void choix_somme()
{
   int n;
   printf("entrer un nombre n : \n");
   scanf("%d", &n);
   int c;
   printf("entrer 1 pour somme n ou 2 pour somme n² ou -1 pour somme 1/n ou -2 pour somme 1/n² :\n");
   scanf("%d",&c);
   
   int i=0;
   int s=0;
   float r=0;
       
   switch (c)
   {
       case 1 :
           for (i=0;i<=n; i++)
           {
             s= s + i;
           }
           printf("la somme de n est : %d\n" , s);
           break;
       case 2 :  
           for (i=0;i<=n; i++)
           {
             s= s + i*i ;
           }
           printf("la somme de n² est : %d\n" , s);
           break;
       
       case -1 :
           for (i=1;i<=n; i++)
           {
             r= r + (1.0/i) ;
           }
           printf("la somme de 1/n est : %.11f\n" , r);
           break;
      
      case -2 :
           for (i=1;i<=n; i++)
           {
             r= r + (1.0/(i*i));
           }
           printf("la somme de 1/n² est : %.11f\n" , r);
           break;   
   }
   
   printf(" fin de -----------------question 2------------------------\n");


}


void fact_N()
{
   long int n ;
   printf("entrer un nombre n : \n");
   scanf("%ld", &n);
   long int p = 1 ;
   long int i = 1;
   for(i=1; i<=n ; i++)
   {
       p = p*i;
   }
   printf("lz factoriel de %lu est : %lu \n" , n , p);
   
   printf(" fin de -----------------question 3------------------------\n");
}

long int fact_R(long int n)
{
    if (n>=0)
    {
         if (n==0)        return 1;
         else             return n*fact_R(n-1);
    }

}




int main()
{
   
   prog1();
   choix_somme();
   fact_N();
   
   int n;
   printf("entrer un nombre n : \n");
   scanf("%d", &n);
   printf("le factoriel de facon recursif de %d est : %ld\n",n, (long int)fact_R(n));
   
   printf(" fin de -----------------question 4------------------------\n");

}


