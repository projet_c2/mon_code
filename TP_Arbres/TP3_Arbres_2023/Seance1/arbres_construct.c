/**
 * @file arbres_construct.c
 * @brief fichier d'implementation du programme pour la construction d'une arborescence
 */
#include <stdio.h>
#include <stdlib.h>

#include "../pile.h"
#include "../eltsArbre.h"
#include "arbres_construct.h"

/**
 * @brief lire le fichier contenant la representation prefixee de l'arborescence
 * @param [in] fileName le nom du fichier contenant la representation prefixee
 * @param [in, out] tabEltPref tableau des elements de la representation prefixee
 * @param [in, out] nbEltsPref l'adresse memoire contenant le nombre des elements du tabEltPref
 * @return le nombre de racines
 */
int lirePref_fromFileName (char * fileName , eltPrefPostFixee_t * tabEltPref , int * nbEltsPref )
{
    int nbRacines = 0;
    FILE* file = fopen(fileName,"r");
    if( file != NULL)
    {
        int i=0;
        fscanf(file,"%d",&nbRacines);
        while ((fscanf(file," %c",&(tabEltPref+i)->val)!=EOF)&&(fscanf(file," %d",&(tabEltPref+i)->nbFils)!=EOF))
        {
            i++;
        }
        (*nbEltsPref) = i;
    }
    fclose( file );
    return nbRacines;

}

/** TO DO
 * @brief afficher les elements de la representation prefixee sur un flux de sortie
 * @param file : le flux de sortie
 * @param [in, out] tabEltPref tableau des elements de la representation prefixee
 * @param [in, out] nbEltsPref le nombre des elements du tabEltPref
 */
void printTabEltPref(FILE *file, eltPrefPostFixee_t * tabEltPref, int nbEltsPref)
{
    if (nbEltsPref!=0)
    {
        int i=0;
        while (i<nbEltsPref-1)
        {
            fprintf(file,"(%c,%d) ",tabEltPref[i].val,tabEltPref[i].nbFils);
            i++;
        } 
        fprintf(file,"(%c,%d)\n",tabEltPref[nbEltsPref-1].val,tabEltPref[nbEltsPref-1].nbFils);
    }
}

/** TO DO
 * @brief creer et initialiser un nouveau point de l'arborescence
 * @param [in] val la valeur du point
 * @return l'adresse du nouveau point 
 */
cell_lvlh_t * allocPoint(char val)
{
    cell_lvlh_t *new = malloc(sizeof(cell_lvlh_t));
    if(new != NULL)
    {
        (*new).val = val ;
        (*new).lv = NULL;
        (*new).lh = NULL;
    }
    return new;
}

/** TO DO
 * @brief construire un arbre avec lvlh a partir de representation prefixee
 * @param [in] tabEltPref tableau des elements de la representation prefixee
 * @param [in] nbRacines nombre de racines de l'arborescence
 * @return : 
 *     - NULL si l'arbre resultatnt est vide
 *     - l'adresse de la racine de l'arbre sinon
*/
cell_lvlh_t * pref2lvlh(eltPrefPostFixee_t * tabEltPref, int  nbRacines )
{                
    pile_t * pile = initPile (NB_ELTPREF_MAX);
    eltType * element_pile = malloc(sizeof(eltType));
    cell_lvlh_t * new =NULL;
    int code = 0;
    char c;
    cell_lvlh_t * adrtete = NULL ;
    cell_lvlh_t ** prec = &adrtete;
    int nbFils_ou_Freres = nbRacines;
    //eltPrefPostFixee_t * cour = tabEltPref ;
    int i=0;
    while ( (nbFils_ou_Freres > 0) || (!estVidePile( pile)) )
    {
        if ((nbFils_ou_Freres > 0))
        { 
            c = tabEltPref[i].val;
            new = allocPoint(c);
            (*prec) = new ;
            element_pile->adrCell = new ;
            element_pile->adrPrec= &(new->lh);
            element_pile->nbFils_ou_Freres = nbFils_ou_Freres-1 ;
            empiler(pile,element_pile,&code);
            prec = &(new->lv);
            nbFils_ou_Freres = tabEltPref[i].nbFils;
            i = i +1;
            //printf( "voila : %c\n", adrtete->val);
        }
        else 
        {
            if (!estVidePile(pile)){
                depiler(pile,element_pile,&code);
                prec=element_pile->adrPrec;
                nbFils_ou_Freres = element_pile->nbFils_ou_Freres;
            }
        }
    }
    free (element_pile);
    libererPile(&pile);
    return adrtete;
}

/** TO DO
 * @brief liberer les blocs memoire d'un arbre
 * @param [in] adrPtRacine l'adresse du pointeur de la racine d'un arbre
 */

void libererArbre(cell_lvlh_t ** adrPtRacine)
{
    cell_lvlh_t * racine = (* adrPtRacine);
    if( racine != NULL)
    {
        pile_t *pile = initPile(NB_ELTPREF_MAX);
        eltType * element_pile =  malloc(sizeof(eltType));
        int code = 0;
        element_pile->adrCell = racine ;
        element_pile->adrPrec= &racine;
        element_pile->nbFils_ou_Freres = 1 ;
        empiler(pile,element_pile,&code);
        while (!estVidePile(pile)){
            depiler(pile,element_pile,&code);
            cell_lvlh_t * cour = element_pile->adrCell;
            
            element_pile->adrPrec= &cour;
            element_pile->nbFils_ou_Freres = 1 ;

            if(cour->lv != NULL)
            {
                element_pile->adrCell = cour->lv;
                empiler(pile,element_pile,&code);
            }
            if( cour->lh != NULL)
            {
                element_pile->adrCell = cour->lh; 
                empiler(pile,element_pile,&code);
            }
            free(cour);
        }
       free (element_pile);
       libererPile(&pile);

    }

}




