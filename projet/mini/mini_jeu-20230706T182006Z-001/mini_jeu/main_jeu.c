
#include "main_jeu.h"
#include "animation.h"




typedef struct {
    int x;
    int y;
    int health;
} Monster;

typedef struct {
    int x;
    int y;
} Traveler;


typedef struct {
    int playerDirection;
    int diamondDirection;
    int playerDistance;
    int diamondDistance;
    int priority;
    int action;
} Rule;


int gameOver = 0;

int score =  0;

int calcule_score(Rule * rule ,Monster * monsters , Traveler * traveler);


typedef struct {
    Rule *rules;
    Monster *monster;
    Traveler *traveler;
    int diamondX;
    int diamondY;
    Rule *result;
} ThreadData;

typedef struct {
    Rule *rule;
    Monster *monsters;
    Traveler *traveler;
    int score;
} th_calcul;





int direction_signal(Monster * monster, int a,int b){
    int dx = a - monster->x;
    int dy = b - monster->y;
    int direction =-1;

    if(0==dx || 0==dy){
        if(0==dx){
            if(dy>0)   direction= 1;
            else       direction= 5;
        }
        else{
            if(dx>0)    direction= 3;
            else        direction = 7;
        }
    }
    
    if(dx>0 && dy>0){
        direction= 2;
    }
    if(dx>0 && dy<0){
        direction= 4;
    }
    if(dx<0 && dy>0){
        direction= 8;
    }
    if(dx<0 && dy<0){
        direction= 6;
    }


    return direction;

}


int distance_signal(Monster * monster, int a , int b) {
    int dx = abs(a - monster->x);
    int dy = abs(b - monster->y);

    if (dx + dy < 10) {
        return 0;  // Distance inférieure à 80
    } else if (dx + dy >= 10 && dx + dy <= 30) {
        return 1;  // Distance entre 80 et 150
    } else {
        return -1;  // Distance supérieure à 150
    }
}


// Fonction pour déplacer le monstre vers le voyageur
void moveToTraveler(int playerDirection,Monster* monster) {
    int a=playerDirection;

    switch(a){
        case 1 :
            monster->y++;
            break;
        case 2 :
            monster->y++;
            monster->x++;
            break;
        case 3 :
             monster->x++;
             break;
        case 4 :
            monster->x++;
            monster->y--;
            break;
        case 5 :
            monster->y--;
            break;
        case 6 :
            monster->x--;
            monster->y--;
            break;
        case 7 :
            monster->x--;
            break;
        case 8 :
            monster->x--;
            monster->y++;
            break;    
    }

}

void moveToDiamond_joueur(Traveler * traveler ,  int diamondX , int diamondY , int numRectanglesX , int numRectanglesY) {
    int distanceX = traveler->x - diamondX;
    int distanceY = traveler->y - diamondY;
    int a=0;

    if ((distanceX<=0) && traveler->x+1<numRectanglesX  ) {
        traveler->x++;
        a=1;
        if ((distanceY<0) && traveler->y+1<numRectanglesY  )  {
        traveler->y++;
       } else if ((distanceY>0) && traveler->y-1>-1 ){
        traveler->y--;
    }
    } else{
        if ((distanceX>0) && traveler->x-1>-1 ) {
        traveler->x--;
        a=1;
        if ((distanceY<0) && traveler->y+1<numRectanglesY ) {
        traveler->y++;
        } else if ((distanceY>0) && traveler->y-1>-1 ) {
        traveler->y--;
    }
    }   


    }

    if(0==a) {if ((distanceY<=0) && traveler->y+1<numRectanglesY) {
        traveler->y++;
    } else if ((distanceY>0) && traveler->y-1>-1) {
        traveler->y--;
    }}


    

    
}

void moveToDiamond(int diamondDirection ,Monster* monster) {
    int a=diamondDirection;

    switch(a){
        case 1 :
            monster->y++;
            break;
        case 2 :
            monster->y++;
            monster->x++;
            break;
        case 3 :
             monster->x++;
             break;
        case 4 :
            monster->x++;
            monster->y--;
            break;
        case 5 :
            monster->y--;
            break;
        case 6 :
            monster->x--;
            monster->y--;
            break;
        case 7 :
            monster->x--;
            break;
        case 8 :
            monster->x--;
            monster->y++;
            break;
           

        
    }


    
}

void moveInCircle(Monster * object , int numRectanglesX , int numRectanglesY) {
    int a=rand();
    if((object->x + (int)(1 * cos(a%3 *(M_PI/2)))<numRectanglesX) && (object->x +(int)(1 * cos(a%3 *(M_PI/2)))>-1 ) && (object->y + (int)(1 * sin(a%3 *(M_PI/2)))<numRectanglesY) && (object->y +(int)(1 * sin(a%3 *(M_PI/2)))>-1 ) )
    {object->x += (int)(1 * cos(a%3 *(M_PI/2)));
    object->y += (int)(1 * sin(a%3 *(M_PI/2)));}
}

/*Rule * makeAIDecision_app(Rule *rules,Monster *monster,Traveler *traveler,int diamondX,int diamondY) {
    int bestScore = -1;
    int bestRuleIndex = -1;

    int playerDirection = direction_signal(monster,traveler->x,traveler->y);
    int diamondDirection = direction_signal(monster,diamondX,diamondY);
    int playerDistance = distance_signal(monster,traveler->x,traveler->y);
    int diamondDistance = distance_signal(monster,diamondX,diamondY);

    for (int i = 0; i < MAX_Rule; i++) {
        // Sauvegarder la règle actuelle pour la comparer avec les autres règles
        Rule currentRule = rules[i];
      if ( currentRule.playerDirection == playerDirection && currentRule.diamondDirection == diamondDirection 
              && currentRule.playerDistance == playerDistance && currentRule.diamondDistance == diamondDistance)
              {
                return &rules[i];
              }
        // Appliquer la règle actuelle au jeu et calculer le score correspondant
        int score = calcule_score(&currentRule, monster, traveler);

        // Vérifier si le score obtenu est supérieur au meilleur score actuel
        if (score > bestScore) {
            bestScore = score;
            bestRuleIndex = i;
        }
    }

    return &rules[bestRuleIndex];
}*/




void *makeDecisionThread(void *arg) {
    ThreadData *data = (ThreadData *)arg;

    int bestScore = -1;
    int bestRuleIndex = -1;

    // Obtenez les données nécessaires de ThreadData
    Rule *rules = data->rules;
    Monster *monster = data->monster;
    Traveler *traveler = data->traveler;
    int diamondX = data->diamondX;
    int diamondY = data->diamondY;

    int playerDirection = direction_signal(monster, traveler->x, traveler->y);
    int diamondDirection = direction_signal(monster, diamondX, diamondY);
    int playerDistance = distance_signal(monster, traveler->x, traveler->y);
    int diamondDistance = distance_signal(monster, diamondX, diamondY);

    for (int i = 0; i < MAX_Rule; i++) {
        Rule currentRule = rules[i];
        if (currentRule.playerDirection == playerDirection && currentRule.diamondDirection == diamondDirection &&
            currentRule.playerDistance == playerDistance && currentRule.diamondDistance == diamondDistance) {
            data->result = &rules[i];
            pthread_exit(NULL);
        }

        int score = calcule_score(&currentRule, monster, traveler);

        if (score > bestScore) {
            bestScore = score;
            bestRuleIndex = i;
        }
    }

    data->result = &rules[bestRuleIndex];
    pthread_exit(NULL);
}

Rule *makeAIDecision_app(Rule *rules, Monster *monster, Traveler *traveler, int diamondX, int diamondY) {
    pthread_t thread;
    ThreadData data;
    data.rules = rules;
    data.monster = monster;
    data.traveler = traveler;
    data.diamondX = diamondX;
    data.diamondY = diamondY;

    pthread_create(&thread, NULL, makeDecisionThread, (void *)&data);
    pthread_join(thread, NULL);

    return data.result;
}




void updateAI_app(Rule * rules ,Monster * monster, Traveler * traveler , int diamondX , int diamondY ) {


    Rule * rule_choisi = makeAIDecision_app(rules,monster,traveler,diamondX,diamondY);


    switch (rule_choisi->action) {
        case 1:
            
            moveToTraveler(rule_choisi->playerDirection,monster);  
            //action = makeAIDecision_app(rule);
            break;
        case 2:
            
                moveToDiamond(rule_choisi->diamondDirection,monster);
                //action = makeAIDecision_app(rule);

            break;

        default  :  // reste immobile
              break;
         }

    
}


// Fonction d'IA pour prendre une décision en fonction des règles
/*int makeAIDecision(Rule * rules,   int playerDirection, int diamondDirection, int playerDistance, int diamondDistance) {
    int maxPriority = -1;
    int selectedAction = -1;


    // Parcours de toutes les règles pour trouver celle avec la priorité la plus élevée
    for (int i = 0; i < MAX_Rule; i++) {
        Rule rule = rules[i];

        
            if(rand()%10==3)    return rule.action;
            if(rand()%10==9)    return rule.action;
            if (rule.priority > maxPriority) {
                maxPriority = rule.priority;
                selectedAction = rule.action;
            }
        //}
    }

    return selectedAction;
}*/

// Exemple d'utilisation de la fonction d'IA dans votre jeu
/*void updateAI( Rule * rules ,Monster * monster, Traveler * traveler, int diamondX , int diamondY , int numRectanglesX , int numRectanglesY ) {
    // Récupérez les informations nécessaires pour la prise de décision
    int playerDirection = direction_signal(monster , traveler->x , traveler->y);
    int diamondDirection = direction_signal(monster , diamondX , diamondY);
    int playerDistance = distance_signal(monster , traveler->x , traveler->y);
    int diamondDistance = distance_signal(monster , diamondX , diamondY);
    

    // Appel de la fonction d'IA pour obtenir l'action à effectuer
    int action = makeAIDecision(rules, playerDirection, diamondDirection, playerDistance, diamondDistance);


    switch (action) {
        case 1:
            
            // Action : Se déplacer vers le joueur dans un rayon donné
            if (playerDistance <=5 ) {
                moveToTraveler(monster, traveler, numRectanglesX , numRectanglesY);
                //printf("hi !!!!\n");
                }
            else {
                action = makeAIDecision(rules, playerDirection, diamondDirection, playerDistance, diamondDistance);
            }
            break;
        case 2:
            // Action : Se déplacer vers le diamant dans un rayon donné
            if (diamondDistance <= 5) {
                moveToDiamond(monster, diamondX, diamondY , numRectanglesX , numRectanglesY);
            }else {
                action = makeAIDecision(rules, playerDirection, diamondDirection, playerDistance, diamondDistance);
                // Le diamant est en dehors du rayon, effectuer une autre action (par exemple, rester immobile)
            }
            break;
        case 3:
            //moveInCircle(&monsters[0]);
            break;

        default  :  // reste immobile
              break;
         }

    
}*/




/*int calcule_score(Rule * rule ,Monster * monsters , Traveler * traveler){


    int monsterX = 0;
    int monsterY = 0;
    int aa=1000;

    
    // Initialisation de SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Erreur lors de l'initialisation de SDL : %s\n", SDL_GetError());
        return -1;
        }



    int SCREEN_WIDTH = (int)(2560 *0.66);
    int  SCREEN_HEIGHT =(int)(1600* 0.66);



    int numRectanglesX = SCREEN_WIDTH / RECTANGLE_SIZE;
     int numRectanglesY = SCREEN_HEIGHT / RECTANGLE_SIZE;


    // Génération aléatoire du labyrinthe
    srand(time(NULL));
    

    SDL_Rect** rectangles = (SDL_Rect**)malloc(numRectanglesX * sizeof(SDL_Rect*));
    if (rectangles == NULL) {
        printf("Erreur lors de l'allocation de mémoire pour les rectangles.\n");
        SDL_Quit();
        return -1;
    }

    for (int i = 0; i < numRectanglesX; i++) {
        rectangles[i] = (SDL_Rect*)malloc(numRectanglesY *sizeof(SDL_Rect));
        
        if (rectangles[i] == NULL) {
            printf("Erreur lors de l'allocation de mémoire pour les rectangles.\n");
            for (int j = 0; j < i; j++) {
                free(rectangles[j]);
            }
            free(rectangles);
            SDL_Quit();
            return -1;
        }
        for (int j = 0; j < numRectanglesY; j++) {
            SDL_Rect rectangle = {i * RECTANGLE_SIZE, j * RECTANGLE_SIZE, RECTANGLE_SIZE, RECTANGLE_SIZE};
            rectangles[i][j] = rectangle;
        }
    }

    

    

    int diamondX = numRectanglesX/2;
    int diamondY = numRectanglesY/2;

    

    // Position initiale du traveler
    traveler->x = numRectanglesX/2;
    traveler->y = numRectanglesY/2;

    // Initialisation de la position des rand() % numRectanglesY
    for (int i = 0; i < MAX_MONSTERS; i++) {
        monsters[i].x = numRectanglesX/3 ;
        monsters[i].y = numRectanglesY/3;
        monsters[i].health = 19;
    }
    
    int gameRunning = 1;


    int score_joueur=0;
    int score = 0;
    
    while (gameRunning) {

        if(aa==1000){
            moveToDiamond_joueur(traveler,  diamondX , diamondY , numRectanglesX , numRectanglesY);
        

        if (monsterX == traveler->x && monsterY == traveler->y) {
                    gameOver = 1;
            }


        // Manger le diamant
        if (traveler->x == diamondX && traveler->y == diamondY) {
            score_joueur ++;
            monsters->health -= 2;
            diamondX = rand() % numRectanglesX;
            diamondY = rand() % numRectanglesY;
        }


        if (monsters->x == traveler->x && monsters->y == traveler->y) {
                    gameOver = 1;
                    score++;
                    monsters->health +=2;

            }


        

            updateAI_app(rule ,monsters,traveler, diamondX , diamondY);
        
            // Vérification si le monstre entre en collision avec le joueur
        if (monsters->x == traveler->x && monsters->y == traveler->y) {
                    gameOver = 1;
                    score++;
                    monsters->health +=2;

            }

        }
        

        if (gameOver) {
             
            
            // Réinitialiser les positions du traveler et des bombes
            traveler->x=numRectanglesX/2;
            traveler->y = numRectanglesY/2;

            diamondX = rand() % numRectanglesX;
            diamondY = rand() % numRectanglesY;


            // Réinitialiser l'état du jeu
            gameOver = 0;

           score_joueur =0;

           
        }


        aa--;
        if(aa==0){aa=1000;}


        if((monsters->health<1) || (monsters->health>20))  break;
    
    }
    

    // Nettoyage
    for (int j = 0; j < numRectanglesX; j++) {
        free(rectangles[j]);
    }
    free(rectangles);
    SDL_Quit();


    return score ;

}*/


void *calcScoreThread(void *arg) {
    th_calcul *data = (th_calcul *)arg;

    // Récupérer les données nécessaires de th_calcul
    Rule *rule = data->rule;
    Monster *monsters = data->monsters;
    Traveler *traveler = data->traveler;

    int monsterX = 0;
    int monsterY = 0;
    int aa = 1000;

    // Initialisation de SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Erreur lors de l'initialisation de SDL : %s\n", SDL_GetError());
        data->score = -1;
        return NULL;
    }

    int SCREEN_WIDTH = (int)(2560 * 0.66);
    int SCREEN_HEIGHT = (int)(1600 * 0.66);

    int numRectanglesX = SCREEN_WIDTH / RECTANGLE_SIZE;
    int numRectanglesY = SCREEN_HEIGHT / RECTANGLE_SIZE;

     int diamondX = numRectanglesX/2;
     int diamondY = numRectanglesY/2;


    // Génération aléatoire du labyrinthe
    srand(time(NULL));

    
    SDL_Rect** rectangles = (SDL_Rect**)malloc(numRectanglesX * sizeof(SDL_Rect*));
    if (rectangles == NULL) {
        printf("Erreur lors de l'allocation de mémoire pour les rectangles.\n");
        SDL_Quit();
        return NULL;
    }

    for (int i = 0; i < numRectanglesX; i++) {
        rectangles[i] = (SDL_Rect*)malloc(numRectanglesY *sizeof(SDL_Rect));
        
        if (rectangles[i] == NULL) {
            printf("Erreur lors de l'allocation de mémoire pour les rectangles.\n");
            for (int j = 0; j < i; j++) {
                free(rectangles[j]);
            }
            free(rectangles);
            SDL_Quit();
            return NULL;
        }
        for (int j = 0; j < numRectanglesY; j++) {
            SDL_Rect rectangle = {i * RECTANGLE_SIZE, j * RECTANGLE_SIZE, RECTANGLE_SIZE, RECTANGLE_SIZE};
            rectangles[i][j] = rectangle;
        }
    }

    // Position initiale du traveler
    traveler->x = numRectanglesX/2;
    traveler->y = numRectanglesY/2;

    // Initialisation de la position des rand() % numRectanglesY
    for (int i = 0; i < MAX_MONSTERS; i++) {
        monsters[i].x = numRectanglesX/3 ;
        monsters[i].y = numRectanglesY/3;
        monsters[i].health = 19;
    }
    
    int gameRunning = 1;


    int score_joueur=0;
    int score = 0;
    
    while (gameRunning) {

        if(aa==1000){
            moveToDiamond_joueur(traveler,  diamondX , diamondY , numRectanglesX , numRectanglesY);
        

        if (monsterX == traveler->x && monsterY == traveler->y) {
                    gameOver = 1;
            }


        // Manger le diamant
        if (traveler->x == diamondX && traveler->y == diamondY) {
            score_joueur ++;
            monsters->health -= 2;
            diamondX = rand() % numRectanglesX;
            diamondY = rand() % numRectanglesY;
        }


        if (monsters->x == traveler->x && monsters->y == traveler->y) {
                    gameOver = 1;
                    score++;
                    monsters->health +=2;

            }


        

            updateAI_app(rule ,monsters,traveler, diamondX , diamondY);
        
            // Vérification si le monstre entre en collision avec le joueur
        if (monsters->x == traveler->x && monsters->y == traveler->y) {
                    gameOver = 1;
                    score++;
                    monsters->health +=2;

            }

        }
        

        if (gameOver) {
             
            
            // Réinitialiser les positions du traveler et des bombes
            traveler->x=numRectanglesX/2;
            traveler->y = numRectanglesY/2;

            diamondX = rand() % numRectanglesX;
            diamondY = rand() % numRectanglesY;


            // Réinitialiser l'état du jeu
            gameOver = 0;

           score_joueur =0;

           
        }


        aa--;
        if(aa==0){aa=1000;}


        if((monsters->health<1) || (monsters->health>20))  break;
    
    }

    // Nettoyage
    for (int j = 0; j < numRectanglesX; j++) {
        free(rectangles[j]);
    }
    free(rectangles);
    SDL_Quit();

    data->score = score;

    return NULL;
}

int calcule_score(Rule *rule, Monster *monsters, Traveler *traveler) {
    pthread_t thread;
    th_calcul data;
    data.rule = rule;
    data.monsters = monsters;
    data.traveler = traveler;
    data.score = 0;

    pthread_create(&thread, NULL, calcScoreThread, (void *)&data);
    pthread_join(thread, NULL);

    return data.score;
}



int optimisat(int j ,int k, Rule  * rules, Monster * monsters , Traveler * traveler){
    int scoremax=0;
    switch(j){
        case 0  :
                for(int i=0;i<3;i++){
                rules[k].playerDirection=i;
                int score =calcule_score(rules,monsters, traveler);
                if(score>scoremax){   scoremax=score ;}
                 }
              break;
        case 1 :
            for(int i=0;i<3;i++){
            rules[k].diamondDirection=i;
            int score =calcule_score(rules,monsters, traveler);
            if(score>scoremax){   scoremax=score ;}
            }
            break;
        case 2 :
            for(int i=0;i<3;i++){
            rules[k].playerDistance=i;
            int score =calcule_score(rules,monsters, traveler);
            if(score>scoremax){   scoremax=score ;}
            }
            break;
        case 3 :
            for(int i=0;i<3;i++){
            rules[k].diamondDistance=i;
            int score =calcule_score(rules,monsters, traveler);
            if(score>scoremax){   scoremax=score ; }
            }
            break;

        case 4 :
            for(int i=0;i<5;i++){
            rules[k].priority=i;
            int score =calcule_score(rules,monsters, traveler);
            if(score>scoremax){   scoremax=score ;}
            }
            break;
        
        case 5 :
            for(int i=1;i<4;i++){
            rules[k].action=i;
            int score =calcule_score(rules,monsters, traveler);
            if(score>scoremax){   scoremax=score ;}
            }
            break;
    }

    return scoremax;
    
}


void  meilleure_score(Rule * rules, Monster * monsters, Traveler * traveler )
{
    for(int i=0; i<2;i++)
    {
        for(int j=0; j<6 ; j++)
        {
           int score = optimisat(j,i, rules,monsters, traveler);
           printf("\nla score de regle %d N_facteur %d: \t %d\n" ,i,j, score );
        }


        printf("\n\n");
    }


    //return score;
}







/*---------------------------essetiel_hier-------------------------------------------------*/







int main() {

    
    Rule rules[] = {{1,2,0,1 , 4, 2},
            {1,1,2,1, 3, 1},
            {1,1,2,0, 4, 3},
            {2,1,2,1, 2, 1},
            {4,5,2,2, 1, 2},
            {3,2,0,0, 3, 1},
            {3,6,1,1, 3, 1},
            {2,8,1,1, 3, 2},
            {7,4,1,2, 4, 1},
            {5,2,2,0, 3, 2},
            {6,5,2,2, 4, 2},
            {8,1,2,1, 2, 1} 
    };
    /*int tab[MAX_donnes] ;
    for (int i = 0; i < MAX_donnes; i++) {
        tab[i] = i ;
    }*/


    Monster * monsters = malloc(MAX_MONSTERS*sizeof(Monster));
    if(monsters==NULL){
        return 1;
    }
    
    Traveler * traveler = malloc(1*sizeof(Traveler));
    if(traveler==NULL)   return 0;
    //traveler->x=0;
    //traveler->y=0;

    //int * score_max = calloc(MAX_Rule , sizeof(int));

    meilleure_score(rules, &monsters[0], traveler);

    //for(int i=0; i<MAX_Rule; i++){printf("\nc'est la meiller si on chage le regle %d %d\n" , i , score_max[i]);}

    /*int a= calcule_score(rules[0] , monsters, traveler);
    printf("%d \n", a);*/
    //int monsterX = 0;
    //int monsterY = 0;
    
    //int aa=100;
    

   /*// Initialisation de SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Erreur lors de l'initialisation de SDL : %s\n", SDL_GetError());
        return -1;
        }


        SDL_DisplayMode screen;

    SDL_GetCurrentDisplayMode(0, &screen);
       printf("Résolution écran\n\tw : %d\n\th : %d\n",
          screen.w, screen.h);

    int SCREEN_WIDTH = screen.w * 0.66;
    int  SCREEN_HEIGHT = screen.h * 0.66;



    int numRectanglesX = SCREEN_WIDTH / RECTANGLE_SIZE;
     int numRectanglesY = SCREEN_HEIGHT / RECTANGLE_SIZE;

     
       // Création de la fenêtre 
    SDL_Window*  window = SDL_CreateWindow("JG17",
                 SDL_WINDOWPOS_CENTERED,
                 SDL_WINDOWPOS_CENTERED, screen.w * 0.66,
                 screen.h * 0.66,
                 SDL_WINDOW_OPENGL);
    if (window == NULL) {
        printf("Erreur lors de l'initialisation de window : %s\n", SDL_GetError());
        return -1;
    }

    


    
     // Initialisation de SDL_ttf
    if (TTF_Init() < 0) {
        fprintf(stderr, "Erreur lors de l'initialisation de SDL_ttf: %s\n", TTF_GetError());
        return 1;
    }
     TTF_Font* font1 = TTF_OpenFont("/usr/share/fonts/truetype/tlwg/Laksaman-BoldItalic.ttf", 64);
    if (font1 == NULL) {
        fprintf(stderr, "Erreur lors du chargement de la police de caractères: %s\n", TTF_GetError());
        return 1;
    } 

    TTF_Font* font = TTF_OpenFont("/usr/share/fonts/truetype/tlwg/Laksaman-BoldItalic.ttf", 24);
    if (font == NULL) {
        fprintf(stderr, "Erreur lors du chargement de la police de caractères: %s\n", TTF_GetError());
        return 1;
    } 


    // Création du renderer
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) {
        printf("Erreur lors de la création du renderer : %s\n", SDL_GetError());
        SDL_DestroyWindow(window);
        SDL_Quit();
        return -1;
    }

    //include les images :
    my_texture1 = load_texture_from_image("bg_jeu.png", window, renderer);
    my_texture2 = load_texture_from_image("per_1.png", window, renderer);
    my_texture3 = load_texture_from_image("dm_hg.png", window, renderer);
    my_texture4 = load_texture_from_image("m1.jpg", window, renderer);
    my_texture5 = load_texture_from_image("action_bom.png", window, renderer);

    // Génération aléatoire du labyrinthe
    srand(time(NULL));
    

    SDL_Rect** rectangles = (SDL_Rect**)malloc(numRectanglesX * sizeof(SDL_Rect*));
    //int ** graph = creation(numRectanglesX , numRectanglesY);
    if (rectangles == NULL) {
        printf("Erreur lors de l'allocation de mémoire pour les rectangles.\n");
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        return -1;
    }

    for (int i = 0; i < numRectanglesX; i++) {
        rectangles[i] = (SDL_Rect*)malloc(numRectanglesY *sizeof(SDL_Rect));
        
        if (rectangles[i] == NULL) {
            printf("Erreur lors de l'allocation de mémoire pour les rectangles.\n");
            for (int j = 0; j < i; j++) {
                free(rectangles[j]);
            }
            free(rectangles);
            SDL_DestroyRenderer(renderer);
            SDL_DestroyWindow(window);
            SDL_Quit();
            return -1;
        }
        for (int j = 0; j < numRectanglesY; j++) {
            SDL_Rect rectangle = {i * RECTANGLE_SIZE, j * RECTANGLE_SIZE, RECTANGLE_SIZE, RECTANGLE_SIZE};
            rectangles[i][j] = rectangle;
        }
    }

    

    // Génération aléatoire des rectangles du labyrinthe
    for (int i = 0; i < numRectanglesX; i++) {
        for (int j = 0; j < numRectanglesY; j++) {
            if (rand() % 10 == 0) {
                rectangles[i][j].w = 0;
                rectangles[i][j].h = 0;
            }
        }
    }

    int diamondX = numRectanglesX/2;
    int diamondY = numRectanglesY/2;

    

    // Position initiale du traveler
    int travelerX = numRectanglesX / 2 * RECTANGLE_SIZE + (RECTANGLE_SIZE - TRAVELER_SIZE) / 2;
    int travelerY = numRectanglesY / 2 * RECTANGLE_SIZE + (RECTANGLE_SIZE - TRAVELER_SIZE) / 2;

    // Initialisation de la position des rand() % numRectanglesY
    for (int i = 0; i < MAX_MONSTERS; i++) {
        monsters[i].x = numRectanglesX / 3;
        monsters[i].y = numRectanglesY / 3;
        monsters[i].health = 10;
    }
    int travelerIndexX = travelerX / RECTANGLE_SIZE;
    int travelerIndexY = travelerY / RECTANGLE_SIZE;




    int * score = meilleure_score(rules,rectangles ,&monsters[0], numRectanglesX , numRectanglesY);

    for(int i=0; i<MAX_Rule; i++){printf("\nc'est la meiller si on chage le regle %d %d\n" , i , score[i]);}


    // Boucle principale du jeu
    int gameRunning = 1;
    SDL_Event event;
    int stepCount = 0;


    
    while (gameRunning) {
        // Gestion des événements
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                gameRunning = 0;
            }
        }

       

        // Déplacement du traveler
        //int rand1 = rand();
        if(1){const Uint8 *keyboardState = SDL_GetKeyboardState(NULL);
        if (keyboardState[SDL_SCANCODE_LEFT] && stepCount >= STEP_THRESHOLD && travelerX - RECTANGLE_SIZE >= 0) {
            int targetX = travelerX - RECTANGLE_SIZE;
            int targetIndexX = targetX / RECTANGLE_SIZE;
            int targetIndexY = travelerY / RECTANGLE_SIZE;
            if (targetIndexX >= 0 && rectangles[targetIndexX][targetIndexY].w > 0) {
                travelerX = targetX;
                SDL_Rect travelerRect = { travelerX, travelerY, TRAVELER_SIZE, TRAVELER_SIZE };
                play_with_t_4g(my_texture2, window,renderer,travelerRect,8,4,1,1,0);
                stepCount = 0;
            }
        }
        if (keyboardState[SDL_SCANCODE_RIGHT] && stepCount >= STEP_THRESHOLD && travelerX + RECTANGLE_SIZE + TRAVELER_SIZE <= SCREEN_WIDTH) {
            int targetX = travelerX + RECTANGLE_SIZE;
            int targetIndexX = targetX / RECTANGLE_SIZE;
            int targetIndexY = travelerY / RECTANGLE_SIZE;
            if (targetIndexX < numRectanglesX && rectangles[targetIndexX][targetIndexY].w > 0) {
                travelerX = targetX;
                SDL_Rect travelerRect = { travelerX, travelerY, TRAVELER_SIZE, TRAVELER_SIZE };
                play_with_t_4g(my_texture2, window,renderer,travelerRect,8,4,1,2,0);
                stepCount = 0;
            }
        }
        if (keyboardState[SDL_SCANCODE_UP] && stepCount >= STEP_THRESHOLD && travelerY - RECTANGLE_SIZE >= 0) {
            int targetY = travelerY - RECTANGLE_SIZE;
            int targetIndexX = travelerX / RECTANGLE_SIZE;
            int targetIndexY = targetY / RECTANGLE_SIZE;
            if (targetIndexY >= 0 && rectangles[targetIndexX][targetIndexY].h > 0) {
                travelerY = targetY;
                SDL_Rect travelerRect = { travelerX, travelerY, TRAVELER_SIZE, TRAVELER_SIZE };
                play_with_t_4g(my_texture2, window,renderer,travelerRect,8,4,1,3,0);
                stepCount = 0;
            }
        }
        if (keyboardState[SDL_SCANCODE_DOWN] && stepCount >= STEP_THRESHOLD && travelerY + RECTANGLE_SIZE + TRAVELER_SIZE <= SCREEN_HEIGHT) {
            int targetY = travelerY + RECTANGLE_SIZE;
            int targetIndexX = travelerX / RECTANGLE_SIZE;
            int targetIndexY = targetY / RECTANGLE_SIZE;
            if (targetIndexY < numRectanglesY && rectangles[targetIndexX][targetIndexY].h > 0) {
                travelerY = targetY;
                SDL_Rect travelerRect = { travelerX, travelerY, TRAVELER_SIZE, TRAVELER_SIZE };
                play_with_t_4g(my_texture2, window,renderer,travelerRect,8,4,1,0,0);
                stepCount = 0;
            }
        }}


        if (monsterX == travelerIndexX && monsterY == travelerIndexY) {
                    gameOver = 1;
            }


        // Manger le diamant
        travelerIndexX = travelerX / RECTANGLE_SIZE;
        travelerIndexY = travelerY / RECTANGLE_SIZE;
        if (travelerIndexX == diamondX && travelerIndexY == diamondY) {
            score ++;
            diamondX = rand() % numRectanglesX;
            diamondY = rand() % numRectanglesY;
            do {
            // Étape a : Génération de coordonnées aléatoires
            diamondX = rand() % numRectanglesX;
            diamondY = rand() % numRectanglesY;

            // Étape b : Vérification si la position est un mur ou non
            if (rectangles[ diamondX][diamondY].h > 0) {
                // La position est un mur, générez une nouvelle position
                break;
            }

            // Si la position est invalide, sortir de la boucle
    
            } while (1);
        }
       
        if(aa==100){   
            updateAI(rectangles, rules ,&monsters[0],travelerIndexX ,travelerIndexY ,diamondX ,diamondY, numRectanglesX, numRectanglesY);}

            // Vérification si le monstre entre en collision avec le joueur
        if (monster->x == travelerIndexX && monster->y == travelerIndexY) {
                    gameOver = 1;
                    score++;
                    monster->health +=1;

            }

         

        

        if (gameOver) {
             
            
            // Réinitialiser les positions du traveler et des bombes
            travelerX = numRectanglesX / 2 * RECTANGLE_SIZE + (RECTANGLE_SIZE - TRAVELER_SIZE) / 2;
            travelerY = numRectanglesY / 2 * RECTANGLE_SIZE + (RECTANGLE_SIZE - TRAVELER_SIZE) / 2;
           

             do {
            // Étape a : Génération de coordonnées aléatoires
            diamondX = rand() % numRectanglesX;
            diamondY = rand() % numRectanglesY;

            // Étape b : Vérification si la position est un mur ou non
            if (rectangles[ diamondX][diamondY].h > 0) {
                break;
            }

            // Si la position est invalide, sortir de la boucle
    
            } while (1);

            // Réinitialiser l'état du jeu
            gameOver = 0;


             // Afficher le message Game Over avec le score
              char message[100];
           snprintf(message, sizeof(message), "Game Over. Votre score est %d\n", score);
           SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, "Game Over", message,NULL);
           score =0;
        }



        // Incrémenter le compteur de pas
        stepCount++;

        // Effacer l'écran
        SDL_SetRenderDrawColor(renderer, 64, 49, 45, 255);
        SDL_RenderClear(renderer);

        // Dessiner le labyrinthe
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
        for (int i = 0; i < numRectanglesX; i++) {
            for (int j = 0; j < numRectanglesY; j++) {
                
                play_with_texture_1(my_texture1,renderer, rectangles[i][j],0);
            }
        }

         // Dessiner le diamant
        if (diamondX != -1 && diamondY != -1) {
            SDL_Rect diamondRect = {diamondX * RECTANGLE_SIZE + (RECTANGLE_SIZE - DIAMOND_SIZE) / 2, diamondY * RECTANGLE_SIZE + (RECTANGLE_SIZE - DIAMOND_SIZE) / 2, DIAMOND_SIZE, DIAMOND_SIZE};
            
            play_with_texture_1(my_texture3, renderer, diamondRect,0);

        }

 

        // Dessiner le traveler
        SDL_Rect travelerRect = { travelerX, travelerY, TRAVELER_SIZE, TRAVELER_SIZE };
        play_with_texture_1(my_texture2, renderer,travelerRect,1);

        // Affichage des mostres
        for (int i = 0; i < MAX_MONSTERS; i++) {
            if (monsters[i].isAlive) {
                SDL_Rect monsterRect = {monsters[i].x *RECTANGLE_SIZE, monsters[i].y*RECTANGLE_SIZE , RECTANGLE_SIZE, RECTANGLE_SIZE};
                SDL_RenderCopy(renderer, my_texture4, NULL, &monsterRect);
            }
        }

        // Affichage du score
        char scoreText[10];
        snprintf(scoreText, sizeof(scoreText), "Score: %d", score);
        SDL_Color textColor ;
        textColor.a=0;
        textColor.b=255;
        textColor.g=255;
        SDL_Surface* textSurface = TTF_RenderText_Solid(font, scoreText, textColor);
        if (textSurface == NULL) {
            fprintf(stderr, "Erreur lors du rendu du texte: %s\n", TTF_GetError());
            return 1;
        }
        SDL_Texture* textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
        if (textTexture == NULL) {
            fprintf(stderr, "Erreur lors de la création de la texture du texte: %s\n", SDL_GetError());
            return 1;
        }
        SDL_Rect textRect = { 10, 10, textSurface->w, textSurface->h };
        SDL_RenderCopy(renderer, textTexture, NULL, &textRect);
        SDL_FreeSurface(textSurface);
        SDL_DestroyTexture(textTexture);

        SDL_RenderPresent(renderer);

        aa--;
        if(aa==0){aa=1000;}
    }
    

    // Nettoyage
    for (int j = 0; j < numRectanglesX; j++) {
        free(rectangles[j]);
    }
    free(rectangles);
    //free(bombs);
    
    SDL_DestroyTexture(my_texture1);
    SDL_DestroyTexture(my_texture2);
    SDL_DestroyTexture(my_texture3);
    SDL_DestroyTexture(my_texture4);
    SDL_DestroyTexture(my_texture5);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();*/

    free(monsters);
    free(traveler);

    return 0;
}
