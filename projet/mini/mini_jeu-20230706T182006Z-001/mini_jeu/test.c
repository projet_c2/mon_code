#include <stdio.h>
/*
#define N 4
#define M 8 

int ** crea(int n, int p){
    int ** mat = malloc(n*sizeof(int *));
    if(mat){
        for(int i=0 ; i<n; i++){
        mat[i]= malloc(p*sizeof(int));
        if(mat[i]){
            for(int j=0 ; j<p ; j++){
                mat[i][j]=0;
                
            }
        }
        }
    }

    return mat
}

void liberer(int ** mat , int n ){
    for(int i=0 ; i<n; i++){
        free(mat[i]);
    }
    free(mat);
}

void afficher(int ** mat , int n , int p){
    for(int i=0 ; i<n; i++){
        printf("\n");
        for(int j=0 ; j<p ; j++){
                printf("%d " , mat[i][j]);
                
            }
    }
}



void mise_a_jour_regle(int** mat, Monstre monstre, int travelerIndexX ,int  travelerIndexY, int diamondX ,  int diamondY) {
    mat[0][0] = direction_signal(monsterX, monsterY, travelerIndexX , travelerIndexY);
    mat[0][1] = distance_signal(monsterX, monsterY, travelerX , travelerY);
    mat[0][2] = is_wall(monsterX , monsterY , mat[0][0]);

    mat[1][0] = direction_signal(monsterX, monsterY, diamondX , diamondY);
    mat[1][1] = distance_signal(monsterX, monsterY, diamondX , diamondY);
    mat[1][2] = is_wall(monsterX , monsterY , mat[1][0]);
}



int direction_signal(int monsterX, int monsterY, int travelerIndexX , int travelerIndexY){
    int dx = travelerIndexX - monsterX;
    int dy = travelerIndexY - monsterY;

    if(0==dx || 0==dy){
        if(0==dx){
            if(dy>0)   return 1;
            else       return 2;
        }
        else{
            if(dx>0)    return 4;
            else        return 3;
        }
    }
    
    if(dx>0 && dy>0){
        return 5;
    }
    if(dx>0 && dy<0){
        return 6;
    }
    if(dx<0 && dy>0){
        return 7;
    }
    if(dx<0 && dy<0){
        return 8;
    }
}


int distance_signal(int monsterX, int monsterY, int travelerX, int travelerY) {
    int dx = abs(travelerX - monsterX);
    int dy = abs(travelerY - monsterY);

    if (dx + dy < 80) {
        return 0;  // Distance inférieure à 80
    } else if (dx + dy >= 80 && dx + dy <= 150) {
        return 1;  // Distance entre 80 et 150
    } else {
        return -1;  // Distance supérieure à 150
    }
}



int is_wall(int monsterX, int monsterY, int direction_signal) {
    // Vérifier si la case aux coordonnées (monsterX, monsterY) est un mur
    // en tenant compte de la direction signalée
    
    // Implémentez votre logique de vérification ici en utilisant direction_signal
    
    // Exemple de logique de vérification (à adapter à votre code) :
    if (direction_signal == 1) {
        // Vérification si la case au-dessus est un mur
        if (0 == rectangles[monsterX][monsterY - 1].h) {
            return 1;  // La case est un mur
        }
    } else if (direction_signal == 2) {
        // Vérification si la case en-dessous est un mur
        if (0 == rectangles[monsterX][monsterY + 1].h) {
            return 1;  // La case est un mur
        }
    } else if (direction_signal == 3) {
        // Vérification si la case à gauche est un mur
        if (0 == rectangles[monsterX - 1][monsterY].h) {
            return 1;  // La case est un mur
        }
    } else if (direction_signal == 4) {
        // Vérification si la case à droite est un mur
        if (0 == rectangles[monsterX + 1][monsterY].h) {
            return 1;  // La case est un mur
        }
    } else if (direction_signal == 5) {
        // Vérification si la case en diagonale supérieure droite est un mur
        if (0 == rectangles[monsterX + 1][monsterY - 1].h) {
            return 1;  // La case est un mur
        }
    } else if (direction_signal == 6) {
        // Vérification si la case en diagonale inférieure droite est un mur
        if (0 == rectangles[monsterX + 1][monsterY + 1].h) {
            return 1;  // La case est un mur
        }
    } else if (direction_signal == 7) {
        // Vérification si la case en diagonale supérieure gauche est un mur
        if (0 == rectangles[monsterX - 1][monsterY - 1].h) {
            return 1;  // La case est un mur
        }
    } else if (direction_signal == 8) {
        // Vérification si la case en diagonale inférieure gauche est un mur
        if (0 == rectangles[monsterX - 1][monsterY + 1].h) {
            return 1;  // La case est un mur
        }
    }
    
    return 0;  // La case n'est pas un mur
}




void decision(int ** regle , int * proirité){
    int pd =0, pj = 0;
    if((1 == regle[0][2])){
        pd = 1;
    }else{
            switch(regle[0][1]){
                case 1:
                     pd = 4;
                     break;
                case 2 :
                     pd = 2;
                     break;
                default  :
                      pd = 1;
                      break;
        }
    }

    if((1 == regle[1][2])){
        pj = 0;
    }else{
            switch(regle[1][1]){
                case 1:
                     pj = 5;
                     break;
                case 2 :
                     pj = 2;
                     break;
                default  :
                      pj = 0;
                      break;
        }
    }

    (pj>pd)? proirité[0]=pj,proirité[1]=regle[0][0] : proirité[0]=pd,proirité[1]=regle[1][0];


}


// Définition des constantes
#define GRID_SIZE 10
#define NUM_ACTIONS 4
#define NUM_STATES (GRID_SIZE * GRID_SIZE)

// Structure pour représenter le cerveau
typedef struct {
    double Q[NUM_STATES][NUM_ACTIONS];
} Brain;

// Fonction pour initialiser le cerveau avec des valeurs aléatoires
void initializeBrain(Brain* brain) {
    for (int state = 0; state < NUM_STATES; state++) {
        for (int action = 0; action < NUM_ACTIONS; action++) {
            brain->Q[state][action] = (double)rand() / RAND_MAX;
        }
    }
}

// Fonction pour choisir une action à partir de l'état actuel du jeu
int chooseAction(Brain* brain, int state) {
    // Exploration-exploitation : choisir une action aléatoire avec une probabilité epsilon,
    // sinon choisir l'action avec la plus grande valeur Q pour l'état donné
    double epsilon = 0.1;
    if ((double)rand() / RAND_MAX < epsilon) {
        return rand() % NUM_ACTIONS;
    } else {
        int maxAction = 0;
        for (int action = 1; action < NUM_ACTIONS; action++) {
            if (brain->Q[state][action] > brain->Q[state][maxAction]) {
                maxAction = action;
            }
        }
        return maxAction;
    }
}

// Fonction pour mettre à jour les valeurs Q du cerveau après une action
void updateQValue(Brain* brain, int state, int action, int nextState, double reward, double learningRate, double discountFactor) {
    // Mettre à jour la valeur Q pour l'action choisie
    double maxNextQ = 0.0;
    for (int nextAction = 0; nextAction < NUM_ACTIONS; nextAction++) {
        if (brain->Q[nextState][nextAction] > maxNextQ) {
            maxNextQ = brain->Q[nextState][nextAction];
        }
    }
    brain->Q[state][action] += learningRate * (reward + discountFactor * maxNextQ - brain->Q[state][action]);
}

// Fonction principale pour l'apprentissage par renforcement
void reinforcementLearning() {
    // Création du cerveau
    Brain brain;
    initializeBrain(&brain);
    
    // Boucle d'apprentissage
    int numEpisodes = 1000;
    double learningRate = 0.1;
    double discountFactor = 0.9;
    
    for (int episode = 0; episode < numEpisodes; episode++) {
        // Initialisation de l'état du jeu
        
        // Boucle du jeu
        
            // Choix de l'action à effectuer
            
            // Mise à jour de l'état du jeu
            
            // Calcul de la récompense
            
            // Mise à jour des valeurs Q du cerveau
            
    }
    
    // Évaluation du cerveau entraîné
    
    // Affichage des résultats
    // les regles de mon jeu sont suivie le principe suivant :
        //  une regle est un triplet de direction de joueur et distance de jouer et la presence de mur et distance diamant et direction de diamant 
        // mon monstre se deplace vers le joueur ou la diamant en regardant la distance entre eux et si rencontre un mur il fait ce quil veux et
        // si la distance est loin ou tres loin il fait ce quil veut
    
}
*/
int main() {
    // Appel de la fonction d'apprentissage par renforcement
    //reinforcementLearning();

     int tab[12*7] ;
    for (int i = 0; i < 12*7; i++) {
        tab[i] = i ;
        printf("%d ", tab[i]);
    }
    
    return 0;
}



/*
// Structure pour représenter une règle
typedef struct {
    int joueur_direction;
    int joueur_distance;
    int mur_present;
    int diamant_distance;
    int diamant_direction;
    int priorite;
} Regle;

// Fonction pour choisir l'action du monstre en fonction des règles
int choisirActionMonstre(Regle* regles, int num_regles, int joueur_distance, int mur_present, int diamant_distance) {
    int action = -1; // Action par défaut si aucune règle ne correspond
    
    // Parcourir les règles pour trouver la meilleure action en fonction des conditions
    int max_priorite = -1;
    for (int i = 0; i < num_regles; i++) {
        Regle regle = regles[i];
        if (joueur_distance <= regle.joueur_distance && mur_present == regle.mur_present &&
            diamant_distance <= regle.diamant_distance) {
            if (regle.priorite > max_priorite) {
                max_priorite = regle.priorite;
                action = regle.joueur_direction; // Choisir l'action du monstre en fonction de la direction du joueur dans la règle
            }
        }
    }
    
    return action;
}

// Fonction principale pour le déplacement du monstre en fonction des règles
void deplacementMonstre() {
    // Définition des règles
    Regle regles[] = {
        {1, 80, 0, 80, 7, 6},
        {1, 80, 1, 80, 3, 2},
        {1, 150, 0, 150, 7, 4},
        {1, 150, 1, 150, 3, 0},
        {2, 80, 0, 80, 7, 6},
        {2, 80, 1, 80, 3, 2},
        {2, 150, 0, 150, 7, 4},
        {2, 150, 1, 150, 3, 0},
        // Ajouter d'autres règles selon votre besoin
    };
    int num_regles = sizeof(regles) / sizeof(Regle);
    
    // Simulation du jeu
    int joueur_distance = 100; // Distance du joueur au monstre (à adapter selon votre jeu)
    int mur_present = 0; // Indicateur de présence de mur (0: pas de mur, 1: mur présent)
    int diamant_distance = 120; // Distance du diamant au monstre (à adapter selon votre jeu)
    
    // Choix de l'action du monstre en fonction des règles
    int action = choisirActionMonstre(regles, num_regles, joueur_distance, mur_present, diamant_distance);
    
    // Affichage de l'action choisie
    printf("Action du monstre : %d\n", action);
}

int main() {
    // Appel de la fonction de déplacement du monstre
    deplacementMonstre();
    
    return 0;
}

*/




/*----------------------------------------------------------------------------------------------------------*/
/*
// Définition des règles
Rule rules[] = {
    {0, 1, 2, 3, 1, 2, 3},  // Règle 1
    {1, 0, 3, 2, 1, 3, 2},  // Règle 2
    // Ajoutez d'autres règles ici
};

// Fonction d'IA pour prendre une décision en fonction des règles
int makeAIDecision(int playerDirection, int diamondDirection, int playerDistance, int diamondDistance, int nextCell) {
    int maxPriority = -1;
    int selectedAction = -1;

    // Parcours de toutes les règles pour trouver celle avec la priorité la plus élevée
    for (int i = 0; i < sizeof(rules) / sizeof(rules[0]); i++) {
        Rule rule = rules[i];

        // Vérification des conditions de la règle
        if (rule.playerDirection == playerDirection && rule.diamondDirection == diamondDirection &&
            rule.playerDistance == playerDistance && rule.diamondDistance == diamondDistance &&
            rule.nextCell == nextCell) {
            if (rule.priority > maxPriority) {
                maxPriority = rule.priority;
                selectedAction = rule.action;
            }
        }
    }

    return selectedAction;
}

// Exemple d'utilisation de la fonction d'IA dans votre jeu
void updateAI() {
    // Récupérez les informations nécessaires pour la prise de décision
    int playerDirection = getPlayerDirection();
    int diamondDirection = getDiamondDirection();
    int playerDistance = getPlayerDistance();
    int diamondDistance = getDiamondDistance();
    int nextCell = getNextCell();

    // Appel de la fonction d'IA pour obtenir l'action à effectuer
    int action = makeAIDecision(playerDirection, diamondDirection, playerDistance, diamondDistance, nextCell);

    // Effectuer l'action dans le jeu
    performAction(action);
}


void performAction(int action, int playerDirection, int diamondDirection, int playerDistance, int diamondDistance, int nextCaseNature) {
    int maxMovementRadius = 5; // Rayon de déplacement maximum
    
    switch (action) {
        case 1:
            // Action : Se déplacer vers le joueur dans un rayon donné
            if (nextCaseNature == 0 && playerDistance <= maxMovementRadius) {
                moveToTraveler(&monsters[0], travelerIndexX, travelerIndexY);
            } else {

            }
            break;
        case 2:
            // Action : Se déplacer vers le diamant dans un rayon donné
            if (nextCaseNature == 0 && diamondDistance <= maxMovementRadius) {
                moveToDiamond(&monsters[0], diamondX, diamondY);
            } else {
                // Le diamant est en dehors du rayon, effectuer une autre action (par exemple, rester immobile)
            }
            break;
        case 3:
            moveInCircle(&monsters[0]);
            break;

        default  :  // reste immobile
              break;
   // }
}
*/
/*-------------------------------------------------------------------------------------------*/


// Mise à jour de la position des 
        /*if (aa==100){for (int i = 0; i < MAX_MONSTERS; i++) {
            if (monsters[i].isAlive) {
                monsterX = monsters[i].x;
                monsterY = monsters[i].y;

                // Déplacement aléatoire des 
                int randomDirection = rand() % 4;
                //a_rand=3;
                switch (randomDirection) {
                    case 0:  // Haut
                        if ((monsterY> 0)&& (rectangles[monsterX][monsterY-1].h > 0) ){
                            monsterY--;
                        }
                        break;
                    case 1:  // Bas
                        if ((monsterY < numRectanglesY) && (rectangles[monsterX][monsterY+1].h > 0)) {
                            monsterY++;
                        }
                        break;
                    case 2:  // Gauche
                        if ((monsterX > 0) && (rectangles[monsterX-1][monsterY].w > 0)) {
                            monsterX--;
                        }
                        break;
                    case 3:  // Droite
                        if ((monsterX < numRectanglesX)  && (rectangles[monsterX+1][monsterY].w > 0)) {
                            monsterX++;
                        }
                        break;
                }

                monsters[i].x = monsterX;
                monsters[i].y = monsterY;
            }
        }}*/

// Vérification si le monstre entre en collision avec le joueur
        /*if (monsters[0].x == travelerIndexX && monsters[0].y == travelerIndexY) {
                    gameOver = 1;
                    break;
        }*/
      
        // Manger un bomb
       /* for (int i = 0; i < NUM_BOMBS; i++) {
          if (travelerIndexX == bombs[i].x && travelerIndexY == bombs[i].y) {
             gameOver = 1;
             SDL_Rect travelerRect = { travelerX, travelerY, TRAVELER_SIZE, TRAVELER_SIZE };
            play_with_texture_4(my_texture5,window,renderer,travelerRect,6,6,31);
             break;  // Sortir de la boucle dès qu'une bombe est mangé
           }
        }*/
/*for (int i = 0; i < NUM_BOMBS; i++) {
                bombs[i].x = rand() % numRectanglesX;
                bombs[i].y = rand() % numRectanglesY;
                bombs[i].timer = 30000;  // Réinitialiser le timer à 3 seint travelerIndexX = travelerX / RECTANGLE_SIZE;
        int travelerIndexY = travelerY / RECTANGLE_SIZE;condes
            }*/

        // Dessiner le bomb
/*
        for (int i = 0; i < NUM_BOMBS; i++) {
            SDL_Rect bombRect = { bombs[i].x * RECTANGLE_SIZE + (RECTANGLE_SIZE - DIAMOND_SIZE) / 2, bombs[i].y * RECTANGLE_SIZE + (RECTANGLE_SIZE - DIAMOND_SIZE) / 2, DIAMOND_SIZE, DIAMOND_SIZE };
            play_with_texture_1(my_texture4, renderer, bombRect,0);

        }
*/

 //monster = monsters[0];

        /*if (aa==200){
        for(int i=0 ; i<MAX_MONSTERS ; i++){
        int distanceToTravelerX = abs(monsters[i].x - travelerIndexX);
        int distanceToTravelerY = abs(monsters[i].y - travelerIndexY);
        int distanceToDiamondX = abs(monsters[i].x - diamondX);
        int distanceToDiamondY = abs(monsters[i].y - diamondY);

        if (distanceToTravelerX <= 4 && distanceToTravelerY <= 4) {
            moveToTraveler(&monsters[i], travelerIndexX, travelerIndexY);
        } else if (distanceToDiamondX <= 2 && distanceToDiamondY <= 2) {
            moveToDiamond(&monsters[i], diamondX, diamondY);
        } else if (distanceToTravelerX <= 7 && distanceToTravelerY <= 7) {
            moveToTraveler(&monsters[i], travelerIndexX, travelerIndexY);
        } else if (distanceToDiamondX <= 3 && distanceToDiamondY <= 3) {
            moveToDiamond(&monsters[i], diamondX, diamondY);
        } else {
            // Règle 9 : Si la distance est loin ou très loin, le monstre fait ce qu'il veut
            int randomDirection = rand() % 4;
            switch (randomDirection) {
                case 0:
                    monsters[i].x++;
                    break;
                case 1:
                    monsters[i].x--;
                    break;
                case 2:
                    monsters[i].y++;
                    break;
                case 3:
                    monsters[i].y--;
                    break;
            }
        }}}*/
        /*if (monsters[0].x == travelerIndexX && monsters[0].y == travelerIndexY) {
                    gameOver = 1;
                    break;
        }*/


         

         // Mise à jour de la position des 
        /*if (aa==100){for (int i = 0; i < MAX_MONSTERS; i++) {
            if (monsters[i].isAlive) {
                int monsterX = monsters[i].x;
                int monsterY = monsters[i].y;

                // Déplacement aléatoire des 
                int randomDirection = rand() % 4;
                //a_rand=3;
                switch (randomDirection) {
                    case 0:  // Haut
                        if ((monsterY> 0)&& (rectangles[monsterX][monsterY-1].h > 0) ){
                            monsterY--;
                        }
                        break;
                    case 1:  // Bas
                        if ((monsterY < numRectanglesY) && (rectangles[monsterX][monsterY+1].h > 0)) {
                            monsterY++;
                        }
                        break;
                    case 2:  // Gauche
                        if ((monsterX > 0) && (rectangles[monsterX-1][monsterY].w > 0)) {
                            monsterX--;
                        }
                        break;
                    case 3:  // Droite
                        if ((monsterX < numRectanglesX)  && (rectangles[monsterX+1][monsterY].w > 0)) {
                            monsterX++;
                        }
                        break;
                }

                // Vérification si le monstre entre en collision avec le joueur
                if (monsterX == travelerIndexX && monsterY == travelerIndexY) {
                    gameOver = 1;
                    break;
                }

                monsters[i].x = monsterX;
                monsters[i].y = monsterY;
            }
        }}*/

        // Gestion des bombes
       /*for (int i = 0; i < NUM_BOMBS; i++) {
         bombs[i].timer -= 16;  // Soustraire le temps écoulé depuis la dernière itération (16 ms)
           if (bombs[i].timer <= 0) {
                bombs[i].x = rand() % numRectanglesX;
                bombs[i].y = rand() % numRectanglesY;
                bombs[i].timer = 30000;  // Réinitialiser le timer à 3 secondes
           }
            while(rectangles[ bombs[i].x][bombs[i].y].h == 0){
               
                bombs[i].x = rand() % numRectanglesX;
                bombs[i].y  = rand() % numRectanglesY;
            }
        }*/

/*else{
                    if(0==a){
                        printf("hello ..\n");
                        // Gestion des diamants
                        diamondX =rand() % numRectanglesX;
                        diamondY =  rand() % numRectanglesY;
                        a=1;
                    }
                }*/

// Vérification des conditions de la règle
        /*if (rule.playerDirection == playerDirection && rule.diamondDirection == diamondDirection &&
            rule.playerDistance == playerDistance && rule.diamondDistance == diamondDistance &&
            rule.nextCase == nextCell) {*/

/*
typedef struct {
    int positionX;
    int positionY;
    int health;
} Traveler;
*/
// Structure représentant le monstre
/*typedef struct {
    int positionX;
    int positionY;
    int health;
} Monster;*/



// Fonction pour attaquer le voyageur
/*void attackTraveler(Monster* monster, Traveler* traveler) {
    // Implémentez ici la logique pour l'attaque du voyageur par le monstre
    // Par exemple, vous pouvez réduire les points de vie du voyageur et du monstre
    int damage = rand() % 10 + 1; // Dommages aléatoires entre 1 et 10
    traveler->health -= damage;
    monster->health -= damage;
    printf("Le monstre attaque le voyageur et lui inflige %d points de dommages !\n", damage);
}*/



/*
#define MAX_ACTIONS 4
#define MAX_STATES 100

typedef struct {
    int state;
    int action;
} StateActionPair;

typedef struct {
    double qValues[MAX_STATES][MAX_ACTIONS];
} Brain;

void initializeBrain(Brain* brain) {
    srand(time(NULL));
    for (int i = 0; i < MAX_STATES; i++) {
        for (int j = 0; j < MAX_ACTIONS; j++) {
            brain->qValues[i][j] = (double)rand() / RAND_MAX;
        }
    }
}

int chooseAction(Brain* brain, int state) {
    double bestQValue = -1;
    int bestAction = 0;
    for (int i = 0; i < MAX_ACTIONS; i++) {
        if (brain->qValues[state][i] > bestQValue) {
            bestQValue = brain->qValues[state][i];
            bestAction = i;
        }
    }
    return bestAction;
}

void updateQValues(Brain* brain, StateActionPair stateActionPair, double reward, StateActionPair nextStateActionPair, double learningRate, double discountFactor) {
    int currentState = stateActionPair.state;
    int currentAction = stateActionPair.action;
    int nextState = nextStateActionPair.state;
    double maxQValue = brain->qValues[nextState][chooseAction(brain, nextState)];
    double qValue = brain->qValues[currentState][currentAction];
    qValue = qValue + learningRate * (reward + discountFactor * maxQValue - qValue);
    brain->qValues[currentState][currentAction] = qValue;
}

void deplacementMonstre(Brain* brain) {
    int currentState = 0;
    int currentAction = chooseAction(brain, currentState);
    
    // Effectuez l'action du monstre et observez les récompenses
    
    int nextState = 1;
    StateActionPair stateActionPair = {currentState, currentAction};
    StateActionPair nextStateActionPair = {nextState, chooseAction(brain, nextState)};
    
    double reward =2.0  Calcul de la récompense obtenue ;
    
    double learningRate = 0.1 Taux d'apprentissage ;
    double discountFactor = 1.5 Facteur d'escompte ;
    
    updateQValues(brain, stateActionPair, reward, nextStateActionPair, learningRate, discountFactor);
}*/


Bomb * bombs = (Bomb *)malloc(NUM_BOMBS * sizeof(Bomb));
    for(int i=0 ; i<NUM_BOMBS;i++){
        bombs[i].x = -1;
        bombs[i].y = -1;
        bombs[i].timer = 30000;
    }

if(b<NUM_BOMBS-1){
        for(int j=b ; j<NUM_BOMBS; j++){
            bombs[j].x = rand() % numRectanglesX;
            bombs[j].y = rand() % numRectanglesY;
            bombs[j].timer = 30000;
        }
    }



    if(j==0 || j==1){
        if(j==0){
        }
        else{
            for(i=0;i<4;i++){
            rule.diamondDirection=i;
            int score = calcule_sore(rule);
            if(score>scoremax){   scoremax=score ; valeur=i;}
        }
        }
    }
    else if(j==2 || j==3){
        if(j==2){
        for(i=0;i<3;i++){
            regle[j]=i;
            int score = calcule_score(regle , thread);
            if(score>scoremax){  scoremax=score ; valeur=i;}
        }}
    }
    else if(j==5 || j==6){
        if(j==5){
            for(int i=0; i<4;i++){
            regle[j]=i;
            int score = calcule_sore(regle, thread);
            if(score>scoremax){   scoremax=score ; valeur=i;}
            }
        }
        else {
            for(int i=0; i<3;i++){
            regle[j]=i;
            int score = calcule_sore(regle, thread);
            if(score>scoremax){   scoremax=score ; valeur=i;}
            }
        }
    }






#include <stdio.h>
#include <math.h>

typedef struct {
    double x;
    double y;
} Point;

void moveInCircle(Monster * object) {
    int a=rand();
    object->x += (int)(1 * cos(a%3 *(M_PI/2)));
    object->y += (int)(1 * sin(a%3 *(M_PI/2)));
}

int main() {
    double centerX = 0.0;
    double centerY = 0.0;
    double radius = 5.0;
    double angle = 0.0;
    double angleIncrement = 0.1;

    Point object;

    for (int i = 0; i < 10; i++) {
        moveInCircle(&object, centerX, centerY, radius, angle);

        printf("Position : (%.2f, %.2f)\n", object.x, object.y);

        angle += angleIncrement;
    }

    return 0;
}


/*----------------------------------------------------------------------------------------------*/

/*--------------------------------essentiel_hier------------------------------------------*/











melange(tab);

int * meilleure_score(Rule rule){
    int * score_max = calloc(MAX_Rule , sizeof(int));
    for(int i=0; i<MAX_donnes;i++){
        int score = optimisat(i%6, regle[i/6]);
        score_max[i]= score;
    }


    return score_max;
}

int optimisat(int j , Rule rule){
    int scoremax=0;
    switch(j){
        case 0  :
                for(int i=0;i<4;i++){
            rule.playerDirection=i;
            int score = calcule_sore(rule);
            if(score>scoremax){   scoremax=score ; valeur=i;}
              }
              break;
        case 1 :
            for(int i=0;i<4;i++){
            rule.diamondDirection=i;
            int score = calcule_sore(rule);
            if(score>scoremax){   scoremax=score ; valeur=i;}
            }
            break;
        case 2 :
            for(int i=0;i<3;i++){
            rule.playerDistance=i;
            int score = calcule_sore(rule);
            if(score>scoremax){   scoremax=score ; valeur=i;}
            }
            break;
        case 3 :
            for(int i=0;i<3;i++){
            rule.diamondDistance=i;
            int score = calcule_sore(rule);
            if(score>scoremax){   scoremax=score ; valeur=i;}
            }
            break;

        case 4 :
            for(int i=0;i<5;i++){
            rule.priority=i;
            int score = calcule_sore(rule);
            if(score>scoremax){   scoremax=score ; valeur=i;}
            }
            break;
        
        case 5 :
            for(int i=1;i<4;i++){
            rule.action=i;
            int score = calcule_sore(rule);
            if(score>scoremax){   scoremax=score ; valeur=i;}
            }
            break;
    }
    
}





1 : 0...6
2 : 7....13
3 : 14....20
4: 21......27

























int calcule_score(int * regle , SDL_Rect ** rectangles ,Monster * monster, int numRectanglesX , int numRectanglesY){
    int travelerX = numRectanglesX / 2 * RECTANGLE_SIZE + (RECTANGLE_SIZE - TRAVELER_SIZE) / 2;
    int travelerY = numRectanglesY / 2 * RECTANGLE_SIZE + (RECTANGLE_SIZE - TRAVELER_SIZE) / 2;
    int travelerIndexX = travelerX / RECTANGLE_SIZE;
    int travelerIndexY = travelerY / RECTANGLE_SIZE;
    int diamondX = numRectanglesX/2;
    int diamondY = numRectanglesY/2;
    monster->x = numRectanglesX/4;
    monster->y = numRectanglesX/6;
    int score_jouer=0;
    int score=0;
    while (gameRunning) {
        // Gestion des événements
        /*while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                gameRunning = 0;
            }
        }*/

       
        travelerIndexX = travelerX / RECTANGLE_SIZE;
        travelerIndexY = travelerY / RECTANGLE_SIZE;

        // Déplacement du traveler

        //const Uint8 *keyboardState = SDL_GetKeyboardState(NULL);
        moveToDiamond_joueur(rectangles, travelerIndexX , travelerIndexY , diamondX , diamondY, numRectanglesX, numRectanglesY);
        


        // Manger le diamant
        
        if (travelerIndexX == diamondX && travelerIndexY == diamondY) {
            score_jouer ++;
            monster->health -= 2;
            diamondX = rand() % numRectanglesX;
            diamondY = rand() % numRectanglesY;
            do {
            // Étape a : Génération de coordonnées aléatoires
            diamondX = rand() % numRectanglesX;
            diamondY = rand() % numRectanglesY;

            // Étape b : Vérification si la position est un mur ou non
            if (rectangles[ diamondX][diamondY].h > 0) {
                // La position est un mur, générez une nouvelle position
                break;
            }

            // Si la position est invalide, sortir de la boucle
    
            } while (1);
        }
       
        if(aa==100){   
            updateAI_app(rectangles, rules ,monster,travelerIndexX ,travelerIndexY ,diamondX ,diamondY);
            monster->health--;}



            if (monster->x == travelerIndexX && monster->y == travelerIndexY) {
                    gameOver = 1;
                    score++;
                    monster->health +=1;

            }

         

        

        if (gameOver) {
             
            
            // Réinitialiser les positions du traveler et des bombes
            travelerX = numRectanglesX / 2 * RECTANGLE_SIZE + (RECTANGLE_SIZE - TRAVELER_SIZE) / 2;
            travelerY = numRectanglesY / 2 * RECTANGLE_SIZE + (RECTANGLE_SIZE - TRAVELER_SIZE) / 2;
            monster->x = numRectanglesX/4;
            monster->y = numRectanglesX/6;

           

             do {
            // Étape a : Génération de coordonnées aléatoires
            diamondX = rand() % numRectanglesX;
            diamondY = rand() % numRectanglesY;

            // Étape b : Vérification si la position est un mur ou non
            if (rectangles[ diamondX][diamondY].h > 0) {
                break;
            }

            // Si la position est invalide, sortir de la boucle
    
            } while (1);

            // Réinitialiser l'état du jeu
            if(monster->health<0)   break;
            gameOver = 0;
        }



        
     }

     return score;

}










/*-----------------fonction-essentiel----------------------------------------------------*/



int calcule_score(Rule * rule ,Monster * monsters , Traveler * traveler){


    int monsterX = 0;
    int monsterY = 0;
    int aa=1000;
    monsters->health=7;

    
    // Initialisation de SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Erreur lors de l'initialisation de SDL : %s\n", SDL_GetError());
        return -1;
        }

/*
    SDL_DisplayMode screen;

    SDL_GetCurrentDisplayMode(0, &screen);
       printf("Résolution écran\n\tw : %d\n\th : %d\n",screen.w, screen.h);*/

    int SCREEN_WIDTH = (int)(2560 *0.66);
    int  SCREEN_HEIGHT =(int)(1600* 0.66);



    int numRectanglesX = SCREEN_WIDTH / RECTANGLE_SIZE;
     int numRectanglesY = SCREEN_HEIGHT / RECTANGLE_SIZE;

    /*
       // Création de la fenêtre 
    SDL_Window*  window = SDL_CreateWindow("JG17",
                 SDL_WINDOWPOS_CENTERED,
                 SDL_WINDOWPOS_CENTERED, screen.w * 0.66,
                 screen.h * 0.66,
                 SDL_WINDOW_OPENGL);
    if (window == NULL) {
        printf("Erreur lors de l'initialisation de window : %s\n", SDL_GetError());
        return -1;
    }

    


    
     // Initialisation de SDL_ttf
    if (TTF_Init() < 0) {
        fprintf(stderr, "Erreur lors de l'initialisation de SDL_ttf: %s\n", TTF_GetError());
        return 1;
    }
     TTF_Font* font1 = TTF_OpenFont("/usr/share/fonts/truetype/tlwg/Laksaman-BoldItalic.ttf", 64);
    if (font1 == NULL) {
        fprintf(stderr, "Erreur lors du chargement de la police de caractères: %s\n", TTF_GetError());
        return 1;
    } 

    TTF_Font* font = TTF_OpenFont("/usr/share/fonts/truetype/tlwg/Laksaman-BoldItalic.ttf", 24);
    if (font == NULL) {
        fprintf(stderr, "Erreur lors du chargement de la police de caractères: %s\n", TTF_GetError());
        return 1;
    } 


    // Création du renderer
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) {
        printf("Erreur lors de la création du renderer : %s\n", SDL_GetError());
        SDL_DestroyWindow(window);
        SDL_Quit();
        return -1;
    }

    //include les images :
    my_texture1 = load_texture_from_image("bg_jeu.png", window, renderer);
    my_texture2 = load_texture_from_image("per_1.png", window, renderer);
    my_texture3 = load_texture_from_image("dm_hg.png", window, renderer);
    my_texture4 = load_texture_from_image("m1.jpg", window, renderer);
    my_texture5 = load_texture_from_image("action_bom.png", window, renderer);*/

    // Génération aléatoire du labyrinthe
    srand(time(NULL));
    

    SDL_Rect** rectangles = (SDL_Rect**)malloc(numRectanglesX * sizeof(SDL_Rect*));
    if (rectangles == NULL) {
        printf("Erreur lors de l'allocation de mémoire pour les rectangles.\n");
        //SDL_DestroyRenderer(renderer);
        //SDL_DestroyWindow(window);
        SDL_Quit();
        return -1;
    }

    for (int i = 0; i < numRectanglesX; i++) {
        rectangles[i] = (SDL_Rect*)malloc(numRectanglesY *sizeof(SDL_Rect));
        
        if (rectangles[i] == NULL) {
            printf("Erreur lors de l'allocation de mémoire pour les rectangles.\n");
            for (int j = 0; j < i; j++) {
                free(rectangles[j]);
            }
            free(rectangles);
            //SDL_DestroyRenderer(renderer);
            //SDL_DestroyWindow(window);
            SDL_Quit();
            return -1;
        }
        for (int j = 0; j < numRectanglesY; j++) {
            SDL_Rect rectangle = {i * RECTANGLE_SIZE, j * RECTANGLE_SIZE, RECTANGLE_SIZE, RECTANGLE_SIZE};
            rectangles[i][j] = rectangle;
        }
    }

    

    

    int diamondX = numRectanglesX/2;
    int diamondY = numRectanglesY/2;

    

    // Position initiale du traveler
    traveler->x = numRectanglesX/2;
    traveler->y = numRectanglesY/2;

    // Initialisation de la position des rand() % numRectanglesY
    for (int i = 0; i < MAX_MONSTERS; i++) {
        monsters[i].x = numRectanglesX/3 ;
        monsters[i].y = numRectanglesY/3;
        monsters[i].health = 19;
    }
    




    


    // Boucle principale du jeu
    int gameRunning = 1;
    //SDL_Event event;
    //int stepCount = 0;

    int score_joueur=0;
    int score = 0;
    
    while (gameRunning) {
        // Gestion des événements
        /*while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                gameRunning = 0;
            }
        }*/

       

        // Déplacement du traveler
        //int rand1 = rand();
        if(aa==1000){
            moveToDiamond_joueur(traveler,  diamondX , diamondY , numRectanglesX , numRectanglesY);
            /*const Uint8 *keyboardState = SDL_GetKeyboardState(NULL);
        if (keyboardState[SDL_SCANCODE_LEFT] && stepCount >= STEP_THRESHOLD && travelerX - RECTANGLE_SIZE >= 0) {
            int targetX = travelerX - RECTANGLE_SIZE;
            int targetIndexX = targetX / RECTANGLE_SIZE;
            int targetIndexY = travelerY / RECTANGLE_SIZE;
            if (targetIndexX >= 0 && rectangles[targetIndexX][targetIndexY].w > 0) {
                travelerX = targetX;
                SDL_Rect travelerRect = { travelerX, travelerY, TRAVELER_SIZE, TRAVELER_SIZE };
                play_with_t_4g(my_texture2, window,renderer,travelerRect,8,4,1,1,0);
                stepCount = 0;
            }
        }
        if (keyboardState[SDL_SCANCODE_RIGHT] && stepCount >= STEP_THRESHOLD && travelerX + RECTANGLE_SIZE + TRAVELER_SIZE <= SCREEN_WIDTH) {
            int targetX = travelerX + RECTANGLE_SIZE;
            int targetIndexX = targetX / RECTANGLE_SIZE;
            int targetIndexY = travelerY / RECTANGLE_SIZE;
            if (targetIndexX < numRectanglesX && rectangles[targetIndexX][targetIndexY].w > 0) {
                travelerX = targetX;
                SDL_Rect travelerRect = { travelerX, travelerY, TRAVELER_SIZE, TRAVELER_SIZE };
                play_with_t_4g(my_texture2, window,renderer,travelerRect,8,4,1,2,0);
                stepCount = 0;
            }
        }
        if (keyboardState[SDL_SCANCODE_UP] && stepCount >= STEP_THRESHOLD && travelerY - RECTANGLE_SIZE >= 0) {
            int targetY = travelerY - RECTANGLE_SIZE;
            int targetIndexX = travelerX / RECTANGLE_SIZE;
            int targetIndexY = targetY / RECTANGLE_SIZE;
            if (targetIndexY >= 0 && rectangles[targetIndexX][targetIndexY].h > 0) {
                travelerY = targetY;
                SDL_Rect travelerRect = { travelerX, travelerY, TRAVELER_SIZE, TRAVELER_SIZE };
                play_with_t_4g(my_texture2, window,renderer,travelerRect,8,4,1,3,0);
                stepCount = 0;
            }
        }
        if (keyboardState[SDL_SCANCODE_DOWN] && stepCount >= STEP_THRESHOLD && travelerY + RECTANGLE_SIZE + TRAVELER_SIZE <= SCREEN_HEIGHT) {
            int targetY = travelerY + RECTANGLE_SIZE;
            int targetIndexX = travelerX / RECTANGLE_SIZE;
            int targetIndexY = targetY / RECTANGLE_SIZE;
            if (targetIndexY < numRectanglesY && rectangles[targetIndexX][targetIndexY].h > 0) {
                travelerY = targetY;
                SDL_Rect travelerRect = { travelerX, travelerY, TRAVELER_SIZE, TRAVELER_SIZE };
                play_with_t_4g(my_texture2, window,renderer,travelerRect,8,4,1,0,0);
                stepCount = 0;
            }
        }*/
        
        


        if (monsterX == traveler->x && monsterY == traveler->y) {
                    gameOver = 1;
            }


        // Manger le diamant
        if (traveler->x == diamondX && traveler->y == diamondY) {
            score_joueur ++;
            monsters->health -= 1;
            diamondX = rand() % numRectanglesX;
            diamondY = rand() % numRectanglesY;
            do {
            // Étape a : Génération de coordonnées aléatoires
            diamondX = rand() % numRectanglesX;
            diamondY = rand() % numRectanglesY;

            // Étape b : Vérification si la position est un mur ou non
            if (rectangles[ diamondX][diamondY].h > 0) {
                // La position est un mur, générez une nouvelle position
                break;
            }

            // Si la position est invalide, sortir de la boucle
    
            } while (1);
        }


        if (monsters->x == traveler->x && monsters->y == traveler->y) {
                    gameOver = 1;
                    score++;
                    monsters->health +=2;

            }


        

            updateAI_app(rule ,monsters,traveler, diamondX , diamondY , numRectanglesX , numRectanglesY );
        
            // Vérification si le monstre entre en collision avec le joueur
        if (monsters->x == traveler->x && monsters->y == traveler->y) {
                    gameOver = 1;
                    score++;
                    monsters->health +=2;

            }

        }
        

        if (gameOver) {
             
            
            // Réinitialiser les positions du traveler et des bombes
            traveler->x=numRectanglesX/2;
            traveler->y = numRectanglesY/2;
           

             do {
            // Étape a : Génération de coordonnées aléatoires
            diamondX = rand() % numRectanglesX;
            diamondY = rand() % numRectanglesY;

            // Étape b : Vérification si la position est un mur ou non
            if (rectangles[ diamondX][diamondY].h > 0) {
                break;
            }

            // Si la position est invalide, sortir de la boucle
    
            } while (1);

            // Réinitialiser l'état du jeu
            gameOver = 0;


             // Afficher le message Game Over avec le score
              /*char message[100];
           snprintf(message, sizeof(message), "Game Over. Votre score est %d\n", score );
           SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, "Game Over", message,NULL);*/
           score_joueur =0;

           
        }


        
        // Incrémenter le compteur de pas
        //stepCount++;

        /*
        // Effacer l'écran
        SDL_SetRenderDrawColor(renderer, 64, 49, 45, 255);
        SDL_RenderClear(renderer);

        // Dessiner le labyrinthe
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
        for (int i = 0; i < numRectanglesX; i++) {
            for (int j = 0; j < numRectanglesY; j++) {
                
                play_with_texture_1(my_texture1,renderer, rectangles[i][j],0);
            }
        }

         // Dessiner le diamant
        if (diamondX != -1 && diamondY != -1) {
            SDL_Rect diamondRect = {diamondX * RECTANGLE_SIZE + (RECTANGLE_SIZE - DIAMOND_SIZE) / 2, diamondY * RECTANGLE_SIZE + (RECTANGLE_SIZE - DIAMOND_SIZE) / 2, DIAMOND_SIZE, DIAMOND_SIZE};
            
            play_with_texture_1(my_texture3, renderer, diamondRect,0);

        }

 

        // Dessiner le traveler
        SDL_Rect travelerRect = { traveler->x*RECTANGLE_SIZE, traveler->y*RECTANGLE_SIZE, TRAVELER_SIZE, TRAVELER_SIZE };
        play_with_texture_1(my_texture2, renderer,travelerRect,1);

        // Affichage des mostres
        //for (int i = 0; i < MAX_MONSTERS; i++) {
            if (monsters->health>0) {
                SDL_Rect monsterRect = {monsters->x *RECTANGLE_SIZE, monsters->y*RECTANGLE_SIZE , RECTANGLE_SIZE, RECTANGLE_SIZE};
                SDL_RenderCopy(renderer, my_texture4, NULL, &monsterRect);
            }
       // }

        // Affichage du score
        char scoreText[100];
        snprintf(scoreText, sizeof(scoreText), "Score: %d\t health: %d", score , monsters->health);
        SDL_Color textColor ;
        textColor.a=0;
        textColor.b=255;
        textColor.g=255;
        SDL_Surface* textSurface = TTF_RenderText_Solid(font, scoreText, textColor);
        if (textSurface == NULL) {
            fprintf(stderr, "Erreur lors du rendu du texte: %s\n", TTF_GetError());
            return 1;
        }
        SDL_Texture* textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
        if (textTexture == NULL) {
            fprintf(stderr, "Erreur lors de la création de la texture du texte: %s\n", SDL_GetError());
            return 1;
        }
        SDL_Rect textRect = { 10, 10, textSurface->w, textSurface->h };
        SDL_RenderCopy(renderer, textTexture, NULL, &textRect);
        SDL_FreeSurface(textSurface);
        SDL_DestroyTexture(textTexture);

        SDL_RenderPresent(renderer);*/

        aa--;
        if(aa==0){aa=1000;}


        if((monsters->health<1) || (monsters->health>20))  break;
    
    }
    

    // Nettoyage
    for (int j = 0; j < numRectanglesX; j++) {
        free(rectangles[j]);
    }
    free(rectangles);
    //free(bombs);
    //free(monsters);
    /*SDL_DestroyTexture(my_texture1);
    SDL_DestroyTexture(my_texture2);
    SDL_DestroyTexture(my_texture3);
    SDL_DestroyTexture(my_texture4);
    SDL_DestroyTexture(my_texture5);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);*/
    SDL_Quit();

    //printf("\nla score de regle : \t %d\n" , score);

    return score ;

}