/*
     Voici un premier programme en C qui correspond au "Hello World" classique
*/

#include <stdio.h>
#include <math.h>


void somm_tab()
{
   printf("entrer n : \n");
   int n ;
   scanf("%d", &n);
   int i=0 , s = 0 ;
   float inv = 0.0 ;
   int l=1;
   while (l<=2)
   {
       printf("la liste des nombre de puissance %d  est :", l);
       for(i=1; i<=n ; i++)
       {
             printf("%d ",(int)pow(i,l)); 
             s = s + i*i ;
             inv = inv + (1.0/(i*i));
       }
       printf("\n");
       
       l = l+1 ;
   }
   printf("\n la somme de carrés de %d est : %d .\n", n, s/2);
   printf(" la somme des inverses  carrés de %d est : %f .\n", n, inv/2);
   int k=0;
   while (k<=12)
   {
       int j = 0;
       printf("tableau de multiplication de %d est : \n " , k);
       for (j=0; j<=9 ; j++){
               printf("                              %d x %d = %d \n " , k, j , k*j);
               }
       printf("\n --------- \n");
       
       k = k+1 ;
   
   
   }



}




void resol_eq2()
{
   printf("entrer a , b ,c\n");
   float a;
   float b;
   float c;
   
   scanf("%f", &a);
   scanf("%f", &b);
   scanf("%f", &c);
   float del = b*b -4*a*c;
   if ( a==0 && b==0 && c==0)    printf("infinité de solutions \n ");
   if ( a==0 && b==0 & c!=0)     printf("impossible\n");
   if ( a==0 && b!=0 )                    printf("premiere degree a une solution %f \n",-c/b);  
   if ( del == 0 && a!=0 )                printf("une  solution double %f \n" , -b/(2*a));
   if (del > 0 &&  a!=0)                 printf("deux solutions differents %f %f \n" , (-b + sqrt(del))/(2*a) , (-b - sqrt(del))/(2*a));
   if ( del <0 && a!=0 )                 printf("aucun solution reelle!!\n");





}




int main1()
{
    int i ;
    printf("entrer i:\n");
    scanf("%d", &i);

    if (i == -1)  printf("i vaut -1\n");
    if (i != -1)  printf("i est différent de -1\n");
    if (i != 0)   printf("i est non nul\n");
    if (i)        printf("i est non nul également\n");
    if (i == 0)   printf("i est nul\n");
    if (!i)       printf("i est nul encore !\n");
    if ((i>=0) && (i<=10)) printf("i est petit\n");
    if ((i>3) || (i<-3)) printf("i est hors intervalle\n");     
    return 0;
}



int main2() 
{
  float x =1.0, y=.0;
  int i = 7;
  /* Quelques exemples */
  printf("Un mot\n");
  printf("un nombre : %d\n", 3);
  printf("Solution x:%f, y:%f\n", x, y);

  printf("%d\n", i);
  printf("%d\n", ++i);
  printf("%d\n", i);
  printf("%d\n", i++);
  printf("%d\n", i);

  printf("%e\n", 3.5);
  printf("\n");
  printf("\t\t %d\n", i);

  return 0;
}



int main()
{
   printf("Bonjour les ZZ1!\n");
   printf("Comment allez-vous ?\n");
   main1();
   main2();
   //somm_tab();
   resol_eq2();
   somm_tab();
   
   

   return 0;
}
