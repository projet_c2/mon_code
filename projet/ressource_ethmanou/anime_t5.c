#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

void end_sdl(char ok,
             char const* msg,
             SDL_Window* window,
             SDL_Renderer* renderer) {
    char msg_formated[255];
    int l;

    if (!ok) {
        strncpy(msg_formated, msg, 250);
        l = strlen(msg_formated);
        strcpy(msg_formated + l, " : %s\n");

        SDL_Log(msg_formated, SDL_GetError());
    }

    if (renderer != NULL) {
        SDL_DestroyRenderer(renderer);
        renderer = NULL;
    }
    if (window != NULL) {
        SDL_DestroyWindow(window);
        window = NULL;
    }

    SDL_Quit();

    if (!ok) {
        exit(EXIT_FAILURE);
    }
}

SDL_Texture* load_texture_from_image(const char* file_image_name, SDL_Window* window, SDL_Renderer* renderer) {
    SDL_Surface* my_image = NULL;
    SDL_Texture* my_texture = NULL;

    my_image = IMG_Load(file_image_name);
    if (my_image == NULL)
        end_sdl(0, "Chargement de l'image impossible", window, renderer);

    my_texture = SDL_CreateTextureFromSurface(renderer, my_image);
    SDL_FreeSurface(my_image);
    if (my_texture == NULL)
        end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);

    return my_texture;
}

/*pas de changement dans cette fonction souffe les image*/
void play_with_texture_5(SDL_Texture* bg_texture, SDL_Texture* my_texture, SDL_Window* window, SDL_Renderer* renderer) {
    SDL_Rect source = {0},
              window_dimensions = {0},
              destination = {0};

    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);

    int nb_images = 62;
    int nb_images_animation = 2* nb_images;
    float zoom = 0.5;
    int offset_x = source.w / 8,
        offset_y = source.h / 8;
    SDL_Rect state[124];

    int i = 0;
    for (int y = 0; y < source.h; y += offset_y) {
        for (int x = 0; x < source.w; x += offset_x) {
            state[i].x = x;
            state[i].y = y;
            state[i].w = offset_x;
            state[i].h = offset_y;
            ++i;
        }
    }

    //state[3] = state[4]; state[7]=state[8];state[11]=state[12];state[15]=state[16];
    for(int j =0; j<nb_images;j+=9){ state[j-1]=state[j];}

    for (; i < nb_images; ++i) {
        state[61 - i] = state[i];
    }

    destination.w = offset_x * zoom;
    destination.h = offset_y * zoom;
    destination.x = window_dimensions.w * 0.55;
    destination.y = window_dimensions.h * 0.75;

    i = 0;
   for (int cpt = 0; cpt < nb_images_animation; ++cpt) {
    SDL_RenderCopy(renderer, bg_texture, NULL, &window_dimensions);
    SDL_RenderCopy(renderer, my_texture, &state[i], &destination);
    i = (i + 1) % nb_images;
    SDL_RenderPresent(renderer);
    SDL_Delay(50); // Utiliser un délai d'environ 16 ms (correspondant à environ 60 FPS)
    }


    SDL_RenderClear(renderer);
}


int main(int argc, char** argv) {
    (void)argc;
    (void)argv;

    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;

    SDL_DisplayMode screen;

    if (SDL_Init(SDL_INIT_VIDEO) != 0)
        end_sdl(0, "ERROR SDL INIT", window, renderer);

    SDL_GetCurrentDisplayMode(0, &screen);
    printf("Résolution écran\n\tw : %d\n\th : %d\n", screen.w, screen.h);

    window = SDL_CreateWindow("Premier dudo dessin",
                              SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED,
                              screen.w * 0.66,
                              screen.h * 0.66,
                              SDL_WINDOW_OPENGL);
    if (window == NULL)
        end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL)
        end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

    SDL_Texture* my_texture, *bg_texture;
    my_texture = load_texture_from_image("img/ab_et.png", window, renderer);
    bg_texture = load_texture_from_image("img/bg_et.png", window, renderer);
    if (my_texture == NULL)
        end_sdl(0, "Echec du chargement de l'image dans la texture", window, renderer);

    play_with_texture_5(bg_texture, my_texture, window, renderer);

    SDL_RenderPresent(renderer);

    SDL_RenderClear(renderer);

    IMG_Quit();

    SDL_DestroyTexture(my_texture);
    end_sdl(1, "Normal ending", window, renderer);
    return EXIT_SUCCESS;
}
