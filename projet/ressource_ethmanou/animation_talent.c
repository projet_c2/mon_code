#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

void end_sdl(char ok, char const* msg, SDL_Window* window, SDL_Renderer* renderer) {
    char msg_formated[255];
    int l;
 
    if (!ok) {
        strncpy(msg_formated, msg, 250);
        l = strlen(msg_formated);
        strcpy(msg_formated + l, " : %s\n");

        SDL_Log(msg_formated, SDL_GetError());
    }

    if (renderer != NULL) {
        SDL_DestroyRenderer(renderer);
        renderer = NULL;
    }

    if (window != NULL) {
        SDL_DestroyWindow(window);
        window = NULL;
    }

    SDL_Quit();

    if (!ok) {
        exit(EXIT_FAILURE);
    }
}

SDL_Texture* load_texture_from_image(char* file_image_name, SDL_Window* window, SDL_Renderer* renderer) {
    SDL_Surface* my_image = NULL;
    SDL_Texture* my_texture = NULL;

    my_image = IMG_Load(file_image_name);
    if (my_image == NULL)
        end_sdl(0, "Chargement de l'image impossible", window, renderer);

    my_texture = SDL_CreateTextureFromSurface(renderer, my_image);
    SDL_FreeSurface(my_image);
    if (my_texture == NULL)
        end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);

    return my_texture;
}

void play_with_texture_1(SDL_Texture* my_texture, SDL_Window* window, SDL_Renderer* renderer) {
    SDL_Rect source = {0},
              window_dimensions = {0},
              destination = {0};

    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);

    destination = window_dimensions;

    SDL_RenderCopy(renderer, my_texture, &source, &destination);
}

void play_with_texture_2(SDL_Texture* my_texture, SDL_Window* window, SDL_Renderer* renderer, float zoom) {
    SDL_Rect source = {0},
              window_dimensions = {0},
              destination = {0};

    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);

    destination.w = source.w * zoom;
    destination.h = source.h * zoom;
    destination.x = (destination.w) / 4;
    destination.y = 3*(destination.h) / 4;

    SDL_RenderCopy(renderer, my_texture, &source, &destination);
}

void play_with_texture_3(SDL_Texture* my_texture, SDL_Window* window, SDL_Renderer* renderer, float zoom) {
    SDL_Rect source = {0},
              window_dimensions = {0},
              destination = {0};

    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);

    int nb_it = 100;
    destination.w = source.w * zoom;
    destination.h = source.h * zoom;
    destination.x = (window_dimensions.w - destination.w) / 2;
    destination.y = (window_dimensions.h-destination.h)/2;

    for (int i = 0; i < nb_it; ++i) {
       // destination.y -= (window_dimensions.h / nb_it);  mouvement ici
       destination.x = (window_dimensions.w - destination.w) / 2 +  (window_dimensions.w - destination.w) / 2 * cos(2*M_PI*i/ nb_it);
       destination.y = (window_dimensions.h - destination.h) / 2 +  (window_dimensions.h - destination.h) / 2 * sin(2*M_PI*i/ nb_it);
       

        SDL_Texture* my_texture1;
        SDL_Texture* my_texture2;
        my_texture1 = load_texture_from_image("img/bg_et.png", window, renderer);
        my_texture2 = load_texture_from_image("img/m.png", window, renderer);

        if ((my_texture1 == NULL) && (my_texture2 == NULL))
            end_sdl(0, "Echec du chargement de l'image dans la texture", window, renderer);

        SDL_RenderClear(renderer);
        play_with_texture_1(my_texture1, window, renderer);
        //play_with_texture_2(my_texture2, window, renderer, 0.15);
        SDL_RenderCopy(renderer, my_texture, &source, &destination);
        SDL_RenderPresent(renderer);
        SDL_Delay(30);

        SDL_RenderClear(renderer);
    }

}

int main(int argc, char** argv) {
    (void)argc;
    (void)argv;

    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;
    SDL_DisplayMode screen;

    if (SDL_Init(SDL_INIT_VIDEO) != 0)
        end_sdl(0, "ERROR SDL INIT", window, renderer);

    SDL_GetCurrentDisplayMode(0, &screen);
    printf("Résolution écran\n\tw : %d\n\th : %d\n", screen.w, screen.h);

    window = SDL_CreateWindow("Premier dudo dessin",
                              SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED,
                              screen.w * 0.66,
                              screen.h * 0.66,
                              SDL_WINDOW_OPENGL);

    if (window == NULL)
        end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL)
        end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

    SDL_Texture* my_texture3;
    my_texture3 = load_texture_from_image(/*"zone_eprience/img/ship.png"*/"img/m.png", window, renderer);

    if (my_texture3 == NULL)
        end_sdl(0, "Echec du chargement de l'image dans la texture", window, renderer);

    SDL_RenderClear(renderer);
    play_with_texture_3(my_texture3, window, renderer, 0.03);
    SDL_RenderPresent(renderer);
    SDL_Delay(1);

    IMG_Quit();

    SDL_DestroyTexture(my_texture3);
    end_sdl(1, "Normal ending", window, renderer);
    return EXIT_SUCCESS;
}
