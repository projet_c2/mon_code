#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void end_sdl(char ok, const char* msg, SDL_Window* window, SDL_Renderer* renderer) {
    if (!ok) {
        printf("%s: %s\n", msg, SDL_GetError());
    }
    if (renderer != NULL) {
        SDL_DestroyRenderer(renderer);
    }
    if (window != NULL) {
        SDL_DestroyWindow(window);
    }
    IMG_Quit();
    SDL_Quit();
    exit(ok ? EXIT_SUCCESS : EXIT_FAILURE);
}

SDL_Texture* load_texture_from_image(const char* file_image_name, SDL_Window* window, SDL_Renderer* renderer) {
    SDL_Surface* my_image = NULL;
    SDL_Texture* my_texture = NULL;

    my_image = IMG_Load(file_image_name);
    if (my_image == NULL) {
        end_sdl(0, "Chargement de l'image impossible", window, renderer);
    }

    my_texture = SDL_CreateTextureFromSurface(renderer, my_image);
    SDL_FreeSurface(my_image);
    if (my_texture == NULL) {
        end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);
    }

    return my_texture;
}

void play_with_texture_1(SDL_Texture* my_texture, SDL_Window* window, SDL_Renderer* renderer) {
    SDL_Rect source = {0},
              window_dimensions = {0},
              destination = {0};

    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);

    destination = window_dimensions;

    SDL_RenderCopy(renderer, my_texture, &source, &destination);
}

void play_with_texture_2(SDL_Texture* my_texture, SDL_Window* window, SDL_Renderer* renderer, float zoom) {
    SDL_Rect source = {0},
              window_dimensions = {0},
              destination = {0};

    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);

    destination.w = source.w * zoom;
    destination.h = source.h * zoom;
    destination.x = (destination.w) / 2;
    destination.y = (destination.h) / 2;

    SDL_RenderCopy(renderer, my_texture, &source, &destination);
}

void play_with_texture_4(SDL_Texture* my_texture, SDL_Window* window, SDL_Renderer* renderer, int speed) {
    SDL_Rect source = {0};
    SDL_Rect window_dimensions = {0};
    SDL_Rect destination = {0};
    SDL_Rect state = {0};

    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);

    int nb_images = 9;
    float zoom = 2;
    int offset_x = source.w / nb_images;
    int offset_y = source.h /6;

    state.x = 0;
    state.y = 1 * offset_y;
    state.w = offset_x;
    state.h = offset_y;

    destination.w = offset_x * zoom;
    destination.h = offset_y * zoom;
    destination.y = window_dimensions.h -( destination.h)*(2) ;

    //int speed = 10;
    int x = 0;

    while (x < window_dimensions.w /*- destination.w*/) {
        x += speed;
        destination.x = x;
        state.x += offset_x;
        state.x %= source.w;
        


        SDL_Texture* my_texture1;
        SDL_Texture* my_texture2;
        my_texture2 = load_texture_from_image("img/z.png", window, renderer);
        my_texture1 = load_texture_from_image("img/bg_et.png", window, renderer);
        SDL_RenderClear(renderer);

        if ((my_texture1 == NULL) && (my_texture2 == NULL))
            end_sdl(0, "Echec du chargement de l'image dans la texture", window, renderer);

        /* affichge              */
        play_with_texture_1(my_texture1, window, renderer);
        //play_with_texture_2(my_texture2, window, renderer, 0.25);
        SDL_RenderCopy(renderer, my_texture, &state, &destination);
        SDL_RenderPresent(renderer);
        SDL_Delay(90);
    }

    SDL_RenderClear(renderer);
}

int main(int argc, char** argv) {
    (void)argc;
    (void)argv;

    SDL_DisplayMode screen;

    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;
    SDL_Texture* my_texture1 = NULL;
    SDL_Texture* my_texture2 = NULL;

    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        printf("Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
        return EXIT_FAILURE;
    }

    SDL_GetCurrentDisplayMode(0, &screen);
       printf("Résolution écran\n\tw : %d\n\th : %d\n",
          screen.w, screen.h);

       /* Création de la fenêtre */
       window = SDL_CreateWindow("Premier dudo dessin",
                 SDL_WINDOWPOS_CENTERED,
                 SDL_WINDOWPOS_CENTERED, screen.w * 0.66,
                 screen.h * 0.66,
                 SDL_WINDOW_OPENGL);
    if (window == NULL) {
        printf("Erreur lors de la création de la fenêtre : %s\n", SDL_GetError());
        SDL_Quit();
        return EXIT_FAILURE;
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) {
        printf("Erreur lors de la création du renderer : %s\n", SDL_GetError());
        SDL_DestroyWindow(window);
        SDL_Quit();
        return EXIT_FAILURE;
    }

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);

    my_texture1 = load_texture_from_image("img/z.png", window, renderer);
    //my_texture2 = load_texture_from_image("zone_eprience/img/defmvsp.png", window, renderer);
    play_with_texture_4(my_texture1, window, renderer,10);
    //play_with_texture_4(my_texture2, window, renderer,5);

    SDL_DestroyTexture(my_texture1);
    SDL_DestroyTexture(my_texture2);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    IMG_Quit();
    SDL_Quit();

    return EXIT_SUCCESS;
}
