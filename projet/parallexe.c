#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

// Constantes de la fenêtre
const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT = 600;


// Constantes du fond parallaxe
const int BACKGROUND_LAYER_COUNT = 7;
const int BACKGROUND_SCROLL_SPEEDS[] = {1, 2, 3, 4 , 5, 6,7 ,8,9,10,11,12};
const Uint32 BACKGROUND_CHANGE_INTERVAL = 3; // Durée en millisecondes avant de changer le fond
const int BACKGROUND_FADE_SPEED = 5; // Vitesse de transition d'opacité du fond

const char* BACKGROUND_IMAGES[] = {
    "fonts/b.png",
    "fonts/b.png",
    "fonts/b.png",
    "fonts/b.png",
    "fonts/b.png",
    "fonts/b.png",
    "fonts/b.png",
    "fonts/b.png",
    "fonts/b.png",
    "fonts/b.png",
    "fonts/b.png",
    "fonts/b.png"

};

// Structure pour représenter un calque de fond parallaxe
typedef struct {
    int x;
    SDL_Texture* texture;
} BackgroundLayer;



int main() {
    // Initialisation de la seed pour la génération de nombres aléatoires
    srand(time(NULL));

    // Initialisation de SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        fprintf(stderr, "Erreur lors de l'initialisation de SDL: %s\n", SDL_GetError());
        return 1;
    }

    // Initialisation de SDL_image
    if (IMG_Init(IMG_INIT_PNG) < 0) {
        fprintf(stderr, "Erreur lors de l'initialisation de SDL_image: %s\n", IMG_GetError());
        return 1;
    }



    // Création de la fenêtre
    SDL_Window* window = SDL_CreateWindow("Jeu de formes", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
    if (window == NULL) {
        fprintf(stderr, "Erreur lors de la création de la fenêtre: %s\n", SDL_GetError());
        return 1;
    }

    // Création du renderer
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL) {
        fprintf(stderr, "Erreur lors de la création du renderer: %s\n", SDL_GetError());
        return 1;
    }

 
    // Création des calques de fond parallaxe
    BackgroundLayer backgroundLayers[BACKGROUND_LAYER_COUNT];
    for (int i = 0; i < BACKGROUND_LAYER_COUNT; i++) {
        SDL_Surface* backgroundSurface = IMG_Load(BACKGROUND_IMAGES[i]);
        if (backgroundSurface == NULL) {
            fprintf(stderr, "Erreur lors du chargement de l'image de fond: %s\n", SDL_GetError());
            return 1;
        }
        SDL_Texture* backgroundTexture = SDL_CreateTextureFromSurface(renderer, backgroundSurface);
        if (backgroundTexture == NULL) {
            fprintf(stderr, "Erreur lors de la création de la texture de fond: %s\n", SDL_GetError());
            return 1;
        }
        SDL_FreeSurface(backgroundSurface);

        BackgroundLayer layer;
        layer.x = 0;
        layer.texture = backgroundTexture;
        backgroundLayers[i] = layer;
    }
    //SDL_RenderClear(renderer);

    Uint32 lastBackgroundChangeTime = 0; // Temps du dernier changement de fond
    int currentBackgroundLayer = 0; // Calque de fond actuel
    Uint8 backgroundAlpha = 255; // Opacité du fond

    SDL_Event event;
    bool quit = false;
    //int score = 0;

    // Variables pour la gestion du délai de frame
    Uint32 frameStartTime; // Temps de départ du frame actuel
    Uint32 frameEndTime; // Temps d'arrivée du frame actuel
    Uint32 frameDuration; // Durée du frame actuel en millisecondes
    Uint32 desiredFrameDuration = 1; // Durée souhaitée de chaque frame en millisecondes

    while (!quit) {
        // Mesurer le temps de départ du frame actuel
        frameStartTime = SDL_GetTicks();

        // Gestion des événements
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                quit = true;
            }
        }


     
        // Gestion du fond parallaxe
        Uint32 currentTicks = SDL_GetTicks();
        if (currentTicks - lastBackgroundChangeTime >= BACKGROUND_CHANGE_INTERVAL) {
            lastBackgroundChangeTime = currentTicks;
            currentBackgroundLayer = (currentBackgroundLayer + 1) % BACKGROUND_LAYER_COUNT;
            backgroundAlpha = 0;
        }

        // Réglage de l'opacité du fond
        if (backgroundAlpha < 255) {
            backgroundAlpha += BACKGROUND_FADE_SPEED;
                backgroundAlpha = 255;
        }


        // Dessin du fond parallaxe
        for (int i = 0; i < BACKGROUND_LAYER_COUNT; i++) {
            SDL_Rect backgroundRect = { backgroundLayers[i].x, 0, WINDOW_WIDTH, WINDOW_HEIGHT };
            SDL_SetTextureAlphaMod(backgroundLayers[i].texture, backgroundAlpha);
            SDL_RenderCopy(renderer, backgroundLayers[i].texture, NULL, &backgroundRect);

            backgroundLayers[i].x -= BACKGROUND_SCROLL_SPEEDS[i];
            if (backgroundLayers[i].x <= -WINDOW_WIDTH) {
                backgroundLayers[i].x = WINDOW_WIDTH;
            }
        }

        // Affichage du rendu
        SDL_RenderPresent(renderer);

    

    }

    // Libération des ressources
    for (int i = 0; i < BACKGROUND_LAYER_COUNT; i++) {
        SDL_DestroyTexture(backgroundLayers[i].texture);
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
