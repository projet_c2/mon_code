#ifndef MAIN_JEU_H
#define MAIN_JEU_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>



// Structure pour représenter une forme
typedef struct {
    int x;
    int y;
    float speed;
    SDL_Texture* texture;
} Shape;

// Structure pour représenter un calque de fond parallaxe
typedef struct {
    int x;
    SDL_Texture* texture;
} BackgroundLayer;

// Fonction pour générer un nombre aléatoire entre min et max
int getRandomNumber(int min, int max);

// Fonction pour réinitialiser la position et la vitesse d'une forme lorsqu'elle sort de l'écran
void resetShape(Shape* shape);

#endif

