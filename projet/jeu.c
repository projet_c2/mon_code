#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

// Constantes de la fenêtre
const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT = 600;

// Constantes des formes
const int SHAPE_SIZE = 50;
const int INITIAL_SHAPE_SPEED = 5;
const int SCORE_INCREMENT = 1;
const float SPEED_INCREASE_FACTOR = 1.0 / 3.0;

// Constantes du plateau
const int PLATE_WIDTH = 100;
const int PLATE_HEIGHT = 20;
const int PLATE_SPEED = 5;

// Constantes du fond parallaxe
const int BACKGROUND_LAYER_COUNT = 3;
const int BACKGROUND_SCROLL_SPEEDS[] = {1, 2, 3};
const Uint32 BACKGROUND_CHANGE_INTERVAL = 30; // Durée en millisecondes avant de changer le fond
const int BACKGROUND_FADE_SPEED = 5; // Vitesse de transition d'opacité du fond

const char* BACKGROUND_IMAGES[] = {
    "fonts/bg1.jpeg",
    "fonts/bg2.png",
    "fonts/bg3.jpeg"
};

// Structure pour représenter une forme
typedef struct {
    int x;
    int y;
    float speed;
    SDL_Texture* texture;
} Shape;

// Structure pour représenter un calque de fond parallaxe
typedef struct {
    int x;
    SDL_Texture* texture;
} BackgroundLayer;

// Fonction pour générer un nombre aléatoire entre min et max
int getRandomNumber(int min, int max) {
    return rand() % (max - min + 1) + min;
}

// Fonction pour réinitialiser la position et la vitesse d'une forme lorsqu'elle sort de l'écran
void resetShape(Shape* shape) {
    shape->x = getRandomNumber(0, WINDOW_WIDTH - SHAPE_SIZE);
    shape->y = 0;
    shape->speed = INITIAL_SHAPE_SPEED;
}

int main() {
    // Initialisation de la seed pour la génération de nombres aléatoires
    srand(time(NULL));

    // Initialisation de SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        fprintf(stderr, "Erreur lors de l'initialisation de SDL: %s\n", SDL_GetError());
        return 1;
    }

    // Initialisation de SDL_image
    if (IMG_Init(IMG_INIT_PNG) < 0) {
        fprintf(stderr, "Erreur lors de l'initialisation de SDL_image: %s\n", IMG_GetError());
        return 1;
    }

    // Initialisation de SDL_ttf
    if (TTF_Init() < 0) {
        fprintf(stderr, "Erreur lors de l'initialisation de SDL_ttf: %s\n", TTF_GetError());
        return 1;
    }

    TTF_Font* font = TTF_OpenFont("/usr/share/fonts/truetype/tlwg/Laksaman-BoldItalic.ttf", 24);
    if (font == NULL) {
        fprintf(stderr, "Erreur lors du chargement de la police de caractères: %s\n", TTF_GetError());
        return 1;
    }

    // Création de la fenêtre
    SDL_Window* window = SDL_CreateWindow("Jeu de formes", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
    if (window == NULL) {
        fprintf(stderr, "Erreur lors de la création de la fenêtre: %s\n", SDL_GetError());
        return 1;
    }

    // Création du renderer
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) {
        fprintf(stderr, "Erreur lors de la création du renderer: %s\n", SDL_GetError());
        return 1;
    }

    // Chargement des textures des formes
    SDL_Texture* rectTexture = IMG_LoadTexture(renderer, "fonts/im.jpeg");
    SDL_Texture* circleTexture = IMG_LoadTexture(renderer, "fonts/Cercle.png");
    if (rectTexture == NULL || circleTexture == NULL) {
        fprintf(stderr, "Erreur lors du chargement des textures des formes: %s\n", SDL_GetError());
        return 1;
    }

    // Chargement des textures du fond parallaxe
    BackgroundLayer backgroundLayers[BACKGROUND_LAYER_COUNT];
    for (int i = 0; i < BACKGROUND_LAYER_COUNT; i++) {
        backgroundLayers[i].x = 0;
        backgroundLayers[i].texture = IMG_LoadTexture(renderer, BACKGROUND_IMAGES[i]);
        if (backgroundLayers[i].texture == NULL) {
            fprintf(stderr, "Erreur lors du chargement de la texture du fond parallaxe: %s\n", SDL_GetError());
            return 1;
        }
    }
    int backgroundOpacity = 255;  // Opacité initiale à 255 (complètement opaque)

    // Initialisation du plateau
    int plateX = WINDOW_WIDTH / 2 - PLATE_WIDTH / 2;
    int plateY = WINDOW_HEIGHT - PLATE_HEIGHT;

    // Création de la forme et réinitialisation
    Shape shape;
    shape.texture = rectTexture;
    resetShape(&shape);

    // Variables de jeu
    int score = 0;
    bool gameOver = false;

    // Variables pour le fond parallaxe
    Uint32 lastBackgroundChangeTime = SDL_GetTicks();
    int currentBackground = 0;

    // Boucle principale du jeu
    SDL_Event event;
    while (!gameOver) {
        // Gestion des événements
        while (SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) {
                gameOver = true;
            }
        }

        // Mise à jour du plateau
        const Uint8* keyboardState = SDL_GetKeyboardState(NULL);
if (keyboardState[SDL_SCANCODE_LEFT] && plateX > 0) {
    plateX -= PLATE_SPEED;
}
if (keyboardState[SDL_SCANCODE_RIGHT] && plateX < WINDOW_WIDTH - PLATE_WIDTH) {
    plateX += PLATE_SPEED;
}

// Vérification de la collision entre la forme et le plateau
if (shape.y + SHAPE_SIZE >= plateY && shape.y + SHAPE_SIZE <= plateY + PLATE_HEIGHT &&
    shape.x + SHAPE_SIZE >= plateX && shape.x <= plateX + PLATE_WIDTH) {
    // Collision détectée, réinitialisation de la forme et augmentation du score
    resetShape(&shape);
    score++;
    shape.speed += SPEED_INCREASE_FACTOR;
}

        // Mise à jour des formes
        shape.y += shape.speed;
        if (shape.y > WINDOW_HEIGHT) {
            // La forme est sortie de l'écran, on réinitialise
            resetShape(&shape);
            score++;
            shape.speed += SPEED_INCREASE_FACTOR;
        }

        // Mise à jour du fond parallaxe
        Uint32 currentTime = SDL_GetTicks();
        Uint32 elapsedTime = currentTime - lastBackgroundChangeTime;
        // Mettre à jour l'opacité du calque de fond actuel
        if (backgroundOpacity < 255) {
            backgroundOpacity += BACKGROUND_FADE_SPEED;
            if (backgroundOpacity > 255) {
                backgroundOpacity = 255;  // Limite l'opacité maximale à 255 (complètement opaque)
            }
        }

        // Rendu du fond parallaxe
        for (int i = 0; i < BACKGROUND_LAYER_COUNT; i++) {
        SDL_Rect backgroundRect = { backgroundLayers[i].x, backgroundLayers[i].y, WINDOW_WIDTH, WINDOW_HEIGHT };
        SDL_SetTextureAlphaMod(backgroundLayers[i].texture, backgroundOpacity);
        SDL_RenderCopy(renderer, backgroundLayers[i].texture, NULL, &backgroundRect);
    }

        // Rendu du plateau
        SDL_Rect plateRect = { plateX, plateY, PLATE_WIDTH, PLATE_HEIGHT };
        SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);
        SDL_RenderFillRect(renderer, &plateRect);

        // Rendu de la forme
        SDL_Rect shapeRect = { shape.x, shape.y, SHAPE_SIZE, SHAPE_SIZE };
        SDL_RenderCopy(renderer, shape.texture, NULL, &shapeRect);

        // Affichage du score
        char scoreText[100];
        sprintf(scoreText, "Score: %d", score);

        SDL_Color textColor = { 0x00, 0x00, 0x00 };
        SDL_Surface* textSurface = TTF_RenderText_Solid(font, scoreText, textColor);
        if (textSurface == NULL) {
            fprintf(stderr, "Erreur lors du rendu du texte: %s\n", TTF_GetError());
            return 1;
        }

        SDL_Texture* textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
        if (textTexture == NULL) {
            fprintf(stderr, "Erreur lors de la création de la texture du texte: %s\n", SDL_GetError());
            return 1;
        }

        SDL_Rect textRect = { 10, 10, textSurface->w, textSurface->h };
        SDL_RenderCopy(renderer, textTexture, NULL, &textRect);

        // Mise à jour de l'affichage
        SDL_RenderPresent(renderer);

        // Nettoyage
        SDL_FreeSurface(textSurface);
        SDL_DestroyTexture(textTexture);
    }

    // Libération des ressources
    for (int i = 0; i < BACKGROUND_LAYER_COUNT; i++) {
        SDL_DestroyTexture(backgroundLayers[i].texture);
    }

    SDL_DestroyTexture(rectTexture);
    SDL_DestroyTexture(circleTexture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    // Libération de SDL_image et SDL_ttf
    IMG_Quit();
    TTF_Quit();

    // Libération de SDL
    SDL_Quit();

    return 0;
}
