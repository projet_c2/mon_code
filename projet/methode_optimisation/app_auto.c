#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define NUM_VARS 10
#define NUM_VALUES 12
#define NUM_ANTS 100

double pheromones[NUM_VARS][NUM_VALUES];

typedef struct {
    int values[NUM_VARS];
} State;



double energy1(State state) {
    double energy = 0.0;

    for (int i = 0; i < NUM_VARS; i++) {
        energy += pow(state.values[i], 2);
    }

    return energy;
}


double f(double x, double power) {
    return 1.0 / pow(x, power);
}

double* apriori(State state, int pos) {
    static double qualities[NUM_VALUES];
    State tempState = state;
    int save = tempState.values[pos];

    for (int v = 1; v <= NUM_VALUES; v++) {
        tempState.values[pos] = v;
        qualities[v - 1] = energy1(tempState);
    }

    tempState.values[pos] = save;
    return qualities;
}

State construction(double alpha, double power, double pmin, double pmax) {
    int order[NUM_VARS];
    for (int i = 0; i < NUM_VARS; i++) {
        order[i] = i;
    }

    

    State state;
    for (int i = 0; i < NUM_VARS; i++) {
        double qualities[NUM_VALUES];
        double interest[NUM_VALUES];
        double total = 0.0;
        double cumulative = 0.0;
        double p = (double)rand() / RAND_MAX;

        double* qualitiesPtr = apriori(state, order[i]);

        for (int j = 0; j < NUM_VALUES; j++) {
            qualities[j] = qualitiesPtr[j];
        }

        for (int j = 0; j < NUM_VALUES; j++) {
            interest[j] = f(qualities[j] + 1, power) + alpha * pheromones[order[i]][j];
            total += interest[j];
        }

        for (int j = 0; j < NUM_VALUES; j++) {
            interest[j] = fmax(fmin(interest[j] / total, pmax), pmin);
            cumulative += interest[j] / total;
            if (p <= cumulative) {
                state.values[order[i]] = j + 1;
                break;
            }
        }
    }

    return state;
}

void antColony(double alpha, double beta, double power, double pmin, double pmax, int maxIterations) {
    double bestE = 10000.0;
    double total = 0.0;
    int cpt = 0;

    while (cpt < maxIterations) {
        double newPheromones[NUM_VARS][NUM_VALUES] = {{0.0}};

        for (int ant = 0; ant < NUM_ANTS; ant++) {
            State state = construction(alpha, power, pmin, pmax);
            double E = energy1(state);

            if (E == 0.0) {
                printf("%d **************************************************************\n", cpt);
                for (int i = 0; i < NUM_VARS; i++) {
                    printf("%d ", state.values[i]);
                }
                printf("\n");
            } else {
                if (E < bestE) {
                    bestE = E;
                }

                for (int i = 0; i < NUM_VARS; i++) {
                    newPheromones[i][state.values[i] - 1] += f(E, power);
                }
            }
        }

        for (int i = 0; i < NUM_VARS; i++) {
            for (int j = 0; j < NUM_VALUES; j++) {
                pheromones[i][j] = beta * newPheromones[i][j] / NUM_ANTS + (1 - beta) * pheromones[i][j];
            }
        }

        total += bestE;
        cpt++;

        if (cpt % 10 == 0) {
            printf("%.2f\t%.2f\n", round(total / cpt * 100) / 100, bestE);
        }
    }
}


int main() {
    srand(time(NULL));
    antColony(100.0, 0.1, 2.0, 0.03, 0.5, 100);


    return 0;
}
