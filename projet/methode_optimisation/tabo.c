#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define TAILLE_TABLEAU 10

int energie(int* etat) {
    // Fonction d'énergie à minimiser
    int nbErreurs = 0;
    int tabCompteur[10] = {0};

    for (int i = 0; i < TAILLE_TABLEAU; i++) {
        int num = etat[i];
        while (num > 0) {
            tabCompteur[num % 10]++;
            num /= 10;
        }
    }

    for (int i = 0; i < TAILLE_TABLEAU; i++) {
        int num = etat[i];
        while (num > 0) {
            if (tabCompteur[num % 10] != i + 1)
                nbErreurs++;
            num /= 10;
        }
    }

    return nbErreurs;
}

void genererEtat(int* etat) {
    // Fonction pour générer un nouvel état
    for (int i = 0; i < TAILLE_TABLEAU; i++) {
        etat[i] = rand() % 14;
    }
}

double probabiliteAcceptation(int deltaE, double T) {
    // Fonction pour calculer la probabilité d'accepter une transition
    return exp(-deltaE / T);
}

void recuitSimule(double temperatureInitiale, int longueurPhase, int nbAcceptations, int (*energie)(int*)) {
    double T = temperatureInitiale;
    double toleranceTemperature = 0.0001;
    int etat[TAILLE_TABLEAU];
    int meilleurEtat[TAILLE_TABLEAU];
    int meilleureEnergie = INT_MAX;
    int nbAcceptationsCourantes = 0;
    int i = 0;
    int listeTabou[TAILLE_TABLEAU];
    int tailleTabou = TAILLE_TABLEAU;
    int indexTabou = 0;

    srand(time(NULL));

    genererEtat(etat);
    for (int i = 0; i < TAILLE_TABLEAU; i++) {
        meilleurEtat[i] = etat[i];
    }
    meilleureEnergie = energie(etat);

    while (T > toleranceTemperature) {
        int nouvelEtat[TAILLE_TABLEAU];
        genererEtat(nouvelEtat);

        int E = energie(etat);
        int E_nouvelEtat = energie(nouvelEtat);
        int deltaE = E_nouvelEtat - E;

        // Vérifier si le nouvel état est tabou ou si l'énergie est supérieure à la meilleure énergie
        if (deltaE >= 0 || listeTabou[nouvelEtat[0]] == i + 1) {
            for (int i = 0; i < TAILLE_TABLEAU; i++) {
                etat[i] = nouvelEtat[i];
            }
            nbAcceptationsCourantes++;

            if (E_nouvelEtat < meilleureEnergie) {
                meilleureEnergie = E_nouvelEtat;
                for (int i = 0; i < TAILLE_TABLEAU; i++) {
                    meilleurEtat[i] = etat[i];
                }
            }
        }

        if (nbAcceptationsCourantes == nbAcceptations || i % longueurPhase == 0) {
            printf("Iteration: %d\tTemperature: %.2f\tMeilleure Energie: %d\n", i, T, meilleureEnergie);
            nbAcceptationsCourantes = 0;
        }

        // Mettre à jour la liste tabou
        listeTabou[etat[indexTabou]] = i + tailleTabou;

        T *= 0.9994;
        i++;
    }

    printf("\nMeilleur Etat:\n");
    for (int i = 0; i < TAILLE_TABLEAU; i++) {
        printf("%d ", meilleurEtat[i]);
    }
    printf("\nMeilleure Energie: %d\n", meilleureEnergie);
}

int main() {
    double temperatureInitiale = 30;
    int longueurPhase = 1000;
    int nbAcceptations = 100;

    recuitSimule(temperatureInitiale, longueurPhase, nbAcceptations, energie);

    return 0;
}
