#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define ARRAY_SIZE 10

#define ALPHA 1.0           // Coefficient d'influence des phéromones
#define BETA 2.0            // Coefficient d'influence des qualités
#define EVAPORATION 0.5     // Taux d'évaporation des phéromones
#define Q 100               // Quantité de phéromones déposée
#define MAX_ITERATIONS 100

double distances[ARRAY_SIZE][ARRAY_SIZE];   // Matrice des distances entre les positions

double pheromones[ARRAY_SIZE][ARRAY_SIZE];  // Matrice des phéromones
double bestTour[ARRAY_SIZE];                // Meilleur chemin trouvé
double bestTourLength;                      // Longueur du meilleur chemin trouvé

double getRandomDouble() {
    return (double)rand() / (double)RAND_MAX;
}

double computeTourLength(int* tour) {
    double length = 0.0;
    for (int i = 0; i < ARRAY_SIZE - 1; i++) {
        int pos1 = tour[i];
        int pos2 = tour[i + 1];
        length += distances[pos1][pos2];
    }
    return length;
}

int selectNextPosition(int* visited, int current) {
    double total = 0.0;
    double probabilities[ARRAY_SIZE];
    int unvisitedCount = 0;

    for (int i = 0; i < ARRAY_SIZE; i++) {
        if (!visited[i]) {
            probabilities[i] = pow(pheromones[current][i], ALPHA) * pow(1.0 / distances[current][i], BETA);
            total += probabilities[i];
            unvisitedCount++;
        } else {
            probabilities[i] = 0.0;
        }
    }

    if (unvisitedCount == 0) {
        return -1; // Toutes les positions ont été visitées
    }

    double p = getRandomDouble();
    double cumulative = 0.0;
    for (int i = 0; i < ARRAY_SIZE; i++) {
        if (!visited[i]) {
            cumulative += probabilities[i] / total;
            if (p <= cumulative) {
                return i;
            }
        }
    }

    // En cas de problème, retourner la première position non visitée
    for (int i = 0; i < ARRAY_SIZE; i++) {
        if (!visited[i]) {
            return i;
        }
    }

    return -1;
}

void updatePheromones() {
    for (int i = 0; i < ARRAY_SIZE; i++) {
        for (int j = 0; j < ARRAY_SIZE; j++) {
            pheromones[i][j] *= (1.0 - EVAPORATION);
        }
    }

    for (int i = 0; i < ARRAY_SIZE - 1; i++) {
        int pos1 = bestTour[i];
        int pos2 = bestTour[i + 1];
        pheromones[pos1][pos2] += Q / bestTourLength;
        pheromones[pos2][pos1] += Q / bestTourLength;
    }
}

void antColonyOptimization() {
    int iterations = 0;
    while (iterations < MAX_ITERATIONS) {
        int toursCount = 0;
        double totalTourLength = 0.0;

        while (toursCount < ARRAY_SIZE) {
            int tour[ARRAY_SIZE];
            int visited[ARRAY_SIZE] = {0};
            int currentPosition = rand() % ARRAY_SIZE;

            tour[0] = currentPosition;
            visited[currentPosition] = 1;

            int nextPosition;
            for (int i = 1; i < ARRAY_SIZE; i++) {
                nextPosition = selectNextPosition(visited, currentPosition);
                if (nextPosition == -1) {
                    break;
                }
                tour[i] = nextPosition;
                visited[nextPosition] = 1;
                currentPosition = nextPosition;
            }

            double tourLength = computeTourLength(tour);
            totalTourLength += tourLength;

            if (toursCount == 0 || tourLength < bestTourLength) {
                bestTourLength = tourLength;
                for (int i = 0; i < ARRAY_SIZE; i++) {
                    bestTour[i] = tour[i];
                }
            }

            toursCount++;
        }

        updatePheromones();

        iterations++;
    }
}

int main() {
    srand(time(NULL));

    // Initialisation des distances entre les positions (peut être adapté à votre problème spécifique)
    for (int i = 0; i < ARRAY_SIZE; i++) {
        for (int j = 0; j < ARRAY_SIZE; j++) {
            distances[i][j] = abs(i - j); // Exemple : distance euclidienne entre les positions i et j
        }
    }

    bestTourLength = -1.0; // Initialisation de la longueur du meilleur chemin

    antColonyOptimization();

    printf("Meilleur chemin : ");
    for (int i = 0; i < ARRAY_SIZE; i++) {
        printf("%d ", (int)bestTour[i]);
    }
    printf("\n");
    printf("Longueur du meilleur chemin : %.2f\n", bestTourLength);

    return 0;
}
