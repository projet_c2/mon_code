#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define ARRAY_SIZE 10
/*
int energy1(int* state);
int* generateState(int* state);
int* generateStateSimulatedAnnealing(int* state, double T, int (*energy)(int*));
int simulatedAnnealing(double initialTemperature, double coolingRate, int numIterations, int (*energy)(int*));
*/

int energy1(int* state) {
    int nbErrors = 0;
    int tabCount[10] = {0};
    for (int i = 0; i < ARRAY_SIZE; i++) {
        int value = state[i];
        while (value > 0) {
            tabCount[value % 10]++;
            value /= 10;
        }
    }

    for (int i = 0; i < ARRAY_SIZE; i++) {
        nbErrors += abs(state[i] - tabCount[i]);
    }
    return nbErrors;
}

int* generateState() {
    int* newState = (int*)malloc(ARRAY_SIZE * sizeof(int));
    for (int i = 0; i < ARRAY_SIZE; i++) {
        newState[i] = rand() % 14 + 1;
    }
    int pos = rand() % ARRAY_SIZE;
    newState[pos] = rand() % 14 + 1;
    return newState;
}


int* generateStateSimulatedAnnealing(int* state, double T, int (*energy)(int*)) {
    int* newState = generateState(state);
    int E_newState = energy(newState);
    int E = energy(state);
    double deltaE = E_newState - E;
    
    if (deltaE <= 0) {
        return newState;
    } else {
        double p = exp(-deltaE / T);
        if ((double)rand() / RAND_MAX < p) {
            return newState;
        } else {
            free(newState);
            return state;
        }
    }
}

int simulatedAnnealing(double initialTemperature, double coolingRate, int numIterations, int (*energy)(int*)) {
    double T = initialTemperature;
    int* state = (int*)malloc(ARRAY_SIZE * sizeof(int));
    for (int i = 0; i < ARRAY_SIZE; i++) {
        state[i] = rand() % 14;
    }

    for (int i = 0; i < numIterations; i++) {
        int* newState = generateStateSimulatedAnnealing(state, T, energy);
        if (newState != state) {
            free(state);
            state = newState;
        }
        T *= coolingRate;
    }

    printf("Final State: ");
    for (int i = 0; i < ARRAY_SIZE; i++) {
        printf("%d ", state[i]);
    }
    printf("\n");

    int finalEnergy = energy(state);
    free(state);

    return finalEnergy;
}

int main() {
    srand(time(NULL));
    double initialTemperature = 30.0;
    double coolingRate = 0.9994;
    int numIterations = 10000;
    int finalEnergy = simulatedAnnealing(initialTemperature, coolingRate, numIterations, energy1);

    printf("Final Energy: %d\n", finalEnergy);

    return 0;
}
