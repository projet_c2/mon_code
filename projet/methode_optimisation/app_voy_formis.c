#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <SDL2/SDL.h>

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600
#define CITY_RADIUS 6
#define ANT_SIZE 4
#define MAX_ANTS 50
#define EVAPORATION_RATE 0.5
#define ALPHA 1.0
#define BETA 2.0

typedef struct {
    int x;
    int y;
} City;

typedef struct {
    int city;
    double tourLength;
    int *visited;
} Ant;

City *cities;
Ant *ants;
int numCities;
double **pheromones;
double **distances;
int *bestTour;
double bestTourLength;

SDL_Window *window = NULL;
SDL_Renderer *renderer = NULL;

void initialize();
void cleanup();
void generateCities();
double calculateDistance(int city1, int city2);
void initializeAnts();
void resetAnts();
void constructSolutions();
void updatePheromones();
void evaporatePheromones();
void render();

int main() {
    srand(time(NULL));

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("SDL initialization failed: %s\n", SDL_GetError());
        return 1;
    }

    window = SDL_CreateWindow("TSP Animation", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (!window) {
        printf("Window creation failed: %s\n", SDL_GetError());
        SDL_Quit();
        return 1;
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (!renderer) {
        printf("Renderer creation failed: %s\n", SDL_GetError());
        SDL_DestroyWindow(window);
        SDL_Quit();
        return 1;
    }

    initialize();

    int quit = 0;
    while (!quit) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                quit = 1;
            }
        }

        resetAnts();
        constructSolutions();
        updatePheromones();
        evaporatePheromones();
        render();
    }

    cleanup();

    return 0;
}

void initialize() {
    numCities = 10; // Nombre de villes

    cities = (City*)malloc(sizeof(City) * numCities);
    ants = (Ant*)malloc(sizeof(Ant) * MAX_ANTS);
    pheromones = (double**)malloc(sizeof(double*) * numCities);
    distances = (double**)malloc(sizeof(double*) * numCities);

    for (int i = 0; i < numCities; i++) {
        pheromones[i] = (double*)malloc(sizeof(double) * numCities);
        distances[i] = (double*)malloc(sizeof(double) * numCities);
        for (int j = 0; j < numCities; j++) {
            pheromones[i][j] = 1.0; // Initialiser les niveaux de phéromones à 1.0
            distances[i][j] = 0.0;
        }
    }

    generateCities();
    initializeAnts();

    bestTourLength = INFINITY;
    bestTour = (int*)malloc(sizeof(int) * numCities);
}

void cleanup() {
    free(cities);
    free(ants);
    for (int i = 0; i < numCities; i++) {
        free(pheromones[i]);
        free(distances[i]);
    }
    free(pheromones);
    free(distances);
    free(bestTour);

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void generateCities() {
    for (int i = 0; i < numCities; i++) {
        cities[i].x = rand() % (SCREEN_WIDTH - CITY_RADIUS * 2) + CITY_RADIUS;
        cities[i].y = rand() % (SCREEN_HEIGHT - CITY_RADIUS * 2) + CITY_RADIUS;
    }
}

double calculateDistance(int city1, int city2) {
    int xDiff = cities[city1].x - cities[city2].x;
    int yDiff = cities[city1].y - cities[city2].y;
    return sqrt(xDiff * xDiff + yDiff * yDiff);
}

void initializeAnts() {
    for (int i = 0; i < MAX_ANTS; i++) {
        ants[i].visited = (int*)malloc(sizeof(int) * numCities);
        for (int j = 0; j < numCities; j++) {
            ants[i].visited[j] = 0;
        }
    }
}

void resetAnts() {
    for (int i = 0; i < MAX_ANTS; i++) {
        ants[i].city = rand() % numCities;
        ants[i].tourLength = 0.0;
        for (int j = 0; j < numCities; j++) {
            ants[i].visited[j] = 0;
        }
        ants[i].visited[ants[i].city] = 1;
    }
}

void constructSolutions() {
    for (int i = 0; i < numCities - 1; i++) {
        for (int j = 0; j < MAX_ANTS; j++) {
            double  *probabilities = (double *)malloc(sizeof(double) * numCities);
            double sum = 0.0;
            for (int k = 0; k < numCities; k++) {
                if (ants[j].visited[k] == 0) {
                    double pheromone = pow(pheromones[ants[j].city][k], ALPHA);
                    double distance = pow(1.0 / distances[ants[j].city][k], BETA);
                    probabilities[k] = pheromone * distance;
                    sum += probabilities[k];
                }
            }
            for (int k = 0; k < numCities; k++) {
                probabilities[k] /= sum;
            }

            double r = (double)rand() / RAND_MAX;
            double total = 0.0;
            int nextCity = -1;
            for (int k = 0; k < numCities; k++) {
                total += probabilities[k];
                if (total > r) {
                    nextCity = k;
                    break;
                }
            }

            if (nextCity == -1) {
                for (int k = 0; k < numCities; k++) {
                    if (ants[j].visited[k] == 0) {
                        nextCity = k;
                        break;
                    }
                }
            }

            ants[j].visited[nextCity] = 1;
            ants[j].tourLength += distances[ants[j].city][nextCity];
            ants[j].city = nextCity;

            free(probabilities);
        }
    }

    for (int i = 0; i < MAX_ANTS; i++) {
        ants[i].tourLength += distances[ants[i].city][0];
        if (ants[i].tourLength < bestTourLength) {
            bestTourLength = ants[i].tourLength;
            for (int j = 0; j < numCities; j++) {
                bestTour[j] = ants[i].visited[j];
            }
        }
    }
}

void updatePheromones() {
    for (int i = 0; i < numCities; i++) {
        for (int j = 0; j < numCities; j++) {
            pheromones[i][j] *= EVAPORATION_RATE;
        }
    }

    for (int i = 0; i < numCities - 1; i++) {
        int city1 = bestTour[i];
        int city2 = bestTour[i + 1];
        pheromones[city1][city2] += 1.0 / bestTourLength;
        pheromones[city2][city1] += 1.0 / bestTourLength;
    }
}

void evaporatePheromones() {
    for (int i = 0; i < numCities; i++) {
        for (int j = 0; j < numCities; j++) {
            pheromones[i][j] *= (1.0 - EVAPORATION_RATE);
        }
    }
}

void render() {
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    for (int i = 0; i < numCities; i++) {
        SDL_Rect rect = { cities[i].x - CITY_RADIUS, cities[i].y - CITY_RADIUS, CITY_RADIUS * 2, CITY_RADIUS * 2 };
        SDL_RenderFillRect(renderer, &rect);
    }

    for (int i = 0; i < MAX_ANTS; i++) {
        SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
        for (int j = 0; j < numCities - 1; j++) {
            int city1 = ants[i].visited[j];
            int city2 = ants[i].visited[j + 1];
            SDL_RenderDrawLine(renderer, cities[city1].x, cities[city1].y, cities[city2].x, cities[city2].y);
        }
        int lastCity = ants[i].visited[numCities - 1];
        int firstCity = ants[i].visited[0];
        SDL_RenderDrawLine(renderer, cities[lastCity].x, cities[lastCity].y, cities[firstCity].x, cities[firstCity].y);

        SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
        SDL_Rect rect = { cities[ants[i].city].x - ANT_SIZE / 2, cities[ants[i].city].y - ANT_SIZE / 2, ANT_SIZE, ANT_SIZE };
        SDL_RenderFillRect(renderer, &rect);
    }

    SDL_RenderPresent(renderer);
}