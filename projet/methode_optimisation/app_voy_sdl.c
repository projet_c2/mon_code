#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <time.h>


#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600
#define NUM_CITIES 50
#define NUM_ANTS 50
#define ALPHA 1.0
#define BETA 5.0
#define RHO 0.5
#define Q 100
#define MAX_ITERATIONS 1000

typedef struct {
    int x;
    int y;
} City;

typedef struct {
    int path[NUM_CITIES];
    double tourLength;
} Ant;

City cities[NUM_CITIES];
Ant ants[NUM_ANTS];
int distances[NUM_CITIES][NUM_CITIES];
double pheromones[NUM_CITIES][NUM_CITIES];
int bestTour[NUM_CITIES];
double bestTourLength = INFINITY;
SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;
SDL_Texture* cityTexture = NULL;
SDL_Texture* antTexture = NULL;

int visited(int *path, int city);


void initSDL() {
    SDL_Init(SDL_INIT_VIDEO);
    IMG_Init(IMG_INIT_PNG);
    window = SDL_CreateWindow("Traveling Salesman Problem", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    cityTexture = IMG_LoadTexture(renderer, "city.png");
    antTexture = IMG_LoadTexture(renderer, "ant.png");
}

void cleanupSDL() {
    SDL_DestroyTexture(cityTexture);
    SDL_DestroyTexture(antTexture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    IMG_Quit();
    SDL_Quit();
}

void generateCities() {
    for (int i = 0; i < NUM_CITIES; i++) {
        cities[i].x = rand() % SCREEN_WIDTH;
        cities[i].y = rand() % SCREEN_HEIGHT;
    }
}

void calculateDistances() {
    for (int i = 0; i < NUM_CITIES; i++) {
        for (int j = 0; j < NUM_CITIES; j++) {
            int dx = cities[i].x - cities[j].x;
            int dy = cities[i].y - cities[j].y;
            distances[i][j] = round(sqrt(dx*dx + dy*dy));
        }
    }
}

void initializePheromones() {
    for (int i = 0; i < NUM_CITIES; i++) {
        for (int j = 0; j < NUM_CITIES; j++) {
            pheromones[i][j] = 1.0;
        }
    }
}

void initializeAnts() {
    for (int i = 0; i < NUM_ANTS; i++) {
        for (int j = 0; j < NUM_CITIES; j++) {
            ants[i].path[j] = j;
        }
        ants[i].tourLength = 0.0;
    }
}

double calculateProbability(int from, int to, int ant) {
    double numerator = pow(pheromones[from][to], ALPHA) * pow(1.0 / distances[from][to], BETA);
    double denominator = 0.0;
    for (int i = 0; i < NUM_CITIES; i++) {
        if (i != from && !visited(ants[ant].path, i)) {
            denominator += pow(pheromones[from][i], ALPHA) * pow(1.0 / distances[from][i], BETA);
        }
    }
    return numerator / denominator;
}

int visited(int* path, int city) {
    for (int i = 0; i < NUM_CITIES; i++) {
        if (path[i] == city) {
            return 1;
        }
    }
    return 0;
}

int selectNextCity(int ant) {
    int from = ants[ant].path[NUM_CITIES - 1];
    double probabilitySum = 0.0;
    for (int i = 0; i < NUM_CITIES; i++) {
        if (!visited(ants[ant].path, i)) {
            probabilitySum += calculateProbability(from, i, ant);
        }
    }
    double r = (double)rand() / RAND_MAX;
    double p = 0.0;
    for (int i = 0; i < NUM_CITIES; i++) {
        if (!visited(ants[ant].path, i)) {
            p += calculateProbability(from, i, ant) / probabilitySum;
            if (r <= p) {
                return i;
            }
        }
    }
    return -1;
}

void constructSolutions() {
    for (int i = 0; i < NUM_ANTS; i++) {
        initializeAnts();
        for (int j = 0; j < NUM_CITIES - 1; j++) {
            int nextCity = selectNextCity(i);
            ants[i].path[j + 1] = nextCity;
            ants[i].tourLength += distances[ants[i].path[j]][nextCity];
        }
        ants[i].tourLength += distances[ants[i].path[NUM_CITIES - 1]][ants[i].path[0]];
        if (ants[i].tourLength < bestTourLength) {
            bestTourLength = ants[i].tourLength;
            memcpy(bestTour, ants[i].path, sizeof(int) * NUM_CITIES);
        }
    }
}

void updatePheromones() {
    for (int i = 0; i < NUM_CITIES; i++) {
        for (int j = 0; j < NUM_CITIES; j++) {
            pheromones[i][j] *= (1.0 - RHO);
        }
    }
    for (int i = 0; i < NUM_ANTS; i++) {
        for (int j = 0; j < NUM_CITIES - 1; j++) {
            int from = ants[i].path[j];
            int to = ants[i].path[j + 1];
            pheromones[from][to] += Q / ants[i].tourLength;
            pheromones[to][from] = pheromones[from][to];
        }
        int from = ants[i].path[NUM_CITIES - 1];
        int to = ants[i].path[0];
        pheromones[from][to] += Q / ants[i].tourLength;
        pheromones[to][from] = pheromones[from][to];
    }
}

void drawCity(int x, int y) {
    SDL_Rect rect = { x - 5, y - 5, 100, 100 };
    SDL_RenderCopy(renderer, cityTexture, NULL, &rect);
}

void drawAnt(int x, int y) {
    SDL_Rect rect = { x - 3, y - 3, 60, 60 };
    SDL_RenderCopy(renderer, antTexture, NULL, &rect);
}

void render() {
    SDL_RenderClear(renderer);

    // Draw cities
    for (int i = 0; i < NUM_CITIES; i++) {
        drawCity(cities[i].x, cities[i].y);
    }

    // Draw ants
    for (int i = 0; i < NUM_CITIES; i++) {
        drawAnt(cities[bestTour[i]].x, cities[bestTour[i]].y);
    }

    SDL_RenderPresent(renderer);
}

int main() {
    srand(time(NULL));

    initSDL();

    generateCities();
    calculateDistances();
    initializePheromones();

    int quit = 0;
    SDL_Event e;

    while (!quit) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = 1;
            } else if (e.type == SDL_KEYDOWN) {
                if (e.key.keysym.sym == SDLK_PLUS || e.key.keysym.sym == SDLK_KP_PLUS) {
                    for (int i = 0; i < NUM_CITIES; i++) {
                        cities[i].x *= 1.2;
                        cities[i].y *= 1.2;
                    }
                } else if (e.key.keysym.sym == SDLK_MINUS || e.key.keysym.sym == SDLK_KP_MINUS) {
                    for (int i = 0; i < NUM_CITIES; i++) {
                        cities[i].x /= 1.2;
                        cities[i].y /= 1.2;
                    }
                }
            }
        }

        constructSolutions();
        updatePheromones();
        render();
    }

    cleanupSDL();

    return 0;
}
