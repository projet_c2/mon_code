#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include <SDL2/SDL.h>

#define NUM_CITIES 5
#define MAX_DISTANCE 9999
#define NUM_ANTS 10
#define EVAPORATION_RATE 0.1
#define ALPHA 1.0
#define BETA 2.0



// Matrice des distances entre les villes
int distances[NUM_CITIES][NUM_CITIES] = {
    {0, 2, 9, 10, 4},
    {2, 0, 6, 4, 2},
    {9, 6, 0, 8, 7},
    {10, 4, 8, 0, 3},
    {4, 2, 7, 3, 0}
};

// Matrice des phéromones entre les villes
double pheromones[NUM_CITIES][NUM_CITIES];

// Fenêtre SDL
SDL_Window* window = NULL;

// Renderer SDL
SDL_Renderer* renderer = NULL;
int screenWidth = 800;
int screenHeight = 900;


// Couleurs
SDL_Color colorCities = {0, 0, 255, 255};
SDL_Color colorPheromones = {255, 0, 0, 255};

// Structure de coordonnées pour une ville
typedef struct {
    int x;
    int y;
} City;

// Tableau des coordonnées des villes
City cities[NUM_CITIES] = {
    {50, 50},
    {50, 600},
    {600, 50},
    {600, 800},
    {400, 300}
};

// Déclaration de la fonction calculateTotalCost
int calculateTotalCost(int path[NUM_CITIES]);

// Fonction pour initialiser les phéromones avec une valeur initiale
void initializePheromones() {
    for (int i = 0; i < NUM_CITIES; i++) {
        for (int j = 0; j < NUM_CITIES; j++) {
            pheromones[i][j] = 1.0;
        }
    }
}

// Fonction pour calculer la probabilité de choisir la prochaine ville
double calculateProbability(int currentCity, int nextCity, bool visited[NUM_CITIES]) {
    double numerator = pheromones[currentCity][nextCity] * pow(1.0 / distances[currentCity][nextCity], BETA);
    double denominator = 0.0;

    for (int i = 0; i < NUM_CITIES; i++) {
        if (!visited[i]) {
            denominator += pheromones[currentCity][i] * pow(1.0 / distances[currentCity][i], BETA);
        }
    }

    return numerator / denominator;
}

// Fonction pour choisir la prochaine ville à visiter en utilisant la méthode gloutonne et les phéromones
int getNextCity(int currentCity, bool visited[NUM_CITIES]) {
    double probabilities[NUM_CITIES];
    double totalProbability = 0.0;

    for (int i = 0; i < NUM_CITIES; i++) {
        if (!visited[i]) {
            probabilities[i] = calculateProbability(currentCity, i, visited);
            totalProbability += probabilities[i];
        } else {
            probabilities[i] = 0.0;
        }
    }

    // Choisir aléatoirement une ville en fonction des probabilités
    double randomValue = (double)rand() / RAND_MAX;
    double cumulativeProbability = 0.0;

    for (int i = 0; i < NUM_CITIES; i++) {
        cumulativeProbability += probabilities[i] / totalProbability;
        if (randomValue <= cumulativeProbability) {
            return i;
        }
    }

    // Retourner la dernière ville non visitée (cas de probabilités nulles)
    for (int i = 0; i < NUM_CITIES; i++) {
        if (!visited[i]) {
            return i;
        }
    }

    return -1; // En cas d'erreur
}

// Fonction pour mettre à jour les phéromones en fonction des solutions des fourmis
void updatePheromones(int antPaths[NUM_ANTS][NUM_CITIES]) {
    for (int i = 0; i < NUM_CITIES; i++) {
        for (int j = 0; j < NUM_CITIES; j++) {
            pheromones[i][j] *= (1.0 - EVAPORATION_RATE);
        }
    }

    for (int i = 0; i < NUM_ANTS; i++) {
        double antPheromones = 1.0 / calculateTotalCost(antPaths[i]);
        for (int j = 0; j < NUM_CITIES - 1; j++) {
            pheromones[antPaths[i][j]][antPaths[i][j + 1]] += antPheromones;
        }
        pheromones[antPaths[i][NUM_CITIES - 1]][antPaths[i][0]] += antPheromones;
    }
}

// Fonction pour calculer le coût total du parcours
int calculateTotalCost(int path[NUM_CITIES]) {
    int totalCost = 0;

    for (int i = 0; i < NUM_CITIES - 1; i++) {
        totalCost += distances[path[i]][path[i + 1]];
    }

    // Ajouter le coût du retour à la ville de départ
    totalCost += distances[path[NUM_CITIES - 1]][path[0]];

    return totalCost;
}

// Fonction pour dessiner les villes et les phéromones
void drawCitiesAndPheromones() {
    SDL_SetRenderDrawColor(renderer, colorCities.r, colorCities.g, colorCities.b, colorCities.a);

    for (int i = 0; i < NUM_CITIES; i++) {
        SDL_Rect rect = {cities[i].x - 5, cities[i].y - 5, 20, 20};
        SDL_RenderFillRect(renderer, &rect);
    }

    SDL_SetRenderDrawColor(renderer, colorPheromones.r, colorPheromones.g, colorPheromones.b, colorPheromones.a);

    for (int i = 0; i < NUM_CITIES; i++) {
        for (int j = 0; j < NUM_CITIES; j++) {
            if (i != j) {
                double thickness = pheromones[i][j] * 3.0;
                thickness = thickness < 1.0 ? 1.0 : thickness;
                thickness = thickness > 3.0 ? 3.0 : thickness;

                SDL_RenderDrawLine(renderer, cities[i].x, cities[i].y, cities[j].x, cities[j].y);
                //SDL_RenderDrawLine(renderer, cities[i].x, cities[i].y, cities[j].x, cities[j].y);
            }
        }
    }

    SDL_RenderPresent(renderer);
}

// Fonction pour afficher les chemins parcourus par les fourmis
void drawAntPaths(int antPaths[NUM_ANTS][NUM_CITIES]) {
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);

    drawCitiesAndPheromones();

    SDL_SetRenderDrawColor(renderer, 255, 255, 0, 255);

    for (int i = 0; i < NUM_ANTS; i++) {
        for (int j = i+1; j < NUM_CITIES - 1; j++) {
            SDL_RenderDrawLine(renderer, cities[antPaths[i][j]].x, cities[antPaths[i][j]].y, cities[antPaths[i][j + 1]].x, cities[antPaths[i][j + 1]].y);
        }
        SDL_RenderDrawLine(renderer, cities[antPaths[i][NUM_CITIES - 1]].x, cities[antPaths[i][NUM_CITIES - 1]].y, cities[antPaths[i][0]].x, cities[antPaths[i][0]].y);
    }

    SDL_RenderPresent(renderer);
}

int main() {
    // Initialisation de SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Erreur lors de l'initialisation de SDL : %s\n", SDL_GetError());
        return 1;
    }

    // Création de la fenêtre SDL
        window = SDL_CreateWindow("Colonies de fourmis - Voyageur de commerce", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, screenWidth, screenHeight, SDL_WINDOW_SHOWN);
    if (window == NULL) {
        printf("Erreur lors de la création de la fenêtre : %s\n", SDL_GetError());
        return 1;
    }

    // Création du renderer SDL
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) {
        printf("Erreur lors de la création du renderer : %s\n", SDL_GetError());
        return 1;
    }

    // Initialisation du générateur de nombres aléatoires
    srand(time(NULL));

    // Initialisation des phéromones
    initializePheromones();

    // Tableau pour stocker les chemins parcourus par les fourmis
    int antPaths[NUM_ANTS][NUM_CITIES];

    // Boucle principale
    bool quit = false;
    SDL_Event event;

    while (!quit) {
        // Génération des chemins des fourmis
        for (int i = 0; i < NUM_ANTS; i++) {
            bool visited[NUM_CITIES] = {false};
            int currentCity = 0;//rand() % NUM_CITIES;

            for (int j = 0; j < NUM_CITIES; j++) {
                antPaths[i][j] = currentCity;
                visited[currentCity] = true;

                int nextCity = getNextCity(currentCity, visited);
                currentCity = nextCity;
            }
        }
        

        // Mise à jour des phéromones
        updatePheromones(antPaths);

        // Dessiner les chemins parcourus par les fourmis
        drawAntPaths(antPaths);

        // Gestion des événements SDL
        while (SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) {
                quit = true;
            }
        }

        // Pause pour ralentir l'animation
        SDL_Delay(10000);


        quit=true;
    }

    // Libérer les ressources SDL
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
