#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

void afficherPhrase(int* L, int taille) {
    printf("Cette phrase contient :\n");
    for (int i = 0; i < taille; i++) {
        printf("\t- %d chiffres %d\n", L[i], i);
    }
}

int compterOccurrences(int* L, int taille, int chiffre) {
    int count = 0;
    for (int i = 0; i < taille; i++) {
        int v = L[i];
        while (v > 0) {
            int d = v % 10;
            if (d == chiffre) {
                count++;
            }
            v /= 10;
        }
    }
    return count;
}

int calculerErreurs(int* L, int taille) {
    int totalErreurs = 0;
    for (int chiffre = 0; chiffre < 10; chiffre++) {
        int occurrences = compterOccurrences(L, taille, chiffre);
        totalErreurs += abs(L[chiffre] - occurrences);
    }
    return totalErreurs;
}

int* genererEtat(int* L, int taille, int (*energie)(int*, int)) {
    int* M = malloc(taille * sizeof(int));
    memcpy(M, L, taille * sizeof(int));

    int meilleurE = energie(L, taille);

    for (int i = 0; i < taille; i++) {
        int meilleurChiffre = M[i];
        int meilleurErreurs = meilleurE;

        for (int chiffre = 0; chiffre <= 9; chiffre++) {
            M[i] = chiffre;
            int E = energie(M, taille);
            if (E < meilleurErreurs) {
                meilleurErreurs = E;
                meilleurChiffre = chiffre;
            }
        }

        M[i] = meilleurChiffre;
    }

    return M;
}

int main() {
    srand(time(NULL));

    int L[] = {0,1111, 2222, 333, 444, 555, 6, 77777, 88, 99}; // Exemple de liste L
    int taille = sizeof(L) / sizeof(L[0]);
    printf("%d\n", taille);

    //afficherPhrase(L, taille);

    int erreurs = calculerErreurs(L, taille);
    printf("Nombre d'erreurs initial : %d\n", erreurs);

    int* nouvelEtat = genererEtat(L, taille, calculerErreurs);
    printf("Nouvel état : ");
    for (int i = 0; i < taille; i++) {
        printf("%d ", nouvelEtat[i]);
    }
    printf("\n");

    free(nouvelEtat);

    return 0;
}
