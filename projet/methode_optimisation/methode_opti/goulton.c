#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

#define NUM_CITIES 5
#define MAX_DISTANCE 9999

// Matrice des distances entre les villes
int distances[NUM_CITIES][NUM_CITIES] = {
    {0, 2, 9, 10, 4},
    {2, 0, 6, 4, 5},
    {9, 6, 0, 8, 7},
    {10, 4, 8, 0, 3},
    {4, 2, 7, 3, 0}
};

// Tableau pour marquer les villes déjà visitées
bool visited[NUM_CITIES] = {false};

// Fonction pour trouver la prochaine ville à visiter
int getNextCity(int currentCity) {
    int minDistance = MAX_DISTANCE;
    int nextCity = -1;

    for (int i = 0; i < NUM_CITIES; i++) {
        if (!visited[i] && distances[currentCity][i] < minDistance) {
            minDistance = distances[currentCity][i];
            nextCity = i;
        }
    }

    return nextCity;
}

// Fonction pour calculer le coût total du parcours
int calculateTotalCost(int path[NUM_CITIES]) {
    int totalCost = 0;

    for (int i = 0; i < NUM_CITIES - 1; i++) {
        totalCost += distances[path[i]][path[i + 1]];
    }

    // Ajouter le coût du retour à la ville de départ
    totalCost += distances[path[NUM_CITIES - 1]][path[0]];

    return totalCost;
}

// Fonction pour effectuer l'algorithme glouton
void greedyAlgorithm() {
    int currentCity = rand()%NUM_CITIES;
    int path[NUM_CITIES];
    int pathIndex = 0;

    visited[currentCity] = true;
    path[pathIndex++] = currentCity;

    while (pathIndex < NUM_CITIES) {
        int nextCity = getNextCity(currentCity);

        visited[nextCity] = true;
        path[pathIndex++] = nextCity;
        currentCity = nextCity;
    }

    // Ajouter la ville de départ à la fin du parcours
    path[pathIndex] = 0;

    int totalCost = calculateTotalCost(path);

    printf("Parcours : ");
    for (int i = 0; i < NUM_CITIES; i++) {
        printf("%c ", 'a' + path[i]);
    }
    printf("\nCoût total : %d\n", totalCost);
}

int main() {
    srand(time(NULL));
    greedyAlgorithm();

    return 0;
}
