#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>

#define NUM_CITIES 5
#define MAX_DISTANCE 9999
#define NUM_ANTS 10
#define EVAPORATION_RATE 0.1
#define ALPHA 1.0
#define BETA 2.0

// Matrice des distances entre les villes
int distances[NUM_CITIES][NUM_CITIES] = {
    {0, 2, 9, 10, 4},
    {2, 0, 6, 4, 2},
    {9, 6, 0, 8, 7},
    {10, 4, 8, 0, 3},
    {4, 2, 7, 3, 0}
};


// Matrice des phéromones entre les villes
double pheromones[NUM_CITIES][NUM_CITIES];

// Déclaration de la fonction calculateTotalCost
int calculateTotalCost(int path[NUM_CITIES]);

// Fonction pour initialiser les phéromones avec une valeur initiale
void initializePheromones() {
    for (int i = 0; i < NUM_CITIES; i++) {
        for (int j = 0; j < NUM_CITIES; j++) {
            pheromones[i][j] = 1.0;
        }
    }
}

// Fonction pour calculer la probabilité de choisir la prochaine ville
double calculateProbability(int currentCity, int nextCity, bool visited[NUM_CITIES]) {
    double numerator = pheromones[currentCity][nextCity] * pow(1.0 / distances[currentCity][nextCity], BETA);
    double denominator = 0.0;

    for (int i = 0; i < NUM_CITIES; i++) {
        if (!visited[i]) {
            denominator += pheromones[currentCity][i] * pow(1.0 / distances[currentCity][i], BETA);
        }
    }

    return numerator / denominator;
}

// Fonction pour choisir la prochaine ville à visiter en utilisant la méthode gloutonne et les phéromones
int getNextCity(int currentCity, bool visited[NUM_CITIES]) {
    double probabilities[NUM_CITIES];
    double totalProbability = 0.0;

    for (int i = 0; i < NUM_CITIES; i++) {
        if (!visited[i]) {
            probabilities[i] = calculateProbability(currentCity, i, visited);
            totalProbability += probabilities[i];
        } else {
            probabilities[i] = 0.0;
        }
    }

    // Choisir aléatoirement la prochaine ville en fonction des probabilités
    double random = (double)rand() / RAND_MAX;
    double cumulativeProbability = 0.0;
    int nextCity = -1;

    for (int i = 0; i < NUM_CITIES; i++) {
        cumulativeProbability += probabilities[i] / totalProbability;
        if (random <= cumulativeProbability) {
            nextCity = i;
            break;
        }
    }

    return nextCity;
}

// Fonction pour mettre à jour les phéromones en fonction des solutions des fourmis
void updatePheromones(int antPaths[NUM_ANTS][NUM_CITIES]) {
    for (int i = 0; i < NUM_CITIES; i++) {
        for (int j = 0; j < NUM_CITIES; j++) {
            pheromones[i][j] *= (1.0 - EVAPORATION_RATE);
        }
    }

    for (int i = 0; i < NUM_ANTS; i++) {
        double antPheromones = 1.0 / calculateTotalCost(antPaths[i]);
        for (int j = 0; j < NUM_CITIES - 1; j++) {
            pheromones[antPaths[i][j]][antPaths[i][j + 1]] += antPheromones;
        }
        pheromones[antPaths[i][NUM_CITIES - 1]][antPaths[i][0]] += antPheromones;
    }
}

// Fonction pour calculer le coût total du parcours
int calculateTotalCost(int path[NUM_CITIES]) {
    int totalCost = 0;

    for (int i = 0; i < NUM_CITIES - 1; i++) {
        totalCost += distances[path[i]][path[i + 1]];
    }

    // Ajouter le coût du retour à la ville de départ
    totalCost += distances[path[NUM_CITIES - 1]][path[0]];

    return totalCost;
}

// Fonction pour effectuer l'algorithme des colonies de fourmis
void antColonyAlgorithm() {
    srand(time(NULL));

    initializePheromones();

    int antPaths[NUM_ANTS][NUM_CITIES];

    for (int iteration = 0; iteration < 100; iteration++) {
        for (int ant = 0; ant < NUM_ANTS; ant++) {
            bool visited[NUM_CITIES] = {false};
            int currentCity = rand() % NUM_CITIES;
            int pathIndex = 0;

            visited[currentCity] = true;
            antPaths[ant][pathIndex++] = currentCity;

            while (pathIndex < NUM_CITIES) {
                int nextCity = getNextCity(currentCity, visited);

                visited[nextCity] = true;
                antPaths[ant][pathIndex++] = nextCity;
                currentCity = nextCity;
            }

            int totalCost = calculateTotalCost(antPaths[ant]);
            printf("Fourmi %d - Parcours : ", ant);
            for (int i = 0; i < NUM_CITIES; i++) {
                printf("%c ", 'a' + antPaths[ant][i]);
            }
            printf(" - Coût total : %d\n", totalCost);
        }

        updatePheromones(antPaths);
    }
}

int main() {
    antColonyAlgorithm();

    return 0;
}
