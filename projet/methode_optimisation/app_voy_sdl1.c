#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <time.h>

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600
#define NUM_CITIES 5
#define NUM_ANTS 1
#define ALPHA 1.0
#define BETA 5.0
#define RHO 0.5
#define Q 100
#define MAX_ITERATIONS 1000

typedef struct {
    int x;
    int y;
} City;

typedef struct {
    int path[NUM_CITIES+1];
    double tourLength;
} Ant;

City cities[NUM_CITIES];
Ant ants[NUM_ANTS];
int distances[NUM_CITIES][NUM_CITIES];
double pheromones[NUM_CITIES][NUM_CITIES];
int bestTour[NUM_CITIES];
double bestTourLength = INFINITY;
SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;
SDL_Texture* cityTexture = NULL;
SDL_Texture* antTexture = NULL;

int visited(int* path, int city);
void drawCity(int x, int y);
void drawAnt(int x, int y);

void render() {
    SDL_RenderClear(renderer);

    // Dessiner les villes
    for (int i = 0; i < NUM_CITIES; i++) {
        drawCity(cities[i].x, cities[i].y);
    }

    // Dessiner les fourmis
    for (int i = 0; i < NUM_ANTS; i++) {
        int cityIndex = ants[i].path[0]; // Première ville visitée par la fourmi
        int x = cities[cityIndex].x;
        int y = cities[cityIndex].y;
        drawAnt(x, y);
    }

    SDL_RenderPresent(renderer);
}

void initSDL() {
    SDL_Init(SDL_INIT_VIDEO);
    IMG_Init(IMG_INIT_PNG);
    window = SDL_CreateWindow("Traveling Salesman Problem", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    cityTexture = IMG_LoadTexture(renderer, "City.png");
    antTexture = IMG_LoadTexture(renderer, "ant.png");
}

void cleanupSDL() {
    SDL_DestroyTexture(cityTexture);
    SDL_DestroyTexture(antTexture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    IMG_Quit();
    SDL_Quit();
}

void generateCities() {
    cities[0].x = 550;
    cities[0].y = 150 ;
    
    cities[1].x = 150 ;
    cities[1].y = 150;
    
    cities[2].x = 400;
    cities[2].y = 300;
    
    cities[3].x = 550;
    cities[3].y = 450;

    cities[4].x = 150;
    cities[4].y = 450;
}

void generateDistances() {
    for (int i = 0; i < NUM_CITIES; i++) {
        for (int j = 0; j < NUM_CITIES; j++) {
            int dx = cities[i].x - cities[j].x;
            int dy = cities[i].y - cities[j].y;
            distances[i][j] = (int)round(sqrt(dx*dx + dy*dy));
        }
    }
}

void initializeAnts() {
    for (int i = 0; i < NUM_ANTS; i++) {
        for (int j = 0; j < NUM_CITIES; j++) {
            ants[i].path[j] = -1;
        }
        ants[i].tourLength = 0.0;
        
        // Place chaque fourmi sur une ville aléatoire
        int randomCity = rand() % NUM_CITIES;
        ants[i].path[0] = randomCity;
        ants[i].path[NUM_CITIES] = randomCity;
    }
}

void updateAnts() {
    for (int i = 0; i < NUM_ANTS; i++) {
        for (int j = 1; j < NUM_CITIES; j++) {
            int currentCity = ants[i].path[j];
            
            // Construire la liste des villes non visitées
            int unvisited[NUM_CITIES];
            int count = 0;
            for (int k = 0; k < NUM_CITIES; k++) {
                if (!visited(ants[i].path, k)) {
                    unvisited[count++] = k;
                }
            }
            
            // Calculer les probabilités de transition vers les villes non visitées
            double probabilities[NUM_CITIES];
            double sum = 0.0;
            for (int k = 0; k < count; k++) {
                int city = unvisited[k];
                double pheromone = pow(pheromones[currentCity][city], ALPHA);
                double distance = pow(1.0 / distances[currentCity][city], BETA);
                probabilities[k] = pheromone * distance;
                sum += probabilities[k];
            }
            
            // Choisir la prochaine ville en fonction des probabilités
            double random = (double)rand() / RAND_MAX;
            double cumulative = 0.0;
            int nextCity = -1;
            for (int k = 0; k < count; k++) {
                probabilities[k] /= sum;
                cumulative += probabilities[k];
                if (random <= cumulative) {
                    nextCity = unvisited[k];
                    break;
                }
            }
            
            // Mettre à jour le chemin et la longueur du tour
            ants[i].path[j] = nextCity;
            ants[i].tourLength += distances[currentCity][nextCity];
        }
    }
}

void updatePheromones() {
    // Évaporation des phéromones
    for (int i = 0; i < NUM_CITIES; i++) {
        for (int j = 0; j < NUM_CITIES; j++) {
            pheromones[i][j] *= (1.0 - RHO);
        }
    }
    
    // Dépôt des phéromones par les fourmis
    for (int i = 0; i < NUM_ANTS; i++) {
        for (int j = 0; j < NUM_CITIES - 1; j++) {
            int city1 = ants[i].path[j];
            int city2 = ants[i].path[j+1];
            pheromones[city1][city2] += Q / ants[i].tourLength;
        }
    }
}

void search() {
    for (int i = 0; i < MAX_ITERATIONS; i++) {
        updateAnts();
        updatePheromones();
        
        // Vérifier si la meilleure solution a été trouvée
        for (int j = 0; j < NUM_ANTS; j++) {
            if (ants[j].tourLength < bestTourLength) {
                bestTourLength = ants[j].tourLength;
                for (int k = 0; k < NUM_CITIES; k++) {
                    bestTour[k] = ants[j].path[k];
                }
            }
        }
    }
}

int visited(int* path, int city) {
    for (int i = 0; i < NUM_CITIES; i++) {
        if (path[i] == city) {
            return 1;
        }
    }
    return 0;
}

void drawCity(int x, int y) {
    SDL_Rect rect = { x - 50, y - 50, 100, 100 };
    SDL_RenderCopy(renderer, cityTexture, NULL, &rect);
}

void drawAnt(int x, int y) {
    SDL_Rect rect = { x - 3, y - 3, 30, 30 };
    SDL_RenderCopy(renderer, antTexture, NULL, &rect);
}

int main(int argc, char** argv) {
    srand(time(NULL));
    
    initSDL();
    generateCities();
    generateDistances();
    initializeAnts();
    
    search();
    
    render();
    SDL_Delay(5000);
    
    cleanupSDL();
    
    return 0;
}
