#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <ctime>

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const int NUM_CITIES = 5;
const int NUM_ANTS = 1;
const int MAX_ITERATIONS = 100;
const double ALPHA = 1.0;  // Importance du niveau de phéromone
const double BETA = 5.0;   // Importance de la distance
const double RHO = 0.5;    // Taux d'évaporation des phéromones
const double Q = 100.0;    // Quantité de phéromone déposée par les fourmis

struct City {
    int x;
    int y;
};

struct Ant {
    int path[NUM_CITIES];
    double tourLength;
};

SDL_Window* window = nullptr;
SDL_Renderer* renderer = nullptr;
SDL_Texture* cityTexture = nullptr;
SDL_Texture* antTexture = nullptr;

City cities[NUM_CITIES];
int distances[NUM_CITIES][NUM_CITIES];
double pheromones[NUM_CITIES][NUM_CITIES];
Ant ants[NUM_ANTS];
double bestTourLength = std::numeric_limits<double>::max();
int bestTour[NUM_CITIES];

bool initSDL() {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "Erreur lors de l'initialisation de SDL : " << SDL_GetError() << std::endl;
        return false;
    }

    window = SDL_CreateWindow("ACO Traveling Salesman Problem", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (!window) {
        std::cerr << "Erreur lors de la création de la fenêtre : " << SDL_GetError() << std::endl;
        return false;
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (!renderer) {
        std::cerr << "Erreur lors de la création du renderer : " << SDL_GetError() << std::endl;
        return false;
    }

    int imgFlags = IMG_INIT_PNG;
    if (!(IMG_Init(imgFlags) & imgFlags)) {
        std::cerr << "Erreur lors de l'initialisation de SDL_image : " << IMG_GetError() << std::endl;
        return false;
    }

    return true;
}

void cleanupSDL() {
    SDL_DestroyTexture(cityTexture);
    SDL_DestroyTexture(antTexture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    IMG_Quit();
    SDL_Quit();
}

SDL_Texture* loadTexture(const std::string& path) {
    SDL_Surface* surface = IMG_Load(path.c_str());
    if (!surface) {
        std::cerr << "Erreur lors du chargement de l'image : " << IMG_GetError() << std::endl;
        return nullptr;
    }

    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);
    return texture;
}

void render() {
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);

    // Afficher les villes
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    for (int i = 0; i < NUM_CITIES; i++) {
        drawCity(cities[i].x, cities[i].y);
    }

    // Afficher l'ant
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    for (int i = 0; i < NUM_CITIES - 1; i++) {
        drawAnt(cities[bestTour[i]].x, cities[bestTour[i]].y);
        SDL_RenderPresent(renderer);
        SDL_Delay(1000);
    }

    drawAnt(cities[bestTour[NUM_CITIES - 1]].x, cities[bestTour[NUM_CITIES - 1]].y);
    SDL_RenderPresent(renderer);
}

void generateCities() {
    for (int i = 0; i < NUM_CITIES; i++) {
        cities[i].x = rand() % (SCREEN_WIDTH - 100) + 50;
        cities[i].y = rand() % (SCREEN_HEIGHT - 100) + 50;
    }
}

void generateDistances() {
    for (int i = 0; i < NUM_CITIES; i++) {
        for (int j = 0; j < NUM_CITIES; j++) {
            int dx = cities[i].x - cities[j].x;
            int dy = cities[i].y - cities[j].y;
            distances[i][j] = sqrt(dx * dx + dy * dy);
        }
    }
}

void initializeAnts() {
    for (int i = 0; i < NUM_ANTS; i++) {
        for (int j = 0; j < NUM_CITIES; j++) {
            ants[i].path[j] = -1;
        }
        ants[i].tourLength = 0.0;
        ants[i].path[0] = rand() % NUM_CITIES;

        for (int j = 0; j < NUM_CITIES - 1; j++) {
            int city1 = ants[i].path[j];
            int nextCity = -1;
            double r = static_cast<double>(rand()) / RAND_MAX;
            double total = 0.0;

            for (int k = 0; k < NUM_CITIES; k++) {
                if (!visited(ants[i].path, k)) {
                    double prob = pow(pheromones[city1][k], ALPHA) * pow(1.0 / distances[city1][k], BETA);
                    total += prob;
                    if (total >= r) {
                        nextCity = k;
                        break;
                    }
                }
            }

            ants[i].path[j + 1] = nextCity;
            ants[i].tourLength += distances[city1][nextCity];
        }

        int lastCity = ants[i].path[NUM_CITIES - 1];
        ants[i].tourLength += distances[lastCity][ants[i].path[0]];

        if (ants[i].tourLength < bestTourLength) {
            bestTourLength = ants[i].tourLength;
            for (int k = 0; k < NUM_CITIES; k++) {
                bestTour[k] = ants[i].path[k];
            }
        }
    }
}

void updateAnts() {
    for (int i = 0; i < NUM_ANTS; i++) {
        for (int j = 0; j < NUM_CITIES - 1; j++) {
            int city1 = ants[i].path[j];
            int city2 = ants[i].path[j + 1];
            ants[i].tourLength += distances[city1][city2];
        }

        int lastCity = ants[i].path[NUM_CITIES - 1];
        int firstCity = ants[i].path[0];
        ants[i].tourLength += distances[lastCity][firstCity];
    }
}

void updatePheromones() {
    for (int i = 0; i < NUM_CITIES; i++) {
        for (int j = 0; j < NUM_CITIES; j++) {
            pheromones[i][j] *= (1.0 - RHO);
        }
    }

    for (int i = 0; i < NUM_ANTS; i++) {
        for (int j = 0; j < NUM_CITIES - 1; j++) {
            int city1 = ants[i].path[j];
            int city2 = ants[i].path[j + 1];
            pheromones[city1][city2] += Q / ants[i].tourLength;
        }

        int lastCity = ants[i].path[NUM_CITIES - 1];
        int firstCity = ants[i].path[0];
        pheromones[lastCity][firstCity] += Q / ants[i].tourLength;
    }
}

bool visited(int* path, int city) {
    for (int i = 0; i < NUM_CITIES; i++) {
        if (path[i] == city) {
            return true;
        }
    }
    return false;
}

void drawCity(int x, int y) {
    SDL_Rect cityRect;
    cityRect.x = x - 5;
    cityRect.y = y - 5;
    cityRect.w = 10;
    cityRect.h = 10;
    SDL_RenderCopy(renderer, cityTexture, nullptr, &cityRect);
}

void drawAnt(int x, int y) {
    SDL_Rect antRect;
    antRect.x = x - 2;
    antRect.y = y - 2;
    antRect.w = 4;
    antRect.h = 4;
    SDL_RenderCopy(renderer, antTexture, nullptr, &antRect);
}

int main() {
    if (!initSDL()) {
        return 1;
    }

    cityTexture = loadTexture("city.png");
    if (!cityTexture) {
        cleanupSDL();
        return 1;
    }

    antTexture = loadTexture("ant.png");
    if (!antTexture) {
        cleanupSDL();
        return 1;
    }

    srand(static_cast<unsigned int>(time(nullptr)));

    generateCities();
    generateDistances();

    bool quit = false;
    SDL_Event e;

    while (!quit) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = true;
            }
        }

        initializeAnts();
        for (int i = 0; i < MAX_ITERATIONS; i++) {
            updateAnts();
            updatePheromones();

            if (i % 10 == 0) {
                render();
            }
        }

        std::cout << "Meilleure distance : " << bestTourLength << std::endl;

        render();
    }

    cleanupSDL();
    return 0;
}
