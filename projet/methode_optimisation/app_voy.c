#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <time.h>

#define NUM_CITIES 10
#define NUM_ANTS 100
#define ALPHA 1.0
#define BETA 2.0
#define RHO 0.5
#define Q 100.0
#define MAX_ITERATIONS 100

typedef struct {
    int x;
    int y;
} City;

City cities[NUM_CITIES];
double distances[NUM_CITIES][NUM_CITIES];
double pheromones[NUM_CITIES][NUM_CITIES];

double calculateDistance(City city1, City city2) {
    int dx = city1.x - city2.x;
    int dy = city1.y - city2.y;
    return sqrt(dx * dx + dy * dy);
}

void initialize() {
    // Initialize cities with random coordinates
    for (int i = 0; i < NUM_CITIES; i++) {
        cities[i].x = rand() % 100;
        cities[i].y = rand() % 100;
    }

    // Calculate distances between cities
    for (int i = 0; i < NUM_CITIES; i++) {
        for (int j = 0; j < NUM_CITIES; j++) {
            if (i == j) {
                distances[i][j] = 0.0;
            } else {
                distances[i][j] = calculateDistance(cities[i], cities[j]);
            }
        }
    }

    // Initialize pheromones
    for (int i = 0; i < NUM_CITIES; i++) {
        for (int j = 0; j < NUM_CITIES; j++) {
            pheromones[i][j] = 1.0;
        }
    }
}

int selectNextCity(int ant, int currentCity, int visited[]) {
    double probabilities[NUM_CITIES] = { 0.0 };
    double totalProbability = 0.0;

    // Calculate probabilities for unvisited cities
    for (int i = 0; i < NUM_CITIES; i++) {
        if (visited[i] == 0) {
            double pheromone = pow(pheromones[currentCity][i], ALPHA);
            double distance = pow(1.0 / distances[currentCity][i], BETA);
            probabilities[i] = pheromone * distance;
            totalProbability += probabilities[i];
        }
    }

    // Select next city based on probabilities
    double randomValue = (double)rand() / RAND_MAX;
    double cumulativeProbability = 0.0;
    for (int i = 0; i < NUM_CITIES; i++) {
        if (visited[i] == 0) {
            probabilities[i] /= totalProbability;
            cumulativeProbability += probabilities[i];
            if (randomValue <= cumulativeProbability) {
                return i;
            }
        }
    }

    // Should not reach here, but return an unvisited city as a fallback
    for (int i = 0; i < NUM_CITIES; i++) {
        if (visited[i] == 0) {
            return i;
        }
    }

    return -1; // Invalid value
}

void antColony() {
    int bestTour[NUM_CITIES + 1];
    double bestDistance = INT_MAX;

    for (int iteration = 0; iteration < MAX_ITERATIONS; iteration++) {
        double tourDistances[NUM_ANTS] = { 0.0 };
        int tours[NUM_ANTS][NUM_CITIES + 1];

        for (int ant = 0; ant < NUM_ANTS; ant++) {
            int visited[NUM_CITIES] = { 0 };
            int startCity = rand() % NUM_CITIES;
            int currentCity = startCity;
            visited[currentCity] = 1;
            tours[ant][0] = currentCity;

            for (int i = 0; i < NUM_CITIES - 1; i++) {
                int nextCity = selectNextCity(ant, currentCity, visited);
                visited[nextCity] = 1;
                tours[ant][i + 1] = nextCity;
                tourDistances[ant] += distances[currentCity][nextCity];
                currentCity = nextCity;
            }

            // Return to start city
            tours[ant][NUM_CITIES] = startCity;
            tourDistances[ant] += distances[currentCity][startCity];
        }

        // Update pheromones
        for (int i = 0; i < NUM_CITIES; i++) {
            for (int j = 0; j < NUM_CITIES; j++) {
                pheromones[i][j] *= RHO; // Evaporation
            }
        }

        for (int ant = 0; ant < NUM_ANTS; ant++) {
            double tourDistance = tourDistances[ant];
            double deltaPheromone = Q / tourDistance;

            for (int i = 0; i < NUM_CITIES; i++) {
                int city1 = tours[ant][i];
                int city2 = tours[ant][i + 1];
                pheromones[city1][city2] += deltaPheromone;
                pheromones[city2][city1] += deltaPheromone;
            }

            // Update best tour if necessary
            if (tourDistance < bestDistance) {
                bestDistance = tourDistance;
                for (int i = 0; i < NUM_CITIES + 1; i++) {
                    bestTour[i] = tours[ant][i];
                }
            }
        }

        printf("Iteration %d: Best Distance = %.2f\n", iteration + 1, bestDistance);
    }

    printf("\nBest Tour: ");
    for (int i = 0; i < NUM_CITIES + 1; i++) {
        printf("%d ", bestTour[i]);
    }
    printf("\nBest Distance: %.2f\n", bestDistance);
}

int main() {
    srand(time(NULL));
    initialize();
    antColony();
    return 0;
}
