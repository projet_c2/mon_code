#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

int main() {
    SDL_Init(SDL_INIT_VIDEO);
    IMG_Init(IMG_INIT_PNG);

    SDL_Window* window = SDL_CreateWindow("Fourmi", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                          WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    SDL_Surface* flowerSurface = IMG_Load("ants.jpg");
    SDL_Texture* flowerTexture = SDL_CreateTextureFromSurface(renderer, flowerSurface);
    SDL_FreeSurface(flowerSurface);

    SDL_Surface* antSurface = IMG_Load("ants.jpg");
    SDL_Texture* antTexture = SDL_CreateTextureFromSurface(renderer, antSurface);
    SDL_FreeSurface(antSurface);

    int antX = WINDOW_WIDTH / 2;
    int antY = WINDOW_HEIGHT / 2;

    SDL_Event event;
    int quit = 0;

    while (!quit) {
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                quit = 1;
            }
        }

        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
        SDL_RenderClear(renderer);

        // Dessiner le fond de fleurs
        int flowerWidth, flowerHeight;
        SDL_QueryTexture(flowerTexture, NULL, NULL, &flowerWidth, &flowerHeight);
        for (int x = 0; x < WINDOW_WIDTH; x += flowerWidth) {
            for (int y = 0; y < WINDOW_HEIGHT; y += flowerHeight) {
                SDL_Rect destRect = { x, y, flowerWidth, flowerHeight };
                SDL_RenderCopy(renderer, flowerTexture, NULL, &destRect);
            }
        }

        // Dessiner la fourmi
        int antWidth, antHeight;
        SDL_QueryTexture(antTexture, NULL, NULL, &antWidth, &antHeight);
        SDL_Rect antRect = { antX, antY, antWidth, antHeight };
        SDL_RenderCopy(renderer, antTexture, NULL, &antRect);

        SDL_RenderPresent(renderer);
    }

    SDL_DestroyTexture(flowerTexture);
    SDL_DestroyTexture(antTexture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    IMG_Quit();
    SDL_Quit();

    return 0;
}
