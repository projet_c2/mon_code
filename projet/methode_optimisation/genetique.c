#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define VILLES 5 // Nombre de villes
#define POPULATION 10 // Taille de la population
#define ITERATIONS 1000 // Nombre d'itérations

typedef struct {
    int chemin[VILLES]; // Ordre de visite des villes
    double fitness; // Évaluation de la qualité de l'individu
} Individu;

typedef struct {
    int x;
    int y;
} Ville;

Ville villes[VILLES] = { {1, 2}, {3, 4}, {5, 6}, {7, 8}, {9, 10} }; // Coordonnées des villes (à adapter)

double distance(Ville ville1, Ville ville2) {
    int dx = ville2.x - ville1.x;
    int dy = ville2.y - ville1.y;
    return sqrt(dx * dx + dy * dy);
}

void creerPopulationInitiale(Individu population[], int taillePopulation) {
    for (int i = 0; i < taillePopulation; i++) {
        // Génération aléatoire d'un ordre de visite des villes
        for (int j = 0; j < VILLES; j++) {
            population[i].chemin[j] = j;
        }
        for (int j = VILLES - 1; j > 0; j--) {
            int index = rand() % (j + 1);
            int temp = population[i].chemin[j];
            population[i].chemin[j] = population[i].chemin[index];
            population[i].chemin[index] = temp;
        }
    }
}

double evaluerChemin(int chemin[]) {
    double distanceTotale = 0.0;
    for (int i = 0; i < VILLES - 1; i++) {
        int villeActuelle = chemin[i];
        int villeSuivante = chemin[i + 1];
        distanceTotale += distance(villes[villeActuelle], villes[villeSuivante]);
    }
    // Ajout de la distance entre la dernière ville et la première ville pour former une boucle
    distanceTotale += distance(villes[chemin[VILLES - 1]], villes[chemin[0]]);
    return distanceTotale;
}

void evaluerPopulation(Individu population[], int taillePopulation) {
    for (int i = 0; i < taillePopulation; i++) {
        double distanceChemin = evaluerChemin(population[i].chemin);
        population[i].fitness = 1 / distanceChemin;
    }
}

void selectionnerParents(Individu population[], Individu parents[], int taillePopulation) {
    for (int i = 0; i < taillePopulation; i++) {
        parents[i] = population[i];
    }
}

void croiserParents(Individu parents[], Individu enfants[], int tailleParents) {
    for (int i = 0; i < tailleParents; i++) {
        int indexParent1 = rand() % tailleParents;
        int indexParent2 = rand() % tailleParents;

        int pointDeCoupure1 = rand() % VILLES;
        int pointDeCoupure2 = rand() % VILLES;

        if (pointDeCoupure1 > pointDeCoupure2) {
            int temp = pointDeCoupure1;
            pointDeCoupure1 = pointDeCoupure2;
            pointDeCoupure2 = temp;
        }

        int* parent1 = parents[indexParent1].chemin;
        int* parent2 = parents[indexParent2].chemin;

        int* enfant = enfants[i].chemin;

        for (int j = 0; j < VILLES; j++) {
            if (j >= pointDeCoupure1 && j <= pointDeCoupure2) {
                enfant[j] = parent1[j];
            } else {
                enfant[j] = -1;
            }
        }

        int indexParent2Copy = 0;
        for (int j = 0; j < VILLES; j++) {
            if (enfant[j] == -1) {
                while (enfant[j] == -1) {
                    int ville = parent2[indexParent2Copy++];
                    int estDejaPresent = 0;
                    for (int k = 0; k < VILLES; k++) {
                        if (enfant[k] == ville) {
                            estDejaPresent = 1;
                            break;
                        }
                    }
                    if (!estDejaPresent) {
                        enfant[j] = ville;
                    }
                }
            }
        }
    }
}

void muterEnfants(Individu enfants[], int tailleEnfants) {
    for (int i = 0; i < tailleEnfants; i++) {
        if (rand() % 100 < 5) { // Probabilité de mutation de 5%
            int index1 = rand() % VILLES;
            int index2 = rand() % VILLES;
            int temp = enfants[i].chemin[index1];
            enfants[i].chemin[index1] = enfants[i].chemin[index2];
            enfants[i].chemin[index2] = temp;
        }
    }
}

void remplacerPopulation(Individu population[], Individu enfants[], int taillePopulation) {
    for (int i = 0; i < taillePopulation; i++) {
        population[i] = enfants[i];
    }
}

void algorithmeGenetique(int taillePopulation, int nombreIterations) {
    Individu population[taillePopulation];
    Individu parents[taillePopulation];
    Individu enfants[taillePopulation];

    creerPopulationInitiale(population, taillePopulation);

    for (int iteration = 0; iteration < nombreIterations; iteration++) {
        evaluerPopulation(population, taillePopulation);
        selectionnerParents(population, parents, taillePopulation);
        croiserParents(parents, enfants, taillePopulation);
        muterEnfants(enfants, taillePopulation);
        remplacerPopulation(population, enfants, taillePopulation);
    }

    evaluerPopulation(population, taillePopulation);

    double meilleurFitness = population[0].fitness;
    int indexMeilleur = 0;
    for (int i = 1; i < taillePopulation; i++) {
        if (population[i].fitness > meilleurFitness) {
            meilleurFitness = population[i].fitness;
            indexMeilleur = i;
        }
    }

    printf("Meilleur individu (chemin) : ");
    for (int i = 0; i < VILLES; i++) {
        printf("%d ", population[indexMeilleur].chemin[i]);
    }
    printf("\n");

    printf("Meilleur individu (fitness) : %f\n", population[indexMeilleur].fitness);
}

int main() {
    srand(time(NULL));

    algorithmeGenetique(POPULATION, ITERATIONS);

    return 0;
}
