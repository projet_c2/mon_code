
#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <time.h>
#include <tar.h>
#include <errno.h>
#include <libgen.h>
#include <zlib.h>

void createWindow() {
    // Initialisation de SDL
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        printf("Erreur lors de l'initialisation de SDL: %s\n", SDL_GetError());
        return;
    }

    // Création de la fenêtre
    SDL_Window* window = SDL_CreateWindow("Ma fenêtre SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_SHOWN);
    if (window == NULL) {
        printf("Erreur lors de la création de la fenêtre: %s\n", SDL_GetError());
        SDL_Quit();
        return;
    }

    // Attente de quelques secondes
    SDL_Delay(3000);

    // Fermeture de la fenêtre
    SDL_DestroyWindow(window);

    // Nettoyage de SDL
    SDL_Quit();
}

int main() {
    // Création de la fenêtre
    createWindow();

    // Compression du programme dans une archive tar.gz
    char* executableName = basename(__FILE__);
    char* command = malloc(strlen(executableName) + 18);
    sprintf(command, "tar -czf all_executables.tar.gz %s", executableName);
    system(command);
    free(command);

    return 0;
}
