#include <stdio.h>



int main()
{
   int i  , *ptri  = &i;
   char c1 = '1', *ptrc1 = &c1;

   printf("ptri = %p  . \nptrc1 = %p \n",ptri, ptrc1);
   printf("ptri = %x  . \nptrc1 = %x \n",ptri, ptrc1);
   // seule la version suivante ne genere pas d'avertissement
   printf("ptri = %p  .\nptrc1 = %p \n",ptri, ptrc1);
   printf("*ptri = %d . \n*ptrc1=%c\n", *ptri, *ptrc1);
   
   ptri++;
   ptrc1++;
   printf("apres\n");
   printf("ptri = %p  et ptrc1 = %p \n",ptri, ptrc1);
   // cela permet de voir la taille d'un int et d'un char en memoire
   // sizeof(int)  sizeof(char)

   return 0 ;
}

