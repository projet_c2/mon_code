#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#define MAX_BUFFER_SIZE 256


struct arbre
{
    char * path;
    struct arbre * lh;
    struct arbre * lv;
};




/// @brief 
/// @param dir 
/// @param pracine 
void parcour(char * dir , struct arbre  * pracine){
    pracine->path = strdup(dir);
    int i=0 , lg =0, aff = 1;
    if (chdir(dir) == 0) 
{
    printf("'%s' dont ses fils sont : ", dir);
    FILE* fp;
    char path[MAX_BUFFER_SIZE];
    char command[] = "ls -d */ 2>&1";

    fp = popen(command, "r");
    if (fp != NULL) {

    struct arbre* cour = pracine;
    while (fgets(path, sizeof(path), fp) != NULL) {
        if (strncmp(path, "ls: impossible d'accéder", 24) == 0) {
            aff=0;
            break;
        }
        lg = strlen(path);
        path[lg - 2] = '\0';
        struct arbre* ele = (struct arbre*)malloc(sizeof(struct arbre));
        ele->path = strdup(path);
        ele->lh = NULL;
        ele->lv = NULL;
        if (i == 0) {
            pracine->lv = ele;
            cour = pracine->lv;
        } else {
            cour->lh = ele;
            cour = cour->lh;
        }
        i++;
    }

    pclose(fp);
    }
    /*if(aff==1){
    struct arbre *cour = pracine->lv;
    while (cour != NULL) {
        printf("%s ", cour->path);
        cour = cour->lh;
    }}*/
    printf("succes\n");
}
     //else{perror("Erreur lors du changement de répertoire");}
     


     chdir("/");

}

int main() {
    char* dir = "/";
    struct arbre  * pracine = (struct arbre *)malloc(sizeof(struct arbre));

    parcour(dir , pracine);

    struct arbre *cour = pracine->lv;
    while (cour != NULL) {
        printf("%s ", cour->path);
        cour = cour->lh;
    }
    printf("\n");
    cour = pracine->lv;
     while (cour != NULL) {
        struct arbre  * pprec = (struct arbre *)malloc(sizeof(struct arbre));
        parcour((cour)->path ,pprec);
        printf("\n");
        cour->lv = pprec;
        cour = cour->lh;
    }

    

    return 0;
}