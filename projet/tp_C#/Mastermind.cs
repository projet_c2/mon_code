
using System;


public class Mastermind{

    public const int NB_COLONNES  = 4;
    public const int NB_LIGNES = 10;

    static void initialiser_solution( int[] solution)
    {

    int i=0;
    Console.WriteLine();
    Console.Write("la solution est : ");
    Random random = new Random();

    int m = 0;
    for (i=0 ; i<NB_COLONNES ; i++)
    {
        m = 1 + (int)(random.NextDouble() * 5);
        solution[i] = m;
        Console.Write(m);
    }
    Console.WriteLine();
    }



     static void initialiser_plateau(int[,] plateau)
    {
      int i , j ;
      for (i=0 ; i<NB_LIGNES ; i++)
      {
        for(j=0 ; j<NB_COLONNES+2 ; j++)
        {
            plateau[i,j] = 0 ;
        }
     }

   }



    static void compiler_proposition(int[,] plateau)
   {
    int[] solution = new int[NB_COLONNES];
    //initialiser_solution(solution);
    //int proposition[NB_COLONNES];
    int pas = 0, i=0, a=0, b=0,k=0;
    while (pas < 10 )
    {
        i=0;
        a=0;
        b=0;
        k=0;
       initialiser_solution(solution);
       
       for (i=0 ; i<NB_COLONNES ; i++)
       {
        Console.WriteLine("\nentrer propo" + (i+1)+ " entre 1 et 6 : ");
        int prp ;
        string input = Console.ReadLine();
        string[] number = input.Split(' ');
        int.TryParse(number[0], out prp);
        plateau[pas, i] = prp;
        if (plateau[pas, i] == solution[i])
        {   a++ ;}
        else
        {     
             k=0;
             while (k<NB_COLONNES)
              {
                    if ( (solution[k]==plateau[pas, i]) )
                    {
                            b = b + 1;
                            k = k + NB_COLONNES;
                    }
                    else      k++;
              }
        }
        }
        plateau[pas, 4] = a;
        plateau[pas , 5] = b;
        Console.WriteLine(" tu est passée ce ligne  par " + a + " pions Bien Placée et "+ b +" pions Mal Placée \n");
        
        pas++;
     }
   }


   static void Main(){

    int[,] plateau = new int[NB_LIGNES, NB_COLONNES + 2];
    initialiser_plateau(plateau);
    compiler_proposition(plateau);
    int i=0, j=0;
    for(i=0 ; i<NB_LIGNES ; i++)
    {
        Console.WriteLine();
        for(j=0 ; j<NB_COLONNES + 2 ; j++)
        {
            Console.Write("  " + plateau[i, j]);
        }
        Console.Write("\n");
    }
   }
}