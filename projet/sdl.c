#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#define MAX_BUFFER_SIZE 1024

struct arbre
{
    char* path;
    struct arbre* lh;
    struct arbre* lv;
};

void renderText(SDL_Renderer* renderer, TTF_Font* font, const char* text, int x, int y, SDL_Color color)
{
    SDL_Surface* surface = TTF_RenderText_Solid(font, text, color);
    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_Rect rect;
    rect.x = x;
    rect.y = y;
    rect.w = surface->w;
    rect.h = surface->h;
    SDL_RenderCopy(renderer, texture, NULL, &rect);
    SDL_FreeSurface(surface);
    SDL_DestroyTexture(texture);
}

void renderTree(SDL_Renderer* renderer, TTF_Font* font, struct arbre* node, int x, int y, int level, SDL_Color fileColor, SDL_Color dirColor)
{
    if (node == NULL)
        return;

    renderText(renderer, font, node->path, x, y, level > 0 ? dirColor : fileColor);

    int newY = y + 20;
    if (node->lv != NULL)
        renderTree(renderer, font, node->lv, x + 20, newY, level + 1, fileColor, dirColor);
    if (node->lh != NULL)
        renderTree(renderer, font, node->lh, x, newY, level, fileColor, dirColor);
}

int main()
{
    /*
    const char* directory = "/";

    struct arbre racine = { "/", NULL, NULL };
    if (chdir(directory) != 0)
    {
        return -1;
    }

    FILE* fp;
    char path[MAX_BUFFER_SIZE];
    c

    fp = popen(command, "r");
    if (fp == NULL)
    {
        return -1;
    }

    struct arbre* cour = &racine;
    while (fgets(path, sizeof(path), fp) != NULL)
    {
        int lg = strlen(path);
        path[lg - 2] = '\0';
        struct arbre* ele = (struct arbre*)malloc(sizeof(struct arbre));
        ele->path = strdup(path);
        ele->lh = NULL;
        ele->lv = NULL;

        if (cour->lv == NULL)
        {
            cour->lv = ele;
            cour = cour->lv;
        }
        else
        {
            cour->lh = ele;
            cour = cour->lh;
        }
    }

    pclose(fp);
*/
    SDL_Init(SDL_INIT_VIDEO);
    TTF_Init();

    SDL_Window* window = SDL_CreateWindow("Explorateur de fichiers", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_SHOWN);
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    TTF_Font* font = TTF_OpenFont(NULL, 16);

    SDL_Color fileColor = { 255, 255, 255 };
    SDL_Color dirColor = { 0, 255, 0 };

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);

    //renderTree(renderer, font, &racine, 20, 20, 0, fileColor, dirColor);

    SDL_RenderPresent(renderer);

    SDL_Event event;
    int quit = 0;
    while (!quit)
    {
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
                quit = 1;
        }
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    TTF_CloseFont(font);
    TTF_Quit();
    SDL_Quit();
/*
    cour = racine.lv;
    while (cour != NULL)
    {
        struct arbre* next = cour->lh;
        free(cour->path);
        free(cour);
        cour = next;
    }
*/
    return 0;
}