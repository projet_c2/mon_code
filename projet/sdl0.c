#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

#define TEXT_SIZE 24

int main()
{
    SDL_Init(SDL_INIT_VIDEO);
    TTF_Init();
    // Initialiser SDL
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        printf("Erreur lors de l'initialisation de SDL : %s\n", SDL_GetError());
        return 1;
    }

    // Créer une fenêtre
    SDL_Window* window = SDL_CreateWindow("Exemple SDL2 - Nombres pairs et impairs", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
    if (window == NULL)
    {
        printf("Erreur lors de la création de la fenêtre : %s\n", SDL_GetError());
        SDL_Quit();
        return 1;
    }

    // Créer un renderer pour dessiner dans la fenêtre
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL)
    {
        printf("Erreur lors de la création du renderer : %s\n", SDL_GetError());
        SDL_DestroyWindow(window);
        SDL_Quit();
        return 1;
    }

    // Charger une police de caractères
    SDL_Color textColor = { 255, 255, 255 }; // Couleur du texte
    TTF_Font* font = TTF_OpenFont("/usr/share/fonts/truetype/tlwg/TlwgTypo-Bold.ttf", TEXT_SIZE);
    if (font == NULL)
    {
        printf("Erreur lors du chargement de la police de caractères : %s\n", TTF_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        return 1;
    }

    // Effacer l'écran avec une couleur de fond
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);

    // Afficher les nombres pairs en bleu et les nombres impairs en rouge
    int x = 400; // Position horizontale initiale
    int y = 300; // Position verticale
    for (int i = 1; i <= 10; i++)
    {
        // Sélectionner la couleur en fonction de la parité du nombre
        SDL_Color numberColor = (i % 2 == 0) ? (SDL_Color) { 0, 0, 255 } : (SDL_Color) { 255, 0, 0 };

        // Convertir le nombre en chaîne de caractères
        char numberText[32];
        sprintf(numberText, "%d", i);

        // Rendu du texte
        SDL_Surface* surface = TTF_RenderText_Solid(font,numberText, numberColor);
        SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
        SDL_Rect rect = { x, y, surface->w, surface->h };
        SDL_RenderCopy(renderer, texture, NULL, &rect);

        // Libérer les ressources de rendu du texte
        SDL_FreeSurface(surface);
        SDL_DestroyTexture(texture);

        // Mettre à jour la position verticale pour le prochain nombre
        y += TEXT_SIZE + 10;
    }

    // Rafraîchir l'écran
    SDL_RenderPresent(renderer);

    // Attendre jusqu'à ce que l'utilisateur ferme la fenêtre
    /*
    SDL_Event event;
    int quit = 0;
    while (!quit)
    {
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
                quit = 1;
        }
    }
*/
    SDL_Delay(3000);
    // Libérer les ressources
    TTF_CloseFont(font);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}