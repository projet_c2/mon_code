#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

// Constantes de la fenêtre
const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT = 600;

// Constantes des formes
const int SHAPE_SIZE = 50;
const int INITIAL_SHAPE_SPEED = 5;
const int SCORE_INCREMENT = 1;
const float SPEED_INCREASE_FACTOR = 1.0 / 3.0;

// Constantes du plateau
const int PLATE_WIDTH = 100;
const int PLATE_HEIGHT = 20;
const int PLATE_SPEED = 5;

// Constantes du fond parallaxe
const int BACKGROUND_LAYER_COUNT = 12;
const int BACKGROUND_SCROLL_SPEEDS[] = {1, 2, 3, 4 , 5, 6,7 ,8,9,10,11,12};
const Uint32 BACKGROUND_CHANGE_INTERVAL = 3; // Durée en millisecondes avant de changer le fond
const int BACKGROUND_FADE_SPEED = 5; // Vitesse de transition d'opacité du fond

const char* BACKGROUND_IMAGES[] = {
    "fonts/bg.jpeg",
    "fonts/bg.jpeg",
    "fonts/bg.jpeg",
    "fonts/bg.jpeg",
    "fonts/bg.jpeg",
    "fonts/bg.jpeg",
    "fonts/bg.jpeg",
    "fonts/bg.jpeg",
    "fonts/bg.jpeg",
    "fonts/bg.jpeg",
    "fonts/bg.jpeg",
    "fonts/bg.jpeg"

};

// Structure pour représenter une forme
typedef struct {
    int x;
    int y;
    float speed;
    SDL_Texture* texture;
} Shape;

// Structure pour représenter un calque de fond parallaxe
typedef struct {
    int x;
    SDL_Texture* texture;
} BackgroundLayer;

// Fonction pour générer un nombre aléatoire entre min et max
int getRandomNumber(int min, int max) {
    return rand() % (max - min + 1) + min;
}

// Fonction pour réinitialiser la position et la vitesse d'une forme lorsqu'elle sort de l'écran
void resetShape(Shape* shape) {
    shape->x = getRandomNumber(0, WINDOW_WIDTH - SHAPE_SIZE);
    shape->y = 0;
}

int main() {
    // Initialisation de la seed pour la génération de nombres aléatoires
    srand(time(NULL));

    // Initialisation de SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        fprintf(stderr, "Erreur lors de l'initialisation de SDL: %s\n", SDL_GetError());
        return 1;
    }

    // Initialisation de SDL_image
    if (IMG_Init(IMG_INIT_PNG) < 0) {
        fprintf(stderr, "Erreur lors de l'initialisation de SDL_image: %s\n", IMG_GetError());
        return 1;
    }

    // Initialisation de SDL_ttf
    if (TTF_Init() < 0) {
        fprintf(stderr, "Erreur lors de l'initialisation de SDL_ttf: %s\n", TTF_GetError());
        return 1;
    }

    TTF_Font* font = TTF_OpenFont("/usr/share/fonts/truetype/tlwg/Laksaman-BoldItalic.ttf", 24);
    if (font == NULL) {
        fprintf(stderr, "Erreur lors du chargement de la police de caractères: %s\n", TTF_GetError());
        return 1;
    }

    // Création de la fenêtre
    SDL_Window* window = SDL_CreateWindow("Jeu de formes", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
    if (window == NULL) {
        fprintf(stderr, "Erreur lors de la création de la fenêtre: %s\n", SDL_GetError());
        return 1;
    }

    // Création du renderer
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL) {
        fprintf(stderr, "Erreur lors de la création du renderer: %s\n", SDL_GetError());
        return 1;
    }

    // Chargement des textures des formes
    SDL_Surface* shapeSurface = IMG_Load("fonts/im.jpeg");
    if (shapeSurface == NULL) {
        fprintf(stderr, "Erreur lors du chargement de l'image de la forme: %s\n", SDL_GetError());
        return 1;
    }
    SDL_Texture* shapeTexture = SDL_CreateTextureFromSurface(renderer, shapeSurface);
    if (shapeTexture == NULL) {
        fprintf(stderr, "Erreur lors de la création de la texture de la forme: %s\n", SDL_GetError());
        return 1;
    }
    SDL_FreeSurface(shapeSurface);

    // Création de la forme
    Shape shape;
    resetShape(&shape);
    shape.speed= INITIAL_SHAPE_SPEED;
    shape.texture = shapeTexture;

    // Création du plateau
    SDL_Rect plateRect = { WINDOW_WIDTH / 2 - PLATE_WIDTH / 2, WINDOW_HEIGHT - PLATE_HEIGHT, PLATE_WIDTH, PLATE_HEIGHT };

    // Création des calques de fond parallaxe
    BackgroundLayer backgroundLayers[BACKGROUND_LAYER_COUNT];
    for (int i = 0; i < BACKGROUND_LAYER_COUNT; i++) {
        SDL_Surface* backgroundSurface = IMG_Load(BACKGROUND_IMAGES[i]);
        if (backgroundSurface == NULL) {
            fprintf(stderr, "Erreur lors du chargement de l'image de fond: %s\n", SDL_GetError());
            return 1;
        }
        SDL_Texture* backgroundTexture = SDL_CreateTextureFromSurface(renderer, backgroundSurface);
        if (backgroundTexture == NULL) {
            fprintf(stderr, "Erreur lors de la création de la texture de fond: %s\n", SDL_GetError());
            return 1;
        }
        SDL_FreeSurface(backgroundSurface);

        BackgroundLayer layer;
        layer.x = 0;
        layer.texture = backgroundTexture;
        backgroundLayers[i] = layer;
    }

    Uint32 lastBackgroundChangeTime = 0; // Temps du dernier changement de fond
    int currentBackgroundLayer = 0; // Calque de fond actuel
    Uint8 backgroundAlpha = 255; // Opacité du fond

    SDL_Event event;
    bool quit = false;
    int score = 0;

    // Variables pour la gestion du délai de frame
    Uint32 frameStartTime; // Temps de départ du frame actuel
    Uint32 frameEndTime; // Temps d'arrivée du frame actuel
    Uint32 frameDuration; // Durée du frame actuel en millisecondes
    Uint32 desiredFrameDuration = 1; // Durée souhaitée de chaque frame en millisecondes

    while (!quit) {
        // Mesurer le temps de départ du frame actuel
        frameStartTime = SDL_GetTicks();

        // Gestion des événements
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                quit = true;
            }
        }


        // Déplacement de la forme vers le bas
        shape.y += shape.speed;

        // Vérifier si la forme est en collision avec le plateau
        bool isCaught = SDL_HasIntersection(&plateRect, &(SDL_Rect){ shape.x, shape.y, SHAPE_SIZE, SHAPE_SIZE });
        

        // Réinitialiser la forme si elle est attrapée ou sortie de l'écran
        if (isCaught || shape.y >= WINDOW_HEIGHT) {
            if (isCaught) {
            score += SCORE_INCREMENT;
            resetShape(&shape);
            shape.speed += INITIAL_SHAPE_SPEED/6;
             }
            else{score--;resetShape(&shape);}
        }

        // Déplacement du plateau
        const Uint8* keystate = SDL_GetKeyboardState(NULL);
        if (keystate[SDL_SCANCODE_LEFT] && plateRect.x - PLATE_SPEED >= 0) {
            plateRect.x -= PLATE_SPEED;
        }
        if (keystate[SDL_SCANCODE_RIGHT] && plateRect.x + plateRect.w + PLATE_SPEED <= WINDOW_WIDTH) {
            plateRect.x += PLATE_SPEED;
        }

        // Gestion du fond parallaxe
        Uint32 currentTicks = SDL_GetTicks();
        if (currentTicks - lastBackgroundChangeTime >= BACKGROUND_CHANGE_INTERVAL) {
            lastBackgroundChangeTime = currentTicks;
            currentBackgroundLayer = (currentBackgroundLayer + 1) % BACKGROUND_LAYER_COUNT;
            backgroundAlpha = 0;
        }

        // Réglage de l'opacité du fond
        if (backgroundAlpha < 255) {
            backgroundAlpha += BACKGROUND_FADE_SPEED;
                backgroundAlpha = 255;
        }


        // Dessin du fond parallaxe
        for (int i = 0; i < BACKGROUND_LAYER_COUNT; i++) {
            SDL_Rect backgroundRect = { backgroundLayers[i].x, 0, WINDOW_WIDTH, WINDOW_HEIGHT };
            SDL_SetTextureAlphaMod(backgroundLayers[i].texture, backgroundAlpha);
            SDL_RenderCopy(renderer, backgroundLayers[i].texture, NULL, &backgroundRect);

            backgroundLayers[i].x -= BACKGROUND_SCROLL_SPEEDS[i];
            if (backgroundLayers[i].x <= -WINDOW_WIDTH) {
                backgroundLayers[i].x = WINDOW_WIDTH;
            }
        }

        // Dessin de la forme
        SDL_Rect shapeRect = { shape.x, shape.y, SHAPE_SIZE, SHAPE_SIZE };
        SDL_RenderCopy(renderer, shape.texture, NULL, &shapeRect);

        // Dessin du plateau
        SDL_RenderFillRect(renderer, &plateRect);

        // Affichage du score
        char scoreText[10];
        snprintf(scoreText, sizeof(scoreText), "Score: %d", score);
        SDL_Color textColor ;
        textColor.a=255;
        textColor.b=255;
        textColor.g=255;
        SDL_Surface* textSurface = TTF_RenderText_Solid(font, scoreText, textColor);
        if (textSurface == NULL) {
            fprintf(stderr, "Erreur lors du rendu du texte: %s\n", TTF_GetError());
            return 1;
        }
        SDL_Texture* textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
        if (textTexture == NULL) {
            fprintf(stderr, "Erreur lors de la création de la texture du texte: %s\n", SDL_GetError());
            return 1;
        }
        SDL_Rect textRect = { 10, 10, textSurface->w, textSurface->h };
        SDL_RenderCopy(renderer, textTexture, NULL, &textRect);
        SDL_FreeSurface(textSurface);
        SDL_DestroyTexture(textTexture);

        // Affichage du rendu
        SDL_RenderPresent(renderer);

        // Mesurer le temps d'arrivée du frame actuel
        frameEndTime = SDL_GetTicks();

        // Calculer la durée du frame actuel
        frameDuration = frameEndTime - frameStartTime;

        // Attendre si nécessaire pour maintenir la durée souhaitée du frame
        if (frameDuration < desiredFrameDuration) {
            SDL_Delay(desiredFrameDuration - frameDuration);
        }
    }

    // Libération des ressources
    SDL_DestroyTexture(shapeTexture);
    for (int i = 0; i < BACKGROUND_LAYER_COUNT; i++) {
        SDL_DestroyTexture(backgroundLayers[i].texture);
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
