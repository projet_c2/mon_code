/**
 * @file arbres_insert.c
 * @brief fichier d'implementation du module pour l'insertion de valeur dans une arborescence
 */
#include <stdio.h>
#include <stdlib.h>

#include "../pile.h"
#include "../eltsArbre.h"
#include "arbres_insert.h"


/**
 * @brief rechercher un point de valeur v
 * @param [in] racine pointeur vers la racine de l'arborescence 
 * @return 
 *   - l'adresse du point contenant v si v existe dans l'arborescence
 *   - NULL, sinon
 */

cell_lvlh_t * rechercher_v(cell_lvlh_t * racine, char valeur)
{
    cell_lvlh_t * cour = racine ;  // Déclaration et initialisation d'un pointeur "cour" avec la valeur de la racine de l'arbre.

    pile_t *pile = initPile(NB_ELTPREF_MAX);  // Initialisation d'une pile avec une taille maximale spécifiée.
    eltType * element_pile =  malloc(sizeof(eltType));  // Allocation dynamique d'un pointeur "element_pile" avec la taille d'un élément.
    int code = 0;  // Initialisation d'une variable "code" à zéro.

    if (element_pile){  // Vérification si l'allocation dynamique a réussi.
        element_pile->nbFils_ou_Freres = 1 ;  // Initialisation d'un champ "nbFils_ou_Freres" de la structure pointée par "element_pile" à 1.
        while ((cour != NULL) && (cour->val != valeur))  // Boucle tant que "cour" n'est pas nul et la valeur de "cour" est différente de "valeur".
        {
            element_pile->adrCell = cour ;  // Affectation de l'adresse de "cour" au champ "adrCell" de la structure pointée par "element_pile".
            element_pile->adrPrec= &cour;  // Affectation de l'adresse de "cour" à "adrPrec" de la structure pointée par "element_pile".
            empiler(pile, element_pile, &code);  // Empilement de "element_pile" dans la pile en utilisant la fonction "empiler".
            cour = cour->lv;  // Passage au fils gauche de "cour".

            while ((cour == NULL) && (!estVidePile(pile)))  // Boucle tant que "cour" est nul et la pile n'est pas vide.
            {
                depiler(pile, element_pile, &code);  // Dépilement de "element_pile" depuis la pile en utilisant la fonction "depiler".
                cour = element_pile->adrCell;  // Affectation de l'adresse de cellule de "element_pile" à "cour".
                cour = cour->lh;  // Passage au frère droit de "cour".
            }
        }

        free(element_pile);  // Libération de la mémoire allouée pour "element_pile".
    }

    libererPile(&pile);  // Libération de la mémoire allouée pour la pile en utilisant la fonction "libererPile".

    return cour;  // Retourne le pointeur "cour" qui peut être l'élément recherché ou NULL s'il n'est pas trouvé.
}


/**
 * @brief rechercher le double prec de w dans une liste de fils
 * @param [in] adrPere l'adresse du pere
 * @param [in] w la valeur a inserer
 * @return l'adresse du pointeur prec apres lequel w doit etre inseree
 */


cell_lvlh_t ** rechercherPrecFilsTries(cell_lvlh_t * adrPere, char w)
{
   cell_lvlh_t * cour = adrPere->lv;  // Déclaration et initialisation d'un pointeur "cour" avec le fils gauche de "adrPere".

   cell_lvlh_t ** prec = &(adrPere->lv);  // Déclaration et initialisation d'un double pointeur "prec" avec l'adresse du fils gauche de "adrPere".
   
   while ((cour != NULL) && ((cour->val - w) < 0))  // Boucle tant que "cour" n'est pas nul et la différence entre la valeur de "cour" et "w" est inférieure à zéro.
   {
      prec = &(cour->lh);  // Affectation de l'adresse du frère droit de "cour" à "prec".
      cour = cour->lh;  // Passage au frère droit de "cour".
   }
   
   return prec;  // Retourne le double pointeur "prec" qui pointe vers l'adresse du frère droit précédant l'insertion de l'élément "w".
}


/** TO DO
 * @brief inserer une valeur w dans les fils d'un point de valeur v
 * @param [in] racine la racine de l'arborescence
 * @param [in] v la valeur d'un point auquel on va inserer la valeur w en fils
 * @param [in] w la valeur a inserer
 * @return 1 - insertion realisee; 0 - insertion n'a pas ete realisee
 */

int insererTrie(cell_lvlh_t *racine, char v, char w)
{
    int code = 1;  // Déclaration et initialisation d'une variable "code" à 1.

    cell_lvlh_t *new = NULL;  // Déclaration d'un pointeur "new" initialisé à NULL.

    new = allocPoint(w);  // Allocation d'une nouvelle cellule avec la valeur "w" et assignation de l'adresse à "new".

    cell_lvlh_t *pere = rechercher_v(racine, v);  // Recherche du pointeur vers le père de la valeur "v" dans l'arbre et assignation à "pere".

    cell_lvlh_t **pprec = rechercherPrecFilsTries(pere, w);  // Recherche du pointeur précédant (adresse du frère droit précédant) pour l'insertion de la valeur "w" dans la liste triée des fils du père "pere".

    if (pere != NULL)  // Vérification si le père a été trouvé.
    {
        if (new != NULL)  // Vérification si l'allocation de mémoire pour la nouvelle cellule a réussi.
        {
            new->lh = (*pprec);  // Affectation du frère droit précédent à la nouvelle cellule.
            (*pprec) = new;  // Affectation de la nouvelle cellule comme frère droit précédant.

        }
        else
        {
            code = 0;  // L'allocation de mémoire pour la nouvelle cellule a échoué, donc le code est mis à 0 pour indiquer une erreur.
        }
    }
    else
    {
        code = 0;  // Le père n'a pas été trouvé, donc le code est mis à 0 pour indiquer une erreur.
    }

    return code;  // Retourne le code indiquant le succès ou l'échec de l'insertion.
}
