/**
 * @file arbres_parcours.c
 * @brief fichier d'implementation du module pour le parcours d'arborescence
 */
#include <stdio.h>
#include <stdlib.h>

#include "../pile.h"
#include "../eltsArbre.h"
#include "arbres_parcours.h"


/** TO DO
 * @brief calculer le nombre de fils ou freres d'un point a partir du pointeur du debut de la liste
 * @param [in] ptCell pointeur vers le 1er frere
 * @return le nombre de fils ou freres
 */

int getNbFils_ou_Freres(cell_lvlh_t *ptCell)
{
    int res = 0;  // Variable pour stocker le résultat (nombre de fils ou de frères)
    cell_lvlh_t *cour = ptCell;  // Déclare un pointeur vers une structure "cell_lvlh_t" et l'initialise avec la valeur de "ptCell"

    while (cour != NULL)  // Boucle qui s'exécute tant que le pointeur "cour" n'est pas NULL
    {
        cour = cour->lh;  // Avance le pointeur "cour" vers le frère suivant en utilisant le pointeur "lh" dans la structure "cour"
        res++;  // Incrémente la variable "res" pour compter le nombre de frères
    }

    return res;  // Retourne le nombre de fils ou de frères
}

/** TO DO
 * @brief parcours en profondeur postfixee
 * @param [in] file le flux de sortie
 * @param [in] racine la racine de l'arborescence
 */


void printPostfixee(FILE *file, cell_lvlh_t *racine)
{
    pile_t *pile = initPile(NB_ELTPREF_MAX);  // Initialisation de la pile en utilisant la fonction "initPile" avec la taille maximale prédéfinie.
    eltType *element_pile = malloc(sizeof(eltType));  // Allocation dynamique de mémoire pour une structure "eltType" et affectation du pointeur à "element_pile"
    int code = 0;  // Variable pour stocker le code de retour
    cell_lvlh_t *cour = racine;  // Déclare un pointeur "cour" et l'initialise avec la valeur de "racine"

    while (cour != NULL)  // Boucle qui s'exécute tant que le pointeur "cour" n'est pas NULL
    {
        while (cour->lv != NULL)  // Boucle qui s'exécute tant que le pointeur "lv" dans la structure "cour" n'est pas NULL
        {
            element_pile->adrCell = cour;  // Affecte le pointeur "cour" à la variable "adrCell" dans la structure pointée par "element_pile"
            element_pile->adrPrec = &cour;  // Affecte l'adresse du pointeur "cour" à la variable "adrPrec" dans la structure pointée par "element_pile"
            element_pile->nbFils_ou_Freres = getNbFils_ou_Freres(cour->lv);  // Appelle la fonction "getNbFils_ou_Freres" avec le pointeur "lv" dans la structure "cour" et affecte  le résultat à la variable "nbFils_ou_Freres" dans la structure pointée par "element_pile"
            empiler(pile, element_pile, &code);  // Empile la structure "element_pile" sur la pile en utilisant la fonction "empiler"
            cour = cour->lv;  // Avance le pointeur "cour" vers le fils suivant en utilisant le pointeur "lv" dans la structure "cour"
        }

        fprintf(file, "(%c,%d) ", cour->val, getNbFils_ou_Freres(cour->lv));  // Écrit dans le fichier les informations de la cellule courante en utilisant la fonction "fprintf" avec le format spécifié.
        cour = cour->lh;  // Avance le pointeur "cour" vers le frère suivant en utilisant le pointeur "lh" dans la structure "cour"

        while ((cour == NULL) && (!estVidePile(pile)))  // Boucle qui s'exécute tant que le pointeur "cour" est NULL et que la pile n'est pas vide
        {
            depiler(pile, element_pile, &code);  // Dépile la structure "element_pile" de la pile en utilisant la fonction "depiler"
            cour = element_pile->adrCell;  // Récupère le pointeur "adrCell" dans la structure pointée par "element_pile" et l'assigne à "cour"
            fprintf(file, "(%c,%d) ", cour->val, getNbFils_ou_Freres(cour->lv));  // Écrit dans le fichier les informations de la cellule courante en utilisant la fonction "fprintf" avec le format spécifié
            cour = cour->lh;  // Avance le pointeur "cour" vers le frère suivant en utilisant le pointeur "lh" dans la structure "cour"
        }
    }

    fprintf(file, "%d\n", getNbFils_ou_Freres(racine));  // Écrit dans le fichier le nombre de fils ou de frères de la racine en utilisant la fonction "fprintf" avec le format spécifié.
    free(element_pile);  // Libère la mémoire allouée pour la structure "element_pile"
    libererPile(&pile);  // Libère la mémoire utilisée par la pile en utilisant la fonction "libererPile".
}
