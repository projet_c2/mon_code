/**
 * @file arbres_construct.c
 * @brief fichier d'implementation du programme pour la construction d'une arborescence
 */

#include <stdio.h>
#include <stdlib.h>

#include "../pile.h"
#include "../eltsArbre.h"
#include "arbres_construct.h"

/**
 * @brief lire le fichier contenant la representation prefixee de l'arborescence
 * @param [in] fileName le nom du fichier contenant la representation prefixee
 * @param [in, out] tabEltPref tableau des elements de la representation prefixee
 * @param [in, out] nbEltsPref l'adresse memoire contenant le nombre des elements du tabEltPref
 * @return le nombre de racines
 */


int lirePref_fromFileName (char * fileName , eltPrefPostFixee_t * tabEltPref , int * nbEltsPref )
{
    int nbRacines = 0;  // Variable locale pour stocker le nombre de racines
    FILE* file = fopen(fileName,"r");  // Ouvre le fichier spécifié en mode lecture et assigne le pointeur de fichier à la variable "file"

    if( file != NULL)  // Vérifie si l'ouverture du fichier a réussi
    {
        int i=0;  // Variable locale pour suivre l'indice du tableau tabEltPref

        fscanf(file,"%d",&nbRacines);  // Lit le nombre de racines à partir du fichier et le stocke dans la variable "nbRacines"

        while ((fscanf(file," %c",&(tabEltPref+i)->val)!=EOF)&&(fscanf(file," %d",&(tabEltPref+i)->nbFils)!=EOF))
        {
            i++;  // Incrémente l'indice pour passer à l'élément suivant du tableau tabEltPref
        }

        (*nbEltsPref) = i;  // Met à jour la valeur de nbEltsPref avec le nombre d'éléments lus à partir du fichier
    }

    fclose( file );  // Ferme le fichier

    return nbRacines;  // Renvoie le nombre de racines lu à partir du fichier
}

/** TO DO
 * @brief afficher les elements de la representation prefixee sur un flux de sortie
 * @param file : le flux de sortie
 * @param [in, out] tabEltPref tableau des elements de la representation prefixee
 * @param [in, out] nbEltsPref le nombre des elements du tabEltPref
 */


void printTabEltPref(FILE *file, eltPrefPostFixee_t * tabEltPref, int nbEltsPref)
{
    if (nbEltsPref!=0)  // Vérifie si le nombre d'éléments à afficher n'est pas nul
    {
        int i=0;  // Variable locale pour suivre l'indice du tableau tabEltPref

        while (i<nbEltsPref-1)  // Boucle pour itérer à travers les éléments du tableau sauf le dernier
        {
            fprintf(file,"(%c,%d) ",tabEltPref[i].val,tabEltPref[i].nbFils);  // afficher les valeurs du caractère et du nombre d'enfants de l'élément actuel dans le fichier
            i++;  // Incrémente l'indice pour passer à l'élément suivant du tableau
        } 

        fprintf(file,"(%c,%d)\n",tabEltPref[nbEltsPref-1].val,tabEltPref[nbEltsPref-1].nbFils);  // Imprime les valeurs du caractère et du nombre d'enfants du dernier élément dans le fichier avec un retour à la ligne
    }
}

/** TO DO
 * @brief creer et initialiser un nouveau point de l'arborescence
 * @param [in] val la valeur du point
 * @return l'adresse du nouveau point 
 */


cell_lvlh_t * allocPoint(char val)
{
    cell_lvlh_t *new = malloc(sizeof(cell_lvlh_t));  // Alloue dynamiquement de la mémoire pour une nouvelle structure cell_lvlh_t et assigne l'adresse mémoire à un pointeur "new"

    if(new != NULL)  // Vérifie si l'allocation de mémoire a réussi
    {
        (*new).val = val ;  // Affecte la valeur du 
        (*new).lv = NULL;  // Initialise le pointeur "lv" 
        (*new).lh = NULL;  // Initialise le pointeur "lh" 
    }

    return new;  // Retourne le pointeur "new" qui pointe vers la nouvelle structure allouée
}


/** TO DO
 * @brief construire un arbre avec lvlh a partir de representation prefixee
 * @param [in] tabEltPref tableau des elements de la representation prefixee
 * @param [in] nbRacines nombre de racines de l'arborescence
 * @return : 
 *     - NULL si l'arbre resultatnt est vide
 *     - l'adresse de la racine de l'arbre sinon
*/


cell_lvlh_t * pref2lvlh(eltPrefPostFixee_t * tabEltPref, int  nbRacines )
{                
    pile_t * pile = initPile (NB_ELTPREF_MAX);  // Initialise une pile avec une taille maximale prédéfinie.
    eltType * element_pile = malloc(sizeof(eltType));  // Alloue dynamiquement de la mémoire pour une nouvelle structure "eltType".
    cell_lvlh_t * new = NULL;  // Déclare un pointeur vers une structure "cell_lvlh_t" et l'initialise à NULL    int code = 0;  // Variable pour stocker un code de retour
    char c;  // Variable pour stocker un caractère
    int code = 0; // Variable pour stocker un code de retour
    cell_lvlh_t * adrtete = NULL ;  // Pointeur vers la tête de la structure "cell_lvlh_t" et l'initialise à NULL
    cell_lvlh_t ** prec = &adrtete;  // Pointeur vers un pointeur de "cell_lvlh_t".
    int nbFils_ou_Freres = nbRacines;  // Variable pour stocker le nombre de fils ou de frères, initialisée avec le nombre de racines.
    int i = 0;  // Variable pour suivre l'indice du tableau tabEltPref

    while ( (nbFils_ou_Freres > 0) || (!estVidePile( pile)) )  // Boucle qui s'exécute tant que le nombre de fils ou de frères est supérieur à zéro ou la pile n'est pas vide.
    {
        if ((nbFils_ou_Freres > 0))  // Vérifie si le nombre de fils ou de frères est supérieur à zéro
        { 
            c = tabEltPref[i].val;  // Récupère le caractère à l'indice i du tableau tabEltPref et le stocke dans la variable c.
            new = allocPoint(c);  // Appelle la fonction "allocPoint" pour allouer dynamiquement une nouvelle cellule avec le caractère c.
            (*prec) = new ;  // Attribue le pointeur "new" à l'adresse pointée par "prec"
            element_pile->adrCell = new ;  // Affecte le pointeur "new" à la variable "adrCell" dans la structure pointée par "element_pile"
            element_pile->adrPrec= &(new->lh);  // Affecte l'adresse du pointeur "lh" dans la structure pointée par "new" à la variable "adrPrec" dans la structure pointée par "element_pile".
            element_pile->nbFils_ou_Freres = nbFils_ou_Freres-1 ;  // Affecte le nombre de fils ou de frères moins un à la variable "nbFils_ou_Freres" dans la structure pointée par "element_pile"
            empiler(pile,element_pile,&code);  // Empile la structure "element_pile" sur la pile en utilisant la fonction "empiler" avec le pointeur de pile, le pointeur de structure.
            prec = &(new->lv);  // Affecte l'adresse du pointeur "lv" dans la structure pointée par "new" à "prec".
            nbFils_ou_Freres = tabEltPref[i].nbFils;  // Récupère le nombre de fils à l'indice i du tableau tabEltPref et l'assigne à la variable "nbFils_ou_Freres".
            i = i + 1;  // Incrémente l'indice i pour passer à l'élément suivant du tableau

        }
        else 
        {
            if (!estVidePile(pile)){  // Vérifie si la pile n'est pas vide
                depiler(pile,element_pile,&code);  // Dépile la structure "element_pile" de la pile en utilisant la fonction "depiler" avec le pointeur de pile, le pointeur de structure.
                prec = element_pile->adrPrec;  // Affecte la valeur de "adrPrec" dans la structure pointée par "element_pile" à "prec".
                nbFils_ou_Freres = element_pile->nbFils_ou_Freres;  // Affecte la valeur de "nbFils_ou_Freres" dans la structure pointée par "element_pile" à la variable "nbFils_ou_Freres".
            }
        }
    }

    free (element_pile);  // Libère la mémoire allouée dynamiquement pour la structure "element_pile"
    libererPile(&pile);  // Libère la mémoire allouée dynamiquement pour la pile en utilisant la fonction "libererPile" avec le pointeur de pile.
    return adrtete;  // Retourne le pointeur vers la tête de la structure.
}

/** TO DO
 * @brief liberer les blocs memoire d'un arbre
 * @param [in] adrPtRacine l'adresse du pointeur de la racine d'un arbre
 */


void libererArbre(cell_lvlh_t ** adrPtRacine)
{
    cell_lvlh_t * racine = (*adrPtRacine);  // Déclare un pointeur vers une structure et l'initialise avec la valeur pointée par "adrPtRacine"
    if (racine != NULL)  // Vérifie si la racine n'est pas NULL
    {
        pile_t *pile = initPile(NB_ELTPREF_MAX);  // Initialise une pile avec une taille maximale prédéfinie.
        eltType *element_pile = malloc(sizeof(eltType));  // Alloue dynamiquement de la mémoire pour une nouvelle structure "eltType" et assigne l'adresse mémoire à "element_pile"
        int code = 0;  // Variable pour stocker un code de retour
        
        element_pile->adrCell = racine;  // Affecte la valeur de "racine" à la variable "adrCell" dans la structure .
        element_pile->adrPrec = &racine;  // Affecte l'adresse de "racine" à la variable "adrPrec" dans la structure.
        element_pile->nbFils_ou_Freres = 1;  // Affecte la valeur 1 à la variable "nbFils_ou_Freres" dans la structure.
        empiler(pile, element_pile, &code);  // Empile la structure "element_pile" sur la pile en utilisant la fonction "empiler".
        
        while (!estVidePile(pile))  // Boucle qui s'exécute tant que la pile n'est pas vide
        {
            depiler(pile, element_pile, &code);  // Dépile la structure "element_pile" de la pile en utilisant la fonction "depiler".
            cell_lvlh_t *cour = element_pile->adrCell;  // Déclare un pointeur vers une structure "cell_lvlh_t" et l'initialise avec la valeur de "adrCell" dans la structure.
            
            element_pile->adrPrec = &cour;  // Affecte l'adresse de "cour" à la variable "adrPrec" dans la structure pointée par "element_pile"
            element_pile->nbFils_ou_Freres = 1;  // Affecte la valeur 1 à la variable "nbFils_ou_Freres" dans la structure pointée par "element_pile"
            
            if (cour->lv != NULL)  // Vérifie si le pointeur "lv" dans la structure pointée par "cour" n'est pas NULL
            {
                element_pile->adrCell = cour->lv;  // Affecte la valeur de "lv" dans la structure pointée par "cour" à la variable "adrCell" dans la structure pointée par "element_pile"
                empiler(pile, element_pile, &code);  // Empile la structure "element_pile" sur la pile en utilisant la fonction "empiler".
            }
            if (cour->lh != NULL)  // Vérifie si le pointeur "lh" dans la structure pointée par "cour" n'est pas NULL
            {
                element_pile->adrCell = cour->lh;  // Affecte la valeur de "lh" dans la structure pointée par "cour" à la variable "adrCell" dans la structure pointée par "element_pile"
                empiler(pile, element_pile, &code);  // Empile la structure "element_pile" sur la pile en utilisant la fonction "empiler".
            }
            
            free(cour);  // Libère la mémoire allouée dynamiquement pour la structure "cour"
        }
        
        free(element_pile);  // Libère la mémoire allouée dynamiquement pour la structure "element_pile"
        libererPile(&pile);  // Libère la mémoire allouée dynamiquement pour la pile en utilisant la fonction "libererPile" avec le pointeur de pile
    }
}





