#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "linkedList.h"
#include "teZZt.h"



BEGIN_TEST_GROUP(linkedList)

TEST(monom_degree_cmp) {
	monom_t v1 = {5.11, 7};
	monom_t v2 = {3., 5};
	monom_t v3 = {5.25, 7};

	printf("\nComparaison des monomes : \n");
	CHECK( monom_degree_cmp(&v1, &v2) > 0 );
	CHECK( monom_degree_cmp(&v2, &v1) < 0 );
	CHECK( monom_degree_cmp(&v1, &v3) == 0 );
}

TEST(monom_save2file) {
	monom_t v = {5., 7};


	char buffer[1024];
	FILE * file = fmemopen(buffer, 1024, "w");
	REQUIRE ( NULL != file);

	monom_save2file(file, &v);

	fclose(file);

	CHECK( 0 == strcmp(buffer, "5.000 7\n") ); 
}


TEST(LL_init_list) {
	cell_t *list=NULL;

	printf("\nInitialization of the linked list : \n");
	LL_init_list(&list);

	REQUIRE ( list == NULL );
}


TEST(LL_create_cell) {
	cell_t *new = NULL;
	monom_t m1 = {3.245, 17};

	printf("\nCreate a new cell (3.245 17) : \n");
	new = LL_create_cell(&m1);
	REQUIRE ( NULL != new );
	CHECK ( NULL == new->next );

	char buffer[1024];
	FILE * file = fmemopen(buffer, 1024, "w");
	REQUIRE ( NULL != file);


	monom_save2file(file, &(new->val));
	fclose(file);
	CHECK( 0 == strcmp(buffer, "3.245 17\n") );



	free(new); 
}



TEST(LL_add_cell1) { 
	cell_t *list = NULL;
	cell_t *new = NULL;
	monom_t m1 = {3.45, 17};

	printf("\najouter une cellule dans une Liste : \n");

	new = LL_create_cell(&m1);
	REQUIRE ( new != NULL );

	LL_add_cell(&list, new);
	CHECK( list == new ); 
	
	CHECK( list->val.coef == 3.45 );  
	CHECK( list->val.degree == 17 );  
	CHECK( list->next == NULL );


	free(list); 
}


TEST(LL_add_cell2) { 
	cell_t *list = NULL;
	cell_t *new = NULL;
	monom_t m1 = {3.45, 17};
	monom_t m2 = {25.8, 9};

	printf("\najouter deux cellule dans une Liste : \n");

	new = LL_create_cell(&m1);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new ); 

	new = LL_create_cell(&m2);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new ); 

	CHECK(list->val.coef==25.8);
	CHECK(list->val.degree==9);
	CHECK((*((cell_t *)(list->next))).val.coef==3.45);
	CHECK((*((cell_t *)(list->next))).val.degree==17);



	free((list->next));
	free((list));
}


TEST(LL_add_cell3) { 
	cell_t *list = NULL;
	cell_t *new = NULL;
	monom_t m1 = {3.245, 17};
	monom_t m2 = {25.8, 9};
	monom_t m3 = {12.4, 3};

	printf("\najouter trois cellule dans une Liste : \n");

	new = LL_create_cell(&m1);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new ); 

	new = LL_create_cell(&m2);
	REQUIRE ( new!= NULL );
	LL_add_cell(&list, new);
	CHECK( list == new ); 

	new = LL_create_cell(&m3);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new );

	LL_save_list_toFile(stdout, list, &(monom_save2file));




	char buffer1[1024];
	FILE * fich = fmemopen(buffer1, 1024, "w+");
	REQUIRE ( NULL != fich);

	LL_save_list_toFile(fich,list,&(monom_save2file));
	
	fclose(fich);
	CHECK( 0 == strcmp(buffer1, "12.400 3\n25.800 9\n3.245 17\n") );


	LL_free_list(&list);
	CHECK(list==NULL);
}



TEST(LL_create_list_fromFileName0) {
	cell_t *list=NULL;

	printf("\n Créer une liste à partir d'un fichier notExist.txt: \n");

	LL_create_list_fromFileName(&list, "notExist.txt");
	CHECK( NULL == list );

}


TEST(LL_create_list_fromFileName) {
	cell_t *list =NULL;

	printf("\n Créer une liste à partir d'un fichier Test.txt: \n");

	
	list=LL_create_list_fromFileName(&list , "Test.txt");
	REQUIRE ( NULL != list );

	CHECK(list->val.coef==11.21);
	CHECK(list->val.degree==7);
	LL_free_list(&list);
}

TEST(LL_save_list_toFile) { 

	cell_t *list=NULL;
	cell_t *new = NULL;
	monom_t m1 = {2.25, 10};
	monom_t m2 = {12.21, 7};

	printf("\n L'affichage d'un polynome sur un flux de sortie :\n");

	new = LL_create_cell(&m1);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new ); 

	new = LL_create_cell(&m2);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new ); 

	char buffer[1024];
	FILE * file = fmemopen(buffer, 1024, "w");

	REQUIRE ( NULL != file);

	REQUIRE(list!=NULL);
	LL_save_list_toFile(stdout, list,&(monom_save2file));
	printf("\n");

	LL_save_list_toFile(file,list,&(monom_save2file));
	
    fclose(file);
	CHECK( 0 == strcmp(buffer, "12.210 7\n2.250 10\n") );


	LL_free_list(&list);


}

TEST(LL_search_prev) { 
	cell_t *list=NULL;
	cell_t *new = NULL;
	
	monom_t m1 = {46.25, 19};
	monom_t m2 = {17.9, 12};
	monom_t m3 = {132.5, 7};
	monom_t m4 = {125.2, 10};
	monom_t m5 = {69.3, 5};

	new = LL_create_cell(&m1);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new ); 

	new = LL_create_cell(&m2);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new ); 

	new = LL_create_cell(&m3);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new );

	new = LL_create_cell(&m4);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new );
	LL_save_list_toFile(stdout, list, &(monom_save2file));
	printf("\n");


	printf("\nrecherche d'un monome dans une liste :\n");

	cell_t **cour=NULL;
	cour = LL_search_prev(&list,&m4,monom_degree_cmp);
    LL_save_list_toFile(stdout, list,&(monom_save2file));

	
	REQUIRE(cour!=NULL);
	CHECK((*cour)->val.coef==m4.coef);
	CHECK((*cour)->val.degree==m4.degree);
	


	
	cell_t **cour5=NULL;
	cour5=LL_search_prev(&list,&m5,monom_degree_cmp);
    LL_save_list_toFile(stdout, list,&(monom_save2file));
	REQUIRE((*cour5)==NULL);
	

	LL_free_list(&list);

}

TEST(LL_add_celln) { 
	cell_t *list=NULL;
	cell_t *new = NULL;
	cell_t **cour=NULL;
	monom_t m1 = {46.25, 19};
	monom_t m2 = {17.9, 12};
	monom_t m3 = {132.5, 7};
	monom_t m4 = {125.2, 10};
	monom_t m5 = {69.3, 5};

	new = LL_create_cell(&m1);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new ); 

	new = LL_create_cell(&m2);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new ); 

	new = LL_create_cell(&m3);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new );

	LL_save_list_toFile(stdout, list,&(monom_save2file));
	printf("\n");

	printf("\ninsertion en fin de la liste\n");
	new=LL_create_cell(&m4);
	cour=LL_search_prev(&list,&m1,monom_degree_cmp);
	LL_add_cell((cell_t **)(&((*cour)->next)),new);

	LL_save_list_toFile(stdout, list,&(monom_save2file));
	printf("\n");

	printf("\ninsertion  au milieu de la cellule\n");
	new=LL_create_cell(&m5);
	cour=LL_search_prev(&list,&m3,monom_degree_cmp);
	LL_add_cell((cell_t **)(&((*cour)->next)),new);

	LL_save_list_toFile(stdout, list,&(monom_save2file));
	printf("\n");


	LL_free_list(&list);

}


TEST(LL_del_cell) { 
	cell_t *list=NULL;
	cell_t *new = NULL;
	monom_t m1 = {46.25, 19};
	monom_t m2 = {17.9, 12};
	monom_t m3 = {132.5, 7};
	monom_t m4 = {125.2, 10};

	cell_t **cour=NULL;
	
	new = LL_create_cell(&m1);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new ); 

	new = LL_create_cell(&m2);
	REQUIRE ( new!= NULL );
	LL_add_cell(&list, new);
	CHECK( list == new ); 

	new = LL_create_cell(&m3);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new );

	new = LL_create_cell(&m4);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new);
	LL_save_list_toFile(stdout, list,&(monom_save2file));

	printf("\nla suppression d'un element de la liste:\n");


	
	cour = LL_search_prev(NULL,&m1,monom_degree_cmp);
	LL_del_cell(cour);

	LL_save_list_toFile(stdout, list,&(monom_save2file));
	printf("\n");

	char buffer1[1024];
	FILE * fich = fmemopen(buffer1, 1024, "w");
	REQUIRE ( NULL != fich);

	LL_save_list_toFile(fich,list,&(monom_save2file));
	
	fclose(fich);
	CHECK( 0 == strcmp(buffer1, "125.200 10\n132.500 7\n17.900 12\n46.250 19\n") );

	printf("\n La supperession le 2iéme élement (cellule) de la liste\n");
	cour = LL_search_prev(&list,&m3,monom_degree_cmp);
	LL_del_cell(cour);

	LL_save_list_toFile(stdout, list,&(monom_save2file));
	printf("\n");

	char bufier[1024];
	FILE * file = fmemopen(bufier, 1024, "w");
	REQUIRE ( NULL != file);

	LL_save_list_toFile(file,list,&(monom_save2file));

	fclose(file);
	CHECK( 0 == strcmp(bufier, "125.200 10\n17.900 12\n46.250 19\n") );
	
	printf("\nLa supperssion de dernieure cellule\n");
	cour = LL_search_prev(&list,&m1,monom_degree_cmp);
	LL_del_cell(cour);

	LL_save_list_toFile(stdout, list,&(monom_save2file));
	printf("\n");

	char buffer[1024];
	FILE * fichier = fmemopen(buffer, 1024, "w");
	
	REQUIRE ( NULL != fichier);

	LL_save_list_toFile(fichier,list,&(monom_save2file));
	
	fclose(fichier);
	CHECK( 0 == strcmp(buffer, "125.200 10\n17.900 12\n") );

	
	LL_free_list(&list);//liberer la liste
}

TEST(LL_free_list) { 
    cell_t *list=NULL;
	cell_t *new=NULL;
	monom_t m1 = {46.25, 19};
	monom_t m2 = {17.9, 12};
	monom_t m3 = {132.5, 7};
	monom_t m4 = {125.2, 10};

    new = LL_create_cell(&m1);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new ); 

	new = LL_create_cell(&m2);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new ); 

	new = LL_create_cell(&m3);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new );

	new = LL_create_cell(&m4);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new );

	printf("\nla liberation de liste :\n");

	LL_free_list(&list);
	CHECK(list==NULL);
}



END_TEST_GROUP(linkedList)

int main(void) {
	RUN_TEST_GROUP(linkedList);
	return EXIT_SUCCESS;
}

