

#ifndef __LINKEDLIST_H__
#define __LINKEDLIST_H__

#include "valCell.h"

typedef struct{
    monom_t val;
    struct cell_t * next;
} cell_t;


void LL_init_list(cell_t **adrHeadPt); // initialiser la liste 


cell_t * LL_create_cell(monom_t *);  // créer une cellule


void LL_add_cell( cell_t **list ,cell_t *); // ajouter une cellule dans une liste



cell_t * LL_create_list_fromFileName(cell_t **,char *);  // créer la liste à partir d'un fichier dont le nom est donné comme parametre




void LL_save_list_toFile(FILE *,cell_t *, void (* monom_save2file)( FILE * ,monom_t *)); // sauvegarder une liste dans un fichier donné comme paramétre


void LL_save_list_toFileName(cell_t *,char*,void (* LL_save_list_toFile)(FILE *,cell_t *,void (* monom_save2file)( FILE * ,monom_t *))); // sauvegarder une liste dans un fichier dont le nom donné comme paramétre


cell_t ** LL_search_prev(cell_t **,monom_t *, int (*value_cmp)(monom_t *,monom_t *)); // rechercher de monome dans une liste


void LL_del_cell(cell_t**); // supprimer une cellule dans une liste


void LL_free_list(cell_t **);  // liberer une liste


#endif
