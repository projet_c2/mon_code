#include <stdio.h>
#include <stdlib.h>
//#include <float.h>  // FLT_EPSILON DBL_EPSILON LDBL_EPSILON
#include <math.h>

#include "linkedList.h"


void poly_derive(cell_t ** poly)
{
    cell_t * cour=NULL; // parcourir la liste (polynome) poly
    cell_t ** prec=poly; // conserver le precedent
    cour = (*poly);
    while ((cour!=NULL)&&(cour->val.degree!=0)){ // tester si on n'est pas à la fin de la liste et le degré de monome de cellule n'est pas zéro
        cour->val.coef =(float) ((cour->val.coef)*(cour->val.degree));  // multiplier le coeficient par le degré
        cour->val.degree = (int)(cour->val.degree-1);  // diminuer le degré par 1
        prec=(cell_t **)(&((*prec)->next));  // deplacer le pointeur prec à la cellule suivante
        cour = (cell_t *)(cour->next);    // deplacer le pointeur cour à la cellule suivante
    }
    if(cour!=NULL) // si on n'est pas à la fin de la liste
    {
        LL_del_cell(prec); // supprimer le precedent 
    }
}



void poly_add(cell_t ** p1,cell_t ** p2)
{
    cell_t **before=NULL;
    before=p1;
    cell_t* cour1=NULL;
    cour1=(*p1);
    while ((cour1!=NULL)&&((*p2)!=NULL)) // tester si on n'est pas à la fin de la liste et auusi on n'est pas à la fin de p2
    {
    
        if(monom_degree_cmp(&(cour1->val),&((*p2)->val))<0) // si le degré de monome p2 est plus grand que cell de monome p1
        {
            cell_t * tmp= (cell_t *)((*p2)->next);; 
            LL_add_cell(before ,(*p2));              // on ajout ce monome à p1 et on le supprime de p2
            before=(cell_t **)(&((*before)->next));
            (*p2)=tmp;
        }
        else
        {
            if(monom_degree_cmp(&(cour1->val),&((*p2)->val))>0) // si le degré de monome p1 est plus grand que cell de monome p2
            {
                before=(cell_t **)(&(cour1->next));
                cour1=(cell_t *)cour1->next;   // on deplace le pointeur pour trouver la vraie position pour ce monome
            } 
            else  // le cas d'égalité des degrés 
            {
                cour1->val.coef=cour1->val.coef + (*p2)->val.coef; 
                if(cour1->val.coef==0){ // la somme de coefficients est nulle
                    LL_del_cell(before); // on supprime cette cellule de p1
                }
                else{
                    before=(cell_t **)(&(cour1->next)); // on deplace les pointeurs vers la nouvell monome de p1 
                }
                cour1=(*before);
                LL_del_cell(p2); // on supprime cette cellule  de p2
            }
        }
            
    }
    if((*p2)!=NULL) // si on n'est pas à la fin de p2
    {
        *before=(*p2);
        (*p2)=NULL;
    }

    
}



cell_t * poly_prod(cell_t *p1,cell_t *p2)
{
    cell_t * poly_resultat=NULL;// le polynome resultant
    cell_t * copie_poly=NULL; // pour conserver la resultat apres chaque iteration 
    while((p2!=NULL)&&(p1!=NULL)){
        cell_t * cour1 = (cell_t *)p1->next;
        poly_resultat = LL_create_cell(&(p1->val));
        cell_t * cour_temp=poly_resultat; // pour parcourir le poly_resultat  temporaire
        while(cour1!=NULL){ 
            cell_t * copie_poly=LL_create_cell(&(cour1->val));
            LL_add_cell((cell_t **)(&(cour_temp->next)),copie_poly);
            cour_temp=(cell_t *)(cour_temp->next);
            cour1=(cell_t *)(cour1->next);
        }
        cell_t * cour =poly_resultat;
        while(cour != NULL)
        {
            cour->val.degree += (p2->val.degree);
            cour->val.coef *= (p2->val.coef);    // met à jour le degré et le coefficient de monome de cette cellule
            cour=(cell_t *)cour->next;
        }
        poly_add(&poly_resultat,&copie_poly); // en fine tout les copie de p1 multiplué par les monomes de p2
        LL_del_cell(&p2);
        copie_poly=poly_resultat;
    }
    return poly_resultat;
}
